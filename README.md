# Bidafarma
Proyecto para la app híbrida de Bidafarma (iOS/Android)

## Herramientas
*   Ionic CLI: 5.2.3+
*   Ionic Framework: 4.6.2
*   @angular/cli: 7.3.9+
*   Cordova CLI: 9.0.0
*   Cordova platforms: Android 8.0.0 / iOS 5.0.1
*   NodeJS: 10.16.0
*   npm: 6.9.0

## To open a CORS disabled explorer
"/Applications/Google Chrome.app/Contents/MacOS/Google Chrome" --allowile-access-from-files --allow-file-access --allow-cross-origin-auth-prompt --enable-file-cookies --user-data-dir="Chrome\tmp\chrome_dev_session" --disable-web-security --new.window​