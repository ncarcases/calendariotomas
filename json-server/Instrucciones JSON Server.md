# Instrucciones JSON Server
1. `npm install` para instalar dependencias
2. Arrancar el servidor JSON Server con el comando `node server.js` ejecutado en la carpeta.
3. No se abre solo el navegador, pero utiliza el puerto 3000.

## Para añadir nuevas rutas
1. Localizar el fichero `routes.json` para ver las rutas que ya tenemos creadas (izquierda) y donde apuntan (a la derecha) y añadir las que queramos
2. Crear el fichero JSON que nos devolverá el servidor falso
3. Añadir la ruta al fichero `db.js`, añadiéndolo mediante una importación con `require` del fichero JSON del paso 2, como está el ejemplo. 
4. Listo, ya debería funcionar. 