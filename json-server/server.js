const jsonServer = require('json-server');
const server = jsonServer.create();
const bodyParser = require('body-parser');
// const mockResponses = require('./controllers/dates-controller');
// const searchMock = require('./controllers/search-controller');

// const db = require(path.join(__dirname, 'db.js'))();
const db = require('./db.js')();
const router = jsonServer.router(db);


const middlewares = jsonServer.defaults();

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares);

server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));

// Add this before server.use(router)
server.use(jsonServer.rewriter(
  require('./routes.json')
));

// Add custom routes before JSON Server router
// server.post('/outbound-calendar', (req, res) => {
//   mockResponses.fillOutboundCalendar(req, res);
// });

// server.post('/inbound-calendar', (req, res) => {
//   mockResponses.fillInboundCalendar(req, res);
// });

// server.post('/booking/search/cash/fast', (req, res) => {
//   searchMock.searchAndRedirect(req, res);
// });

server.get('/**', (req, res, next) => {
  next();
});

// server.post('/**', (req, res, next) => {
//   req.method = 'GET';
//   req.query = Object.assign(req.query, req.body);
//   next();
// });

// Use default router
server.use(router)
server.listen(3000, () => {
  console.log('JSON Server is running');
})
