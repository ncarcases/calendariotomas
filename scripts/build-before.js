module.exports = function(ctx) {
    console.log('************************* START BEFORE BUILD  *************************');
    if (!process.env.PWD) {
        process.env.PWD = process.cwd();
    }
    const gentlyCopy = require('gently-copy');
    gentlyCopy(`${process.env.PWD}/conf/android/AndroidManifest.xml`, `${process.env.PWD}/android/app/src/main/`, {
        overwrite: true
    });

    /* ENTORNO DE PRE */

    // gentlyCopy(`${process.env.PWD}/conf/android/google-servicesPRE.json`, `${process.env.PWD}/android/app/google-services.json`, {
    //     overwrite: true
    // });

    /* ENTORNO DE PRO */
    gentlyCopy(`${process.env.PWD}/conf/android/google-services_PRO.json`, `${process.env.PWD}/android/app/google-services.json`, {
        overwrite: true
    });


    // iOS
    // gentlyCopy(`${process.env.PWD}/conf/ios/Info.plist`, `${process.env.PWD}/ios/App/App/Info.plist`, {
    //     overwrite: true
    // });

    /* ENTORNO DE PRE */
    // gentlyCopy(`${process.env.PWD}/conf/ios/GoogleService-Info_PRE.plist`, `${process.env.PWD}/ios/App/GoogleService-Info.plist`, {
    //     overwrite: true
    // });

    /* ENTORNO DE PRO */
    // gentlyCopy(`${process.env.PWD}/conf/ios/GoogleService-Info_PRO.plist`, `${process.env.PWD}/ios/App/GoogleService-Info.plist`, {
    //     overwrite: true
    // });


    console.log('************************* END BEFORE BUILD *************************');
};