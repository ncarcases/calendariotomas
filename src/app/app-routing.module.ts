import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  }, {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule',
    canActivate: [AuthGuard],
  }, {
    path: 'login',
    loadChildren: './login/login.module#LoginPageModule'
  }, {
    path: 'register',
    loadChildren: './register/register.module#RegisterPageModule'
  }, {
    path: 'appointments/:selectedTab',
    loadChildren: './my-appointments/my-appointments.module#MyAppointmentsPageModule'
  }, {
    path: 'history/:selectedTab',
    loadChildren: './my-history/my-history.module#MyHistoryPageModule'
  }, {
    path: 'my-medication',
    loadChildren: './my-medication/my-medication.module#MyMedicationPageModule'
  },
  {
    path: 'recover-password',
    loadChildren: './recover-password/recover-password.module#RecoverPasswordPageModule'
  },
]
;

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
