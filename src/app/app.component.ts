import { Component, NgZone } from '@angular/core';
import * as moment from 'moment';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';
import { LocalStorageKeys } from 'src/models/localstorage.keys.model';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { ConfigurationService } from '../services/configuration.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { Router } from '@angular/router';


import { Plugins } from '@capacitor/core';
const { MedNotifications } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform             : Platform,
    private splashScreen         : SplashScreen,
    private statusBar            : StatusBar,
    private configurationService : ConfigurationService,
    private translateService     : TranslateService,
    public storage               : Storage,
    private currentEnvironment   : ConfigGlobalModel,
    private router               : Router,
    private localNotifications   : LocalNotifications,
    private ngZone: NgZone,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      this.statusBar.backgroundColorByHexString('#54C6D3');
      this.statusBar.styleLightContent();
      this.statusBar.overlaysWebView(false);
      this.splashScreen.hide();
      this.translateService.setDefaultLang('es');
      // Recover the selected environment or show the selector if there's none


      /* SIMULANDO PARA PRODUCCION*/
      this.storage.set(LocalStorageKeys.KEY_ENVIRONMENT, 'Production');
      await this.configurationService.loadJson('../assets/config/config.json').toPromise();
      this.currentEnvironment.config = this.configurationService.getEnvironmentInfo('Production');
      /* FIN DE SIMULANDO PARA PRODUCCCION*/

      /* DESCOMENTAR PARA DESARROLLO */
      // this.storage.get(LocalStorageKeys.KEY_ENVIRONMENT).then(async selectedEnv => {
      //   await this.configurationService.loadJson('../assets/config/config.json').toPromise();
      //   console.log('Current environment: ', selectedEnv);
      //   if (selectedEnv == null) {
      //     try {
      //     this.configurationService.presentEnvironmentSelector();
      //     } catch (err) {
      //       console.error(err);
      //     }
      //   }
      //   // Load in global in order to avoid multiple storage.gets through the app.
      //   this.currentEnvironment.config = this.configurationService.getEnvironmentInfo(selectedEnv);
      // });
       /* DESCOMENTAR PARA DESARROLLO */

      this.localNotifications.on('click').subscribe((action) => {
        this.navigateToCalendar(action);
      });
      this.localNotifications.on( 'trigger' ).subscribe( ( action ) => {
            const newParam = { ...action };
            newParam.data = { ...action.takeNotification, triggerDate: moment() };
            console.log('BOFORE SCHEDULING NEW NOTIFICATION', newParam);
            this.platform.is( 'ios' )
                ? MedNotifications.scheduleNotification( newParam )
                : this.localNotifications.update({
                    id: action.id,
                    data: { ...action.data.takeNotification, triggerDate: moment() },
                });
      });

    });
  }

    public navigateToCalendar( action ) {
      console.log('BEOFRE NAVIGATE', action);
      this.ngZone.run(() => {
      this.router.navigate(['/my-medication/reminders'], {
        queryParams: {
          data: this.platform.is('ios') ? JSON.stringify(action) : JSON.stringify(action.data)
        }
      });
    });
  }

}
