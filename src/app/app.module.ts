import { NgModule, LOCALE_ID } from '@angular/core';
import es from '@angular/common/locales/es';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HttpClientModule, HttpClient } from '@angular/common/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SuperTabsModule } from '@ionic-super-tabs/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ConfigurationService } from '../services/configuration.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { LoginService } from '../services/login.service';
import { AppointmentMockService } from 'src/services/appointment.mock.service';
import { IonicStorageModule } from '@ionic/storage';
import { LocalStorageKeys } from 'src/models/localstorage.keys.model';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallService } from 'src/services/call.service';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { registerLocaleData} from '@angular/common';
import { PushService } from 'src/services/push.service';
import { File } from '@ionic-native/file/ngx';
import { DatabaseService } from 'src/services/database.service';

import { SQLite } from '@ionic-native/sqlite/ngx';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

registerLocaleData(es);

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    SuperTabsModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: LOCALE_ID, useValue: 'es-ES'},
    ConfigurationService,
    ConfigGlobalModel,
    LocalStorageKeys,
    LoginService,
    AppointmentMockService,
    SocialSharing,
    InAppBrowser,
    CallService,
    PushService,
    File,
    DatabaseService,
    SQLite,
    LocalNotifications,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
