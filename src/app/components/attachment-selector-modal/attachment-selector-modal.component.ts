import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { MyMedAttachmentsService } from 'src/services/mymedattachments.service';
import { AttachmentViewerModalComponent } from '../attachment-viewer-modal/attachment-viewer-modal.component';
import { Subscription, fromEvent } from 'rxjs';

@Component({
  selector: 'app-attachment-selector-modal',
  templateUrl: './attachment-selector-modal.component.html',
  styleUrls: ['./attachment-selector-modal.component.scss'],
})
export class AttachmentSelectorModalComponent implements OnInit, OnDestroy {
  public attachmentList;
  private backButtonSubscription: Subscription;

  constructor(
    private navParams: NavParams,
    private attachmentsService: MyMedAttachmentsService,
    private modalCtlr: ModalController
  ) {
    this.attachmentList = navParams.get('data');
  }

  ngOnInit() {
    const event = fromEvent(document, 'backbutton');
    this.backButtonSubscription = event.subscribe(async () => {
      const modal = await this.modalCtlr.getTop();
      if (modal) {
        modal.dismiss();
      }
    });
  }

  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }

  async downloadAttachment(attachment) {
    this.attachmentsService.download(attachment);
    this.modalCtlr.dismiss();
  }

}
