import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { ShareService } from 'src/services/share.service';
import { Subscription, fromEvent } from 'rxjs';
import { File, IWriteOptions } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-attachment-viewer-modal',
  templateUrl: './attachment-viewer-modal.component.html',
  styleUrls: ['./attachment-viewer-modal.component.scss'],
})
export class AttachmentViewerModalComponent implements OnInit, OnDestroy {
  @Input() imageSrc: string;
  @Input() imageBlob: Blob;
  private backbuttonSubscription: Subscription;

  constructor(
    public navParams: NavParams,
    public modalCtlr: ModalController,
    public shareService: ShareService,
    private file: File
  ) { }

  ngOnInit() {
    this.imageSrc = this.navParams.get('imageUrl');
    this.imageBlob = this.navParams.get('imageBlob');
    const event = fromEvent(document, 'backbutton');
    this.backbuttonSubscription = event.subscribe(async () => {
      const modal = await this.modalCtlr.getTop();
      if (modal) {
        modal.dismiss();
      }
    });
  }

  ngOnDestroy() {
    this.backbuttonSubscription.unsubscribe();
  }

  onShareButtonClicked() {
    this.createImageToShareFromBlob(this.imageBlob, 'image.jpg');
  }

  async createImageToShareFromBlob(imageBlob: Blob, name: string) {
    const createFileOptions: IWriteOptions = {
      replace: true,
      append: false,
    };
    if (this.file.dataDirectory) {
      // * 1.- Create the image in local
      this.file.writeFile(this.file.dataDirectory, name, imageBlob, createFileOptions)
      .then((response) => {
        console.log('imagen creada:', response);
        console.log(response.nativeURL);
        const options = {
          subject: 'Imagen compartida a través de Bidafarma',
          files: [response.nativeURL]
        };
        this.shareService.shareWithParameters(options);
      })
      .catch((err) => {
        console.error(err);
      });
    }
  }

  closeModal = () => this.modalCtlr.dismiss();



}
