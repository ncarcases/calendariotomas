import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { BriefModel } from 'src/models/brief.model';
import { MyMedAttachmentsService } from 'src/services/mymedattachments.service';
import { AttachmentModel } from 'src/models/attachment.model';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { KeyValue } from '@angular/common';
import { Subscription, fromEvent } from 'rxjs';

@Component({
  selector: 'app-brief-details-modal',
  templateUrl: './brief-details-modal.component.html',
  styleUrls: ['./brief-details-modal.component.scss'],
})
export class BriefDetailsModalComponent implements OnInit, OnDestroy {
  brief: BriefModel;
  private backButtonSubscription: Subscription;
  public originalOrder = (a: KeyValue<number, string>, b: KeyValue<number, string>): number => {
    return 0;
  }

  constructor(
    private modalCtlr: ModalController,
    private params: NavParams,
    private attachmentsService: MyMedAttachmentsService,
    private currentConfig: ConfigGlobalModel
  ) {
    this.brief = this.params.data as BriefModel;
  }

  ngOnInit() {
    const event = fromEvent(document, 'backbutton');
    this.backButtonSubscription = event.subscribe(async () => {
      const modal = await this.modalCtlr.getTop();
      if (modal) {
        modal.dismiss();
      }
    });
  }

  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }
  getTime(dateString) {
    const date = new Date(dateString);
    // return a string with hour and minutes from date input
    return `${date.getHours()}:${date.getMinutes()}`;
  }

  closeModal() {
    this.modalCtlr.dismiss();
  }

  protected download(attachment: AttachmentModel): void {
    if (attachment) {
      attachment.idCard = this.currentConfig.config.codPaciente;
      this.attachmentsService.download(attachment);
    }
  }

  // private downloadMockDocumentToTest() {
  //   const response = 'Hola mundo';
  //   const contentType = 'text/html';
  //   const randomNumber = Math.floor(Math.random() * 100);
  //   const filename = `prueba${randomNumber}.txt`;
  //   this.attachmentsService.createDocumentFromBlob(response, contentType, filename);
  // }
}
