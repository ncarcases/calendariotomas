import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { AssesmentDetailsService } from '../../../services/assesment-details.service';
import { MapperRepository } from '../../../repositories/mapper.repository';
import { BriefDetailsModalComponent } from '../brief-details-modal/brief-details-modal.component';
import { Subscription, fromEvent } from 'rxjs';

@Component({
  selector: 'app-brief-selector-modal',
  templateUrl: './brief-selector-modal.component.html',
  styleUrls: ['./brief-selector-modal.component.scss'],
})
export class BriefSelectorModalComponent implements OnInit, OnDestroy {
  public briefList;
  private backButtonSubscription: Subscription;

  constructor(
    private navParams: NavParams,
    private modalCtlr: ModalController) {
      this.briefList = navParams.get('data');
  }

  ngOnInit() {
    const event = fromEvent(document, 'backbutton');
    this.backButtonSubscription = event.subscribe(async () => {
      const modal = await this.modalCtlr.getTop();
      if (modal) {
        modal.dismiss();
      }
    });
  }

  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }

  async showBriefDetails(brief) {
    const briefDetailsModal = await this.modalCtlr.create({
      component: BriefDetailsModalComponent,
      componentProps: brief
    });
    await briefDetailsModal.present();
  }
}
