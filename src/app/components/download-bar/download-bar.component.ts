import { Component, OnChanges, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { BriefDetailsModalComponent } from '../brief-details-modal/brief-details-modal.component';
import { ToastService } from 'src/services/toast.service';
import { TranslateService } from '@ngx-translate/core';
import { BriefModel } from 'src/models/brief.model';
import { MyMedAttachmentsService } from 'src/services/mymedattachments.service';
import { BriefSelectorModalComponent } from '../brief-selector-modal/brief-selector-modal.component';
import { AttachmentSelectorModalComponent } from '../attachment-selector-modal/attachment-selector-modal.component';
import { AssesmentDetailsService } from '../../../services/assesment-details.service';
import { MapperRepository } from 'src/repositories/mapper.repository';

@Component({
  selector: 'app-download-bar',
  templateUrl: './download-bar.component.html',
  styleUrls: ['./download-bar.component.scss'],
})

export class DownloadBarComponent implements OnChanges, OnInit {
  @Input() data: any;
  @Input() isLoading: boolean;
  @Input() isError: boolean;

  public numBriefs = null;
  public numAttachments = null;
  protected briefs: BriefModel[] = [];

  constructor(
    private modalCtlr: ModalController,
    private toast: ToastService,
    private translate: TranslateService,
    private dowloadAttachmentService: MyMedAttachmentsService,
    private assesmentDetailsService: AssesmentDetailsService,
    private mapper: MapperRepository
  ) {
    this.isError = false;
    this.isLoading = true;
  }

  ngOnInit() {}

  async ngOnChanges() {
    if (this.data) {
      // Process all the brief data to get details such as dates, etc. No further processing is needed to show the info
      this.briefs = await this.downloadAllBriefs(this.data);
      // These are only for the numbers of the badges
      // this.numBriefs = this.data.total;
      this.numBriefs = this.countBriefs(this.briefs);
      this.numAttachments = this.countAttachmentFiles(this.briefs);
      this.isLoading = false;
    }
  }

  async downloadAllBriefs(data: any) {
    const result: any = [];
    // Generates an error for cannot read property length of undefined
    if (!data.entry) { return; }
    // tslint:disable-next-line: prefer-for-of
    for (let index = 0; index < data.entry.length; index++) {
      if (data && data.entry[index].resource && data.entry[index].resource.id) {
        const briefDetails = await this.assesmentDetailsService.getBriefDetails(data.entry[index].resource.id);
        result.push(this.mapper.mapToBriefModel(briefDetails));
      }
    }
    return result;
  }

  countAttachmentFiles(briefs: BriefModel[]): number {
    let result = 0;
    if (briefs) {
      briefs.forEach(singleBrief => {
        if (singleBrief.status === 4) {
          result += singleBrief.attachment.length;
        }
      });
    }
    return result;
  }

  countBriefs(briefs: BriefModel[]): number {
    let result = 0;
    if (briefs) {
      briefs.forEach(singleBrief => {
        if (singleBrief.status === 4) {
          result++;
        }
      });
    }
    return result;
  }

  async showBriefs() {
    switch (this.numBriefs) {
      case 0:
        this.toast.presentToast(this.translate.instant('COMMON.NO-BRIEFS'), 4000);
        break;
      case 1:
        this.briefs.forEach(brief => {
          if (brief.status === 4) {
            this.presentBriefModal(brief);
          }
        });
        break;
      default:
        // We have all the data stored in the "briefs" variable, create a selector
        const briefSelectorModal = await this.modalCtlr.create({
          component: BriefSelectorModalComponent,
          componentProps: {
            data: this.briefs
          },
          cssClass: 'brief-selector-modal',
          showBackdrop: true
        });
        return await briefSelectorModal.present();
    }
  }

  async showAttachments() {
    switch (this.numAttachments) {
      case 0:
        this.toast.presentToast(this.translate.instant('COMMON.NO-ATTACHMENTS'), 5000);
        break;

      case 1:
        // As it's only one attachment, directly download it
        const selectedAttachment = this.briefs[0].attachment;
        this.dowloadAttachmentService.download(selectedAttachment[0]);
        break;

      default:
        // Get the all the attachments to show the selector and send one of them to download
        const listOfAttachments = [];
        this.briefs.forEach(brief => {
          if (brief.attachment) {
            brief.attachment.forEach(attachment => {
              if (brief.status === 4) {
                listOfAttachments.push(attachment);
              }
            });
          }
        });
        // After we have all the info of the attachments stored in an array, create and show a selector
        const attachmentSelectorModal = await this.modalCtlr.create({
          component: AttachmentSelectorModalComponent,
          componentProps: {
            data: listOfAttachments,
          },
          cssClass: 'attachment-selector-modal',
          showBackdrop: true
        });
        return await attachmentSelectorModal.present();
    }
  }

  async presentBriefModal(brief: BriefModel) {
    const briefDetailsModal = await this.modalCtlr.create({
      component: BriefDetailsModalComponent,
      componentProps: brief
    });
    await briefDetailsModal.present();
  }
}
