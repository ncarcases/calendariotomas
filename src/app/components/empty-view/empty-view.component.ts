import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-empty-view',
  templateUrl: './empty-view.component.html',
  styleUrls: ['./empty-view.component.scss'],
})
export class EmptyViewComponent implements OnInit {
  @Input() message: string;
  @Input() cssIconClass: string;
  @Input() componentHeight: any;
  height: string = null;

  constructor(
    public translate: TranslateService
  ) { }

  ngOnInit() {
    this.height = `${this.componentHeight}vh`;
  }
}
