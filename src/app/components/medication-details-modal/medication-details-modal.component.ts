import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Subscription, fromEvent } from 'rxjs';
import {
  MedicationState,
} from '../../models/medication';

@Component({
  selector: 'app-medication-details-modal',
  templateUrl: './medication-details-modal.component.html',
  styleUrls: ['./medication-details-modal.component.scss'],
})
export class MedicationDetailsModalComponent implements OnInit, OnDestroy {
  private backButtonSubscription: Subscription;
  public medicationDetails = null;

  public medicationState = MedicationState;

  constructor(
    private modalCtlr: ModalController,
    private navParams: NavParams,
  ) {}

  ngOnInit() {
    // Set the component to close with the device back button
    const backButtonEvent = fromEvent(window, 'popstate');
    this.backButtonSubscription = backButtonEvent.subscribe(() => this.modalCtlr.dismiss());
    // Get data from the parent component
    this.medicationDetails = this.navParams.get('medItem');
  }

  closeModal = () => this.modalCtlr.dismiss();

  public ngOnDestroy() {
    if (this.backButtonSubscription) {
      this.backButtonSubscription.unsubscribe();
    }
  }

}
