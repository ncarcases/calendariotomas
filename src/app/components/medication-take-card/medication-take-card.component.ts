import { Component, Input, Output, EventEmitter } from '@angular/core';

// Models
import {
  MedicationState,
  MedicationTake,
  MedicationTakeStatus,
} from '../../models/medication';

@Component({
  selector: 'app-medication-take-card',
  templateUrl: './medication-take-card.component.html',
  styleUrls: ['./medication-take-card.component.scss'],
})
export class MedicationTakeCardComponent {
  @Input() take: MedicationTake;
  @Output() public toSelectTake: EventEmitter<MedicationTake> = new EventEmitter();

  public medicationState = MedicationState;
  public state: string;

  public medicationTakeStatus = MedicationTakeStatus;

  public async showMedicationTakeInModal() {
    this.toSelectTake.emit(this.take);
  }

}
