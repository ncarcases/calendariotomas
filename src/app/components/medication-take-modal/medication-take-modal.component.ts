import { Component, OnInit, OnDestroy } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { fromEvent, Subscription } from 'rxjs';

// Models
import {
  MedicationTake,
  MedicationTakeStatus,
  MedicationTakeAction,
} from '../../models/medication';

@Component({
  selector: 'app-medication-take-modal',
  templateUrl: './medication-take-modal.component.html',
  styleUrls: ['./medication-take-modal.component.scss'],
})
export class MedicationTakeModalComponent implements OnInit, OnDestroy {

  public medication: MedicationTake = null;
  public medicationTakeStatus = MedicationTakeStatus;
  public backbuttonSubscription: Subscription;

  constructor(
    private modalCtlr: ModalController,
    private navParams: NavParams,
  ) { }


  public ngOnInit() {
    this.medication = this.navParams.get('take');
    const backButtonEvent = fromEvent(window, 'popstate');
    this.backbuttonSubscription = backButtonEvent.subscribe(() => this.modalCtlr.dismiss());

  }
  public toDismiss() {
    this.modalCtlr.dismiss(MedicationTakeAction.DISMISS);
  }
  public toTake() {
    this.modalCtlr.dismiss(MedicationTakeAction.TAKE);
  }
  public toDelete() {
    this.modalCtlr.dismiss(MedicationTakeAction.UNDO);
  }
  public toDestroy() {
    this.modalCtlr.dismiss(MedicationTakeAction.DELETE);
  }

  public ngOnDestroy() {
    if (this.backbuttonSubscription) {
      this.backbuttonSubscription.unsubscribe();
    }
  }

}
