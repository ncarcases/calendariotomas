import { Component, OnInit, Input } from '@angular/core';
import { AppointmentModel } from 'src/models/appointment.model';
import { ShareService } from 'src/services/share.service';
import { TranslateService } from '@ngx-translate/core';
import { MapsService } from 'src/services/maps.service';

@Component({
  selector: 'app-single-appointment',
  templateUrl: './single-appointment.component.html',
  styleUrls: ['./single-appointment.component.scss'],
})

export class SingleAppointmentComponent implements OnInit {
  @Input() appointment: AppointmentModel;

  constructor(
    private shareService: ShareService,
    private translate: TranslateService,
    private mapsService: MapsService
  ) { }

  ngOnInit() {
  }

  public onShareButtonClicked() {
    return this.shareService.share(this.mapAppointmentModel2Map());
  }

  public onMapClicked() {
    return this.mapsService.navigateToAddress(this.appointment.address);
  }

  private mapAppointmentModel2Map(): string[] {
    const result: string[] = [];
    const days = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
    const weekDay = this.appointment.date.getDay() - 1;
    if (weekDay === 0) {
      result.push(this.translate.instant('SHARE-SERVICE.SHARE-DATE') + days[weekDay] + ' ' + this.appointment.date.toLocaleDateString() + ' ' + this.appointment.time);
    } else {
      result.push(this.translate.instant('SHARE-SERVICE.SHARE-DATE') + days[weekDay - 1] + ' ' + this.appointment.date.toLocaleDateString() + ' ' + this.appointment.time);
    }
    result.push(this.translate.instant('SHARE-SERVICE.SHARE-SPECIALTY') + this.appointment.speciality);
    result.push(this.translate.instant('SHARE-SERVICE.SHARE-HOSPITAL') + this.appointment.hospital);
    return result;
  }
}
