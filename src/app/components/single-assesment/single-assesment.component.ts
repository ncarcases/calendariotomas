import { Component, OnInit, Input } from '@angular/core';
import { AssesmentModel } from 'src/models/assesment.model';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { AppointmentExtensionMockService } from 'src/services/appointmentextension.mock.service';
import { AssesmentDetailsService } from 'src/services/assesment-details.service';

@Component({
  selector: 'app-single-assesment',
  templateUrl: './single-assesment.component.html',
  styleUrls: ['./single-assesment.component.scss'],
})
export class SingleAssesmentComponent implements OnInit {
  @Input() medicalAssesment: AssesmentModel;
  protected downloadBarData: any = null; // was AppointmentExtensionModel but changed to any by the moment
  protected isDownloadBarError = false;
  protected isDownloadBarLoading = false;

  downloadBarVisible = false;

  constructor(
    private currentEnvironment: ConfigGlobalModel,
    private appointmentExtensionMockService: AppointmentExtensionMockService,
    private assesmentDetailsService: AssesmentDetailsService
    ) {}

  ngOnInit() {
  }

  async toggleDownloadBar() {
    this.downloadBarVisible = !this.downloadBarVisible;
    // From here, do things only if downloadBar is visible
    if (this.downloadBarVisible) {
      // downloadBarData variable stores the information, so we only ask for it in case it hasn't been downloaded yet
      if (!this.downloadBarData) {
        try {
          this.isDownloadBarLoading = true;
          if (this.currentEnvironment && this.currentEnvironment.config.baseUrl) {
            // Call the service to get details
            this.downloadBarData = await this.assesmentDetailsService.getAssesmentDetails(this.medicalAssesment.id);
            // Should remove here the loading status to the bar, but it's now set to receive a true value for the boolean isLoading and set it to false inside the own download bar component ngOnChanges lifecycle method
          } else {
            this.loadMockAssesmentsData();
            this.isDownloadBarLoading = false;
          }
        } catch (err) {
          console.error(JSON.stringify(err));
          this.isDownloadBarLoading = false;
          this.isDownloadBarError = true;
        }
      }
    }
  }

  loadMockAssesmentsData() {
    setTimeout(() => this.downloadBarData = this.appointmentExtensionMockService.getRandomAppointmentExtensionData(), 2000);
  }
}
