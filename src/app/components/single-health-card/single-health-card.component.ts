import { Component, OnChanges, OnInit, Input, Pipe } from '@angular/core';
import { HealthCardModel } from '../../../models/health-card.model';
import { CustomDatePipe } from '../../../modules/shared/pipes/custom-date.pipe';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-single-health-card',
  templateUrl: './single-health-card.component.html',
  styleUrls: ['./single-health-card.component.scss'],
})
export class SingleHealthCardComponent implements OnChanges, OnInit {

  @Input() item: HealthCardModel;

  constructor(
    private translate: TranslateService
  ) {}

  ngOnInit() {}

  ngOnChanges() {}

  formatDate(date) {
    // this helper function takes a date object and makes it shorter according to his main function or returns a string if date argument is not provided.
    const customDatePipe = new CustomDatePipe(this.translate);
    return date ? customDatePipe.transform(date, 'short') : this.translate.instant('BASIC-DATA.COMMON.NO-UPDATE-DATE');
  }
}
