import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { AlertService } from 'src/services/alert.service';

// Models
import {
  MedicationState,
  SwitchState,
  Medication,
} from '../../models/medication';

@Component({
  selector    : 'app-single-med-card',
  templateUrl : './single-med-card.component.html',
  styleUrls   : ['./single-med-card.component.scss'],
})
export class SingleMedCardComponent implements OnInit {
  @Input() medItem: Medication;
  @Output() switchOn: EventEmitter<Medication> = new EventEmitter();
  @Output() switchOff: EventEmitter<Medication> = new EventEmitter();
  @Output() showModal: EventEmitter<Medication> = new EventEmitter();

  public active = false;
  public toggleValue = false;

  public medicationState = MedicationState;
  public switchState = SwitchState;

  constructor(
    private translate        : TranslateService,
    private alertSrvc        : AlertService,
  ) {
  }

  public ngOnInit() {
    this.active = this.medItem.toggleNotificationButtonState === this.switchState.ON && this.medItem.textStatus === MedicationState.ACTIVO;

    this.toggleValue = this.active;
  }

  // * Método no relacionado con recordatorios, simplemente muestra la modal con los detalles de la medicación
  public showMedicationDetailsInModal(medItem: Medication) {
    this.showModal.emit(medItem);
  }

  // * Reacciona al toggle de activar o desactivar los recordatorios, actualiza la base de datos local con el valor del estado (on/off) si es un móvil y dispara las acciones de crear o eliminar los recordatorios
  async toggleReminders(event) {
    if (this.toggleValue === event.detail.checked) {
      return;
    }
    switch (event.detail.checked) {
      case true:
        this.switchOn.emit(this.medItem);
        this.toggleValue = true;
        break;

      case false:
        await this.alertSrvc.presentAlertWithButtons(
          null,
          this.translate.instant('MY-MEDICATION.ALERT_SUSPEND'),
          [{
            text: this.translate.instant('MY-MEDICATION.CANCEL'),
            handler: () => {
              this.toggleValue = true;
              this.active = true;
            }
          }, {
            text: this.translate.instant('MY-MEDICATION.SUSPEND'),
            handler: () => {
              this.switchOff.emit(this.medItem);
            }
          }],
          null,
          'single-med-card-alert',
          false,
        );
        this.toggleValue = false;

        break;
    }
  }
}

