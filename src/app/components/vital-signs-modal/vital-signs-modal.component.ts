import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';
import { VitalSignsModel, VitalSignsMeasures } from 'src/models/vital-signs.model';
import { AlertService } from 'src/services/alert.service';
import { Subscription, fromEvent } from 'rxjs';

@Component({
  selector: 'app-vital-signs-modal',
  templateUrl: './vital-signs-modal.component.html',
  styleUrls: ['./vital-signs-modal.component.scss'],
})
export class VitalSignsModalComponent implements OnInit, OnDestroy {
  public vitalSignsList: VitalSignsModel;
  public vitalSignsArray: VitalSignsMeasures[];
  private backButtonSubscription: Subscription;
  constructor(
    public navParams: NavParams,
    public modalCtlr: ModalController,
    public alert: AlertService
  ) {
    this.vitalSignsList = this.navParams.get('data');
  }

  ngOnInit() {
    this.vitalSignsArray = this.vitalSignsList.entries;
    const event = fromEvent(document, 'backbutton');
    this.backButtonSubscription = event.subscribe(async () => {
      const modal = this.modalCtlr.getTop();
      if (modal) {
        this.modalCtlr.dismiss();
      }
    });
  }

  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }

  closeModal() {
    this.modalCtlr.dismiss();
  }
}
