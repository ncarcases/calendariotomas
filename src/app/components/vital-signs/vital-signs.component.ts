import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { VitalSignsModel } from 'src/models/vital-signs.model';
import { ModalController } from '@ionic/angular';
import { VitalSignsModalComponent } from '../vital-signs-modal/vital-signs-modal.component';
import { TranslateService } from '@ngx-translate/core';
import { CustomDatePipe } from '../../../modules/shared/pipes/custom-date.pipe';

@Component({
  selector: 'app-vital-signs',
  templateUrl: './vital-signs.component.html',
  styleUrls: ['./vital-signs.component.scss'],
})
export class VitalSignsComponent implements OnInit {
  @Input() vitalSignsList: VitalSignsModel;
  vitalSignsShortList = null;
  styles = null;

  constructor(
    public modalCtlr: ModalController,
    public translate: TranslateService
  ) { }

  ngOnInit() {
    // Show only the first values in the main view, the rest of them should be presented in a modal
    this.vitalSignsShortList = this.vitalSignsList.entries[0] || null;
    this.styles = {
      background: {
       'background-color': this.vitalSignsList.cssBackgroundColor,
      },
      icon: {
        'color': this.vitalSignsList.cssIconColor,
      }
    };
  }

  async presentModalWithFullData() {
    const modal = await this.modalCtlr.create({
      component: VitalSignsModalComponent,
      componentProps: {
        data: this.vitalSignsList
      }
    });
    return await modal.present();
  }

  formatDate(date) {
    // This helper function takes a date object and makes it shorter according to his main function or returns a string if date argument is not provided.
    const customDatePipe = new CustomDatePipe(this.translate);
    return date ? customDatePipe.transform(date, 'short') : this.translate.instant('BASIC-DATA.COMMON.NO-UPDATE-DATE');
  }
}
