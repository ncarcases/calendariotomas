import { error } from 'util';

export const browserDBInstance = (db) => {
    return {
        executeSql: (sql, data?) => {
            return new Promise((resolve, reject) => {
                db.transaction((tx) => {
                    tx.executeSql(
                        sql,
                        data ? data : [],
                        (tx, rs) => {
                            resolve(rs);
                        },
                        (tx, err) => {
                            reject(err);
                        }
                    );
                    // error(err => {
                    //     if (err) {
                    //         reject(err);
                    //     }
                    // });
                });
            });
        },
        sqlBatch: (arr) => {
            return new Promise((r, rr) => {
                const batch = [];
                db.transaction((tx) => {
                    for (let i = 0; i < arr.length; i++) {
                        batch.push(
                            new Promise((resolve, reject) => {
                                tx.executeSql(arr[i], [], () => {
                                    resolve(true);
                                });
                            })
                        );
                        Promise.all(batch).then(() => r(true));
                    }
                });
            });
        },
    };
};
