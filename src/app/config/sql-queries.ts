import * as moment from 'moment';
export function createTablesQueries(tableName) {
    let sqlQuery = '';
    const today = moment().format('DD/M/YYYY');
    switch (tableName) {
        // Tabla de medicinas
        case  'medicines':
            sqlQuery = `CREATE TABLE IF NOT EXISTS medicines(
                medicationId TEXT PRIMARY KEY,
                codPaciente TEXT NOT NULL,
                name TEXT,
                hasReminders TEXT,
                reminderId TEXT,
                startDate TEXT,
                endDate TEXT,
                durationInDays INTEGER,
                toggleNotificationButtonState TEXT NOT NULL,
                remoteNotificationActive TEXT,
                frequency TEXT,
                frequencyDescription TEXT,
                frequencyInterval TEXT,
                frequencyValues TEXT,
                frequencyString TEXT,
                textStatus TEXT,
                numberDailyTakeDescription TEXT,
                takeIdsAsString TEXT
              )`;
            break;
        // Tabla de tómas
        case 'takes':
            sqlQuery = `CREATE TABLE IF NOT EXISTS takes(
                id INTEGER PRIMARY KEY,
                medicationId TEXT,
                codPaciente TEXT NOT NULL,
                quantity TEXT,
                time TEXT,
                posology TEXT,
                FOREIGN KEY(medicationId) REFERENCES medicines(medicationId)
              )`;
            break;
    }

    return sqlQuery;
}







