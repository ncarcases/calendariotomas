import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot,
} from '@angular/router';

import { AuthService } from '../../services/auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    public auth: AuthService,
    public router: Router,
    public route: ActivatedRoute,
  ) {}

  public async canActivate(route: ActivatedRouteSnapshot) : Promise<boolean> {
    const isAuthorize = await this.auth.isAuthenticated();
    if (!isAuthorize) {
      this.router.navigate(['/login'], { queryParams: route.queryParams });
      return false;
    } else {
      return true;
    }
  }
}
