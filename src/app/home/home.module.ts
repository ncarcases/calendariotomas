import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'src/modules/shared/shared.module';
import { LogoutService } from '../../services/logout.service';
import { FileOpener } from '@ionic-native/file-opener/ngx';

import { HomePage } from './home.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RouterModule.forChild([{
      path: '',
      component: HomePage
    }])
  ],
  declarations: [
    HomePage
  ],
  providers: [
    LogoutService,
    FileOpener,
  ]
})
export class HomePageModule {}
