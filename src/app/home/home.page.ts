import { Component, ViewChild } from '@angular/core';
import { IonContent, AlertController } from '@ionic/angular';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { TranslateService } from '@ngx-translate/core';
import { LogoutService } from 'src/services/logout.service';
import { AlertService } from 'src/services/alert.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  // Capture the DOM element to control scroll in component load
  @ViewChild(IonContent) content: IonContent;

  constructor(
    private config: ConfigGlobalModel,
    private logoutService: LogoutService,
    public translate: TranslateService,
    public alertSrvc: AlertService,
    public alertCtlr: AlertController,
  ) { }

  public ionViewDidEnter() {
    // * Check if the user has a profile in MyMed Bidafarma services and show a message if not
    if (this.config.config.codPaciente === null) {
      this.alertSrvc.presentAlert(
        this.translate.instant('LOGIN.NO-REGISTERED-USER-HEADER'),
        this.translate.instant('LOGIN.NO-REGISTERED-USER-MSG'),
        [this.translate.instant('LOGIN.NO-REGISTERED-USER-BUTTONS.OK')]
      );
    }

  }
  onLogoutButtonClicked = () => this.logoutService.logout();
  scrollToTopOnPageLoad = () => this.content.scrollToTop();
}
