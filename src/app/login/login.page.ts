import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { LoginService } from 'src/services/login.service';
import { LoginModel } from 'src/models/login.model';
import { NavController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { ToastService } from 'src/services/toast.service';
import { Storage } from '@ionic/storage';
import { LocalStorageKeys } from 'src/models/localstorage.keys.model';
import { AlertService } from 'src/services/alert.service';
import { MedicationRemindersService } from 'src/services/medications/medication.reminders.service';
import {
  Router,
  ActivatedRoute,
} from '@angular/router';
import { take } from 'rxjs/operators';
import { MedicationService } from '../../services/medications/medication.service';
import { PushService } from 'src/services/push.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup = null;
  user = null;
  passwordIsVisible = false;

  private fromPushNotification = false;

  constructor(
    public formBuilder: FormBuilder,
    private loginService: LoginService,
    private navCtl: NavController,
    public translate: TranslateService,
    public toast: ToastService,
    public storage: Storage,
    private alertService: AlertService,
    private currentConfig: ConfigGlobalModel,
    private remindersSrvc: MedicationRemindersService,
    public router: Router,
    public route: ActivatedRoute,
    private pushService: PushService,
    private medicationSrvc: MedicationService
  ) {
    // * Initialize form upon page creation
    this.loginForm = this.formBuilder.group({
      mail: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    // * Retrieve value from Storage to avoid the need to type it again
    this.storage.get(LocalStorageKeys.KEY_USERNAME).then(value => {
      this.user = value;
      this.loginForm.setValue({
        mail: value,
        password: ''
      });
    });
  }

  public ionViewWillEnter() {
    this.passwordIsVisible = false;
  }

  public forgetUser() { }

  public userChange() {
    // * Request confirmation
    const buttons = [{
      text: this.translate.instant('LOGOUT.YES'),
      handler: () => this.deleteUserInfo()
    }, {
      text: this.translate.instant('LOGOUT.NO'),
      role: 'cancel',
      handler: () => null
    }];
    this.alertService.presentAlertWithButtons(
      this.translate.instant('COMMON.CONFIRMATION-TITLE'),
      this.translate.instant('LOGIN.CHANGE-USER-CONFIRM'),
      buttons
    );
  }

  async onLogin() {
    // * Save mail as username in IonicStorage to avoid user to type it
    this.storage.set(LocalStorageKeys.KEY_USERNAME, this.loginForm.value.mail);
    await this.alertService.presentLoadingDialog(this.translate.instant('LOGIN.LOGIN-IN-PROGRESS'));
    try {
      const data: LoginModel = new LoginModel();
      data.mail = this.loginForm.value.mail;
      data.password = this.loginForm.value.password;

      // * Start login
      const response: any = await this.loginService.login(data);
      if (response.error && 
          response.error.token && 
          response.error.token.error &&
          response.error.token.data.message === 'Paciente no encontrado') {
        this.showLoginErrorMessagesNoMyMed();
      } else if (response.error) {
        this.alertService.dismissLoadingDialog();
        // console.log('error =>', response.error.error);
        // * Deal with the error to show the right message to the user
        this.showLoginErrorMessages(response.error.error);
      } else if (response) {
        // * login ok, save token, codPaciente and mail in config file
        this.currentConfig.config.token = response.token;
        this.currentConfig.config.codPaciente = response.idPaciente;
        this.currentConfig.config.mail = response.mail;
        // TODO: guardar esta info en el storage
        this.storage.set('mail', response.mail);
        this.storage.set('codPaciente', response.idPaciente);
        this.storage.set('token', response.token);
        await this.checkLastUserLogged();

        // * Remove loading banner and redirect to home
        this.alertService.dismissLoadingDialog();

        // Set FirebaseToken
        if (window.hasOwnProperty('cordova')) {
          this.activateFirebaseToken();
        }
        this.medicationSrvc.fetchMedication().then((medications) => {
          this.toEnter();
        }, error => {
          this.toEnter();
        });
      } else {
        this.alertService.dismissLoadingDialog();
        this.alertService.presentAlertWithButtons(
          this.translate.instant('COMMON.ERROR'),
          this.translate.instant('COMMON.ERROR-LONG'),
          [this.translate.instant('COMMON.OK')]
        );
      }
    } catch (err) {
      console.error(JSON.stringify(err));
      this.alertService.dismissLoadingDialog();
      this.alertService.presentAlertWithButtons(
        this.translate.instant('COMMON.ERROR'),
        this.translate.instant('COMMON.ERROR-LONG'),
        [this.translate.instant('COMMON.OK')]
      );
    }
  }

  private toEnter() {
    this.route.queryParams.pipe(take(1)).subscribe(params => {
      console.warn(params, 'PARAMETROS EN EL LOGIN DE UNA NOTIFICACION');
      if (params && params.data) {
        this.router.navigate(['/my-medication/reminders'], { queryParams: params });
      } else {
        this.router.navigateByUrl('/home');
      }
    });
  }

  showLoginErrorMessages(errorStr) {
    if (errorStr.substring(0, 12) === 'no such user') {
      this.alertService.dismissLoadingDialog();
      this.alertService.presentAlertWithButtons(
        this.translate.instant('COMMON.ERROR'),
        this.translate.instant('LOGIN.LOGIN-ERRORS.NO-SUCH-USER'),
        [this.translate.instant('COMMON.OK')]
      );
    } else if (errorStr === 'Invalid Credentials') {
      this.alertService.dismissLoadingDialog();
      this.alertService.presentAlertWithButtons(
        this.translate.instant('COMMON.ERROR'),
        this.translate.instant('LOGIN.LOGIN-ERRORS.INVALID-CREDENTIALS'),
        [this.translate.instant('COMMON.OK')]
      );
    }
  }

  private showLoginErrorMessagesNoMyMed() {
    this.alertService.dismissLoadingDialog();
    this.alertService.presentAlertWithButtons(
      this.translate.instant('COMMON.ERROR'),
      this.translate.instant('LOGIN.LOGIN-ERRORS.NO-MYMED'),
      [this.translate.instant('COMMON.OK')]
    );
  }

  async checkLastUserLogged() {
    await this.storage.get('LAST_USER').then(async user => {
      const lastUser = JSON.parse(user);
      if (lastUser && this.currentConfig.config.codPaciente !== lastUser.codPaciente) {

        this.removeToken(lastUser.mail);
        this.showInfoToast();

        this.remindersSrvc.refreshReminders();

        await this.storage.set('LAST_USER', JSON.stringify(this.currentConfig.config));
      }
      await this.storage.set('LAST_USER', JSON.stringify(this.currentConfig.config));
    });
  }

  public showInfoToast() {
    this.alertService.presentToast(
      this.translate.instant('LOGIN.CHANGE_USER_TOAST'),
      null,
      'med-toast-info',
    );
  }

  deleteUserInfo() {
    // * Erase saved user data and selected user in storage
    this.user = null;
    this.storage.remove(LocalStorageKeys.KEY_USERNAME);
    // * Empty form fields
    this.loginForm.setValue({
      mail: '',
      password: '',
    });
    // * Set validator in form mail field when it's visible
    this.loginForm.get('mail').setValidators(Validators.required);
    // Deactivate reminders
    // this.medicationsSQLService.getSQLiteMedication()
    // .then(medicines => {
    //   medicines.forEach(medicine => {
    //     if (medicine.takeIdsAsString && medicine.takeIdsAsString.length > 0) {
    //       const ids = medicine.takeIdsAsString.split(',');
    //       ids.forEach(id => {
    //         this.remindersSrvc.findReminderById(Number(id))
    //         .then(res => {
    //           console.log('alerta desactivada');
    //           console.log(res);
    //         });
    //       });
    //     }
    //   });
    // });
    // * Finally, delete local DB
    // this.dbService.deleteTables();
  }

  public removeToken(mail) {
    this.pushService.removeToken(mail).pipe(take(1)).subscribe();
  }

  public activateFirebaseToken() {
    // this.firebaseTokenService.getToken(); // Captura notificaciones cuando está la aplicación corriendo
    this.pushService.initPushNotifications(); // Captura los eventos de los mensajes push
  }
}
