export interface Medication {
  medicationId: number;
  name: string;
  hasReminders: string;
  reminderId? : string;
  hasEndDate? : boolean;
  startDate?: string;
  endDate?: string;
  durationInDays?: number;
  toggleNotificationButtonState?: any;
  remoteNotificationActive?: string;
  frequency?: string;
  frequencyDescription?: string;
  frequencyInterval?: string;
  frequencyValues?: boolean[];
  frequencyString?: string;
  textStatus?: string;
  numberDailyTakeDescription?: string;
  takes?: MedicationTake[];
}

export enum MedicationState {
  ACTIVO = 'activo',
  SUSPENDIDO = 'suspendido',
  FINALIZADO = 'finalizado',
  SIN_RECORDATORIO = 'sin-recordatorio',
  ELIMINADO = 'eliminado',
}
export enum SwitchState {
  ON = 'on',
  OFF = 'off',
}
export enum ReminderState {
  YES = 'Yes',
  NO = 'No',
}

export enum MedicationPushState {
  DELETEPRESCRIPTION = 'DELETEPRESCRIPTION',
  UPDATEREMINDER = 'UPDATEREMINDER',
  CREATEREMINDER = 'CREATEREMINDER',
  DELETEREMINDER = 'DELETEREMINDER',
  UPDATEPRESCRIPTION = 'UPDATEPRESCRIPTION',
  CREATIONPRESCRIPTION = 'CREATIONPRESCRIPTION',
}

export enum MedicationFrequency {
  EVERYDAY = '1',
  ALTERNATING = '2',
  INTERVAL = '3',
}

export interface MedicationTake {
  codPaciente: string;
  id: number;
  medicationId: string;
  medicationName: string;
  posology: string;
  quantity: string;
  time: string;
  status?: MedicationTakeStatus;
  registerId?: number;
  medicationStatus?: MedicationState;
}

export enum MedicationTakeStatus {
  PENDING = 'pendiente',
  REJECTED = 'rechazada',
  TAKEN = 'tomada',
}
export enum MedicationTakeAction {
  DISMISS,
  TAKE,
  UNDO,
  DELETE
}

export const FILTER_BUTTONS = [{
    label: 'Activos',
    filterTag: 'activo',
    size: '2',
    fillProperty: 'outline'
  }, {
    label: 'Suspendidos',
    filterTag: 'suspendido',
    size: '3',
    fillProperty: 'outline'
  }, {
    label: 'Finalizados',
    filterTag: 'finalizado',
    size: '3',
    fillProperty: 'outline'
  }, {
    label: 'Sin recordatorio',
    filterTag: 'sin-recordatorio',
    size: '4',
    fillProperty: 'outline'
  }];

// HTTP REQUEST MODELS
export interface MedicationTakeRegisterRequest {
    resourceType: string;
    id?: string;
    fchToma: Date;
    fchAccionToma: Date;
    accion: number;
    codPaciente: string;
    codPrescripcion: number;
    codDefToma: number;
    activo?: boolean;
}
