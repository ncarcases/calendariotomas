import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from 'src/modules/shared/shared.module';

import { MyAppointmentsPage } from './my-appointments.page';
import { NextAppointmentsComponent } from './next-appointments/next-appointments.component';
import { PastAppointmentsComponent } from './past-appointments/past-appointments.component';
import { SingleAppointmentComponent } from '../components/single-appointment/single-appointment.component';
import { MapsService } from 'src/services/maps.service';
import { ShareService } from 'src/services/share.service';
import { AppointmentService } from 'src/services/appointment.service';


const routes: Routes = [
  {
    path: '',
    component: MyAppointmentsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [
    MyAppointmentsPage,
    NextAppointmentsComponent,
    PastAppointmentsComponent,
    SingleAppointmentComponent
  ],
  providers: [
    ShareService,
    MapsService,
    AppointmentService
  ]
})
export class MyAppointmentsPageModule {}
