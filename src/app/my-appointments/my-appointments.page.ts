import { Component, OnInit } from '@angular/core';
import { NextAppointmentsComponent } from './next-appointments/next-appointments.component';
import { SingleAppointmentComponent } from '../components/single-appointment/single-appointment.component';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-my-appointments',
  templateUrl: './my-appointments.page.html',
  styleUrls: ['./my-appointments.page.scss'],
})
export class MyAppointmentsPage implements OnInit {
  /* Property used to control which tabs is selected and load only in
  demand. By default, the view will load NextAppointments */
  selected = 'nextAppointments';

  constructor(private route: ActivatedRoute) {
    if (this.route && this.route.snapshot.paramMap.get('selectedTab')) {
      this.selected = this.route.snapshot.paramMap.get('selectedTab');
    }
  }

  ngOnInit() {
  }
}
