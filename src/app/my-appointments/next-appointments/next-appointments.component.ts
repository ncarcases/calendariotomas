import { Component, OnInit } from '@angular/core';
import { SingleAppointmentComponent } from '../../components/single-appointment/single-appointment.component';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { AppointmentModel } from 'src/models/appointment.model';
import { AppointmentMockService } from 'src/services/appointment.mock.service';
import { AppointmentService } from 'src/services/appointment.service';
import { MapperRepository } from 'src/repositories/mapper.repository';

@Component({
  selector: 'app-next-appointments',
  templateUrl: './next-appointments.component.html',
  styleUrls: ['./next-appointments.component.scss'],
})

export class NextAppointmentsComponent implements OnInit {
  public nextAppointmentElements: AppointmentModel[] = [];
  public isLoading = true;
  public isError = false;

  constructor(
    private currentEnvironment: ConfigGlobalModel,
    private appointmentMockService: AppointmentMockService,
    private appointmentService: AppointmentService,
    private mapper: MapperRepository) {
  }

  ngOnInit() {
    if (this.currentEnvironment.config.baseUrl) {
      this.appointmentService.getNextAppointments()
      .then(response => {
        this.nextAppointmentElements = response.entry ? this.mapper.mapToAppointmentModel(response, true) : [];
        this.isLoading = false;
      })
      .catch(err => {
        console.error(err);
        this.isLoading = false;
        this.isError = true;
      });
    } else {
      setTimeout(() => {
        this.nextAppointmentElements = this.appointmentMockService.getAppointments();
        this.isLoading = false;
      }, 3000);
    }
  }
}
