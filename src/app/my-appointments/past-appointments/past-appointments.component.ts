import { Component, OnInit } from '@angular/core';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { AppointmentMockService } from 'src/services/appointment.mock.service';
import { AppointmentService } from 'src/services/appointment.service';
import { AppointmentModel } from 'src/models/appointment.model';
import { MapperRepository } from 'src/repositories/mapper.repository';
import { GroupedAppointmentModel } from 'src/models/grouped-appointment.model';
import { GroupAppointmentModel } from 'src/models/group-appointment.model';

@Component({
  selector: 'app-past-appointments',
  templateUrl: './past-appointments.component.html',
  styleUrls: ['./past-appointments.component.scss'],
})

export class PastAppointmentsComponent implements OnInit {
  public pastAppointmentElements: AppointmentModel[] = [];
  public isLoading = true;
  public isError = false;

  constructor(
    private currentEnvironment: ConfigGlobalModel,
    private appointmentMockService: AppointmentMockService,
    private appointmentService: AppointmentService,
    private mapper: MapperRepository
  ) { }

  ngOnInit() {
    try {
      if (this.currentEnvironment.config.baseUrl) {
        this.appointmentService.getPastAppointments()
        .then(response => {
          this.pastAppointmentElements = response.entry ? this.mapper.mapToAppointmentModel(response, false) : [];
          this.isLoading = false;
        })
        .catch(err => {
          console.error(err);
          this.isLoading = false;
          this.isError = true;
        });
      } else {
        setTimeout(() => {
          this.pastAppointmentElements = this.appointmentMockService.getHistory();
          this.isLoading = false;
        }, 3000);
      }
    } catch (err) {
      console.error(JSON.stringify(err));
      this.isLoading = false;
      this.isError = true;
    }
  }
}
