import { Component, OnInit } from '@angular/core';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { AlertInput } from '@ionic/core';
import { AssessmentMockService } from 'src/services/assessment.mock.service';
import { AssesmentModel } from 'src/models/assesment.model';
import { AssesmentService } from 'src/services/assesment.service';
import { MapperRepository } from '../../../repositories/mapper.repository';
import { AlertController } from '@ionic/angular';
import { ToastService } from 'src/services/toast.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-assesments',
  templateUrl: './assesments.component.html',
  styleUrls: ['./assesments.component.scss'],
})
export class AssesmentsComponent implements OnInit {
  public assesmentElements: AssesmentModel[] = null;
  protected selectedAssesmentElement: string = null;
  public isLoading = false;
  public isError = false;
  public serviceTypesCount = new Set();

  constructor(
    private currentEnvironment: ConfigGlobalModel,
    private assesmentMockService: AssessmentMockService,
    private assesmentService: AssesmentService,
    private alertCtlr: AlertController,
    private toast: ToastService,
    private translate: TranslateService,
    private mapper: MapperRepository
  ) { }

  async ngOnInit() {
    try {
      this.isLoading = true;
      this.isError = false;
      if (this.currentEnvironment.config.baseUrl) {
        const data = await this.assesmentService.getmedicalAssestments();
        // Send only data to mapper if there's useful in the response. If not, just modify a variable to show a message.
        if (data.total > 0) {
          const mapeo = this.mapper.mapToAssesmentModel(data);
          this.assesmentElements = mapeo;
          // Find unique values to count it and decide wheter to show or not the filter button
          this.assesmentElements.forEach(assesmentElement => {
            this.serviceTypesCount.add(assesmentElement.serviceType);
          });
        }
        this.isLoading = false;
      }
    } catch (err) {
      console.error(JSON.stringify(err));
      this.isError = true;
      this.isLoading = false;
    }
  }

  async filterAssesmentsByType() {
    const assesmentsTypes = [];
    // * Put all the different categories in an array
    this.assesmentElements.forEach(assesment => {
      assesmentsTypes.push(assesment.serviceType);
    });
    // * find unique values in array with a set to show a list for the user to choose
    const distinctTypes = new Set(assesmentsTypes);

    if (distinctTypes.size === 1) {
      const assesmentElementArray = Array.from(distinctTypes.entries());
      this.toast.presentToast(this.translate.instant('MEDICAL-ASSESSTMENT.FILTER.MESSAGE'), 5000);
    } else {
      const optionSelector = await this.alertCtlr.create({
        header: this.translate.instant('MEDICAL-ASSESSTMENT.FILTER.SELECTOR-HEADER'),
        message: this.translate.instant('MEDICAL-ASSESSTMENT.FILTER.SELECTOR-MESSAGE'),
        inputs: this.getRadioListForSelector(distinctTypes),
        buttons: [{
          text: this.translate.instant('MEDICAL-ASSESSTMENT.FILTER.DELETE-FILTER'),
          handler: () => this.selectedAssesmentElement = null
        }, {
          text: this.translate.instant('MEDICAL-ASSESSTMENT.FILTER.SELECT-OPTION'),
          handler: (option) => this.selectedAssesmentElement = option
        }]
      });
      optionSelector.present();
    }
  }

  getRadioListForSelector(typesList) {
    const buttons: AlertInput[] = [];
    typesList.forEach(type => {
      const button: any = {
        type: 'radio',
        name: type,
        value: type,
        label: type,
        disabled: false,
      };
      buttons.push(button);
    });
    return buttons;
  }

  loadMockAssesmentsData = () => this.assesmentElements = this.assesmentMockService.getMedicalAssesment();

}
