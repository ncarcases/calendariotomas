import { Component, OnInit } from '@angular/core';
import { HealthCardModel } from '../../../models/health-card.model';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { AllergyMockService } from 'src/services/allergy.mock.service';
import { ContraindicationMockService } from 'src/services/contraindication.mock.service';
import { VitalSignsModel } from 'src/models/vital-signs.model';
import { VitalSignsMockService } from 'src/services/vital-signs.mock.service';
import { AllergyService } from 'src/services/allergy.service';
import { ContraindicationsService } from 'src/services/contraindications.service';
import { VitalSignsService } from 'src/services/vital-signs.service';
import { TranslateService } from '@ngx-translate/core';
import { MapperRepository } from 'src/repositories/mapper.repository';

@Component({
  selector: 'app-basics',
  templateUrl: './basics.component.html',
  styleUrls: ['./basics.component.scss'],
})
export class BasicsComponent implements OnInit {
  // May need to update VitalSignsModel or set the type of collection property to any;
  protected vitalSignItems: VitalSignsModel = null;
  public collection: HealthCardModel[] = [];
  public isLoading = false;
  public isError = null;

  public allergyOptions = {
    category: this.translate.instant('BASIC-DATA.ALLERGIES.LABEL'),
    cssClassIcon: 'icon-bida-alergias',
    keyword: 'alergias',
    cssBackgroundColor: 'rgba(47, 165, 226, 0.1)',
    cssIconColor: 'rgb(47, 165, 226)'
  };
  public contraindicationsOptions = {
    category: this.translate.instant('BASIC-DATA.CONTRAINDICATIONS.LABEL'),
    cssClassIcon: 'icon-bida-contraindicaciones',
    keyword: 'contraindicaciones',
    cssBackgroundColor: 'rgba(253, 141, 154, 0.1)',
    cssIconColor: 'rgb(253, 141, 154)'
  };
  public vitalSignsOptions = {
    category: this.translate.instant('BASIC-DATA.VITAL-SIGNS.LABEL'),
    cssClassIcon: 'icon-bida-constantes',
    cssBackgroundColor: 'rgba(171,72,229,0.1)',
    cssIconColor: 'rgb(171,72,229)',
  };

  constructor(
    private currentEnvironment: ConfigGlobalModel,
    private allergyMockService: AllergyMockService,
    private contraindicationMockService: ContraindicationMockService,
    private vitalSignsMockService: VitalSignsMockService,
    private allergyService: AllergyService,
    private contraindicationsService: ContraindicationsService,
    private vitalSignsService: VitalSignsService,
    public translate: TranslateService,
    private mapper: MapperRepository) { }

  async ngOnInit() {
    // set flags for template conditional rendering
    this.isLoading = true;
    if (this.currentEnvironment.config.baseUrl) {
      // call functions wich request data from services
      this.allergyService.getAllegies()
      .then(allergiesResponse => {
        this.collection.push(this.mapper.mapToHealthCardModel(allergiesResponse, this.allergyOptions));
        this.contraindicationsService.getContraindications()
        .then((contraindicationsResponse) => {
          this.collection.push(this.mapper.mapToHealthCardModel(contraindicationsResponse, this.contraindicationsOptions));
          this.vitalSignsService.getVitalSigns()
          .then((vitalSignsResponse) => {
            this.vitalSignItems = this.mapper.mapToVitalSignsModel(vitalSignsResponse, this.vitalSignsOptions);
          })
          .catch(err => this.setFlagsToError())
          .finally(() => {
            this.isLoading = false;
          });
        })
        .catch(err => this.setFlagsToError());
      })
      .catch(err => this.setFlagsToError());
    } else {
      this.loadMockData('alergias');
      this.loadMockData('contraindicaciones');
      this.loadMockData('constantes');
    }
  }

  setFlagsToError() {
    this.isError = true;
    this.isLoading = false;
  }

  loadMockData(category) {
    setTimeout(() => {
      switch (category) {
        case 'alergias':
          const auxAllergies = this.allergyMockService.getAllegies();
          this.collection.push(auxAllergies);
          break;

        case 'contraindicaciones':
          const auxContraindications = this.contraindicationMockService.getContraindications();
          this.collection.push(auxContraindications);
          break;

        case 'constantes':
          const auxVitalSigns = this.vitalSignsMockService.getVitalSigns();
          this.vitalSignItems = auxVitalSigns;
      }
      this.isLoading = false;
    }, 2000);
  }
}
