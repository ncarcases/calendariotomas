import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { MyHistoryPage } from './my-history.page';
import { SharedModule } from 'src/modules/shared/shared.module';
import { AssesmentsComponent } from './assesments/assesments.component';
import { BasicsComponent } from './basics/basics.component';
import { SingleAssesmentComponent} from '../components/single-assesment/single-assesment.component';
import { SingleHealthCardComponent } from '../components/single-health-card/single-health-card.component';
import { VitalSignsComponent } from '../components/vital-signs/vital-signs.component';
import { VitalSignsModalComponent } from '../components/vital-signs-modal/vital-signs-modal.component';
import { DownloadBarComponent } from '../components/download-bar/download-bar.component';
import { BriefDetailsModalComponent } from '../components/brief-details-modal/brief-details-modal.component';
import { BriefSelectorModalComponent } from '../components/brief-selector-modal/brief-selector-modal.component';
import { AssessmentMockService } from 'src/services/assessment.mock.service';
import { AllergyMockService } from 'src/services/allergy.mock.service';
import { AllergyService } from 'src/services/allergy.service';
import { ContraindicationMockService } from 'src/services/contraindication.mock.service';
import { VitalSignsMockService } from 'src/services/vital-signs.mock.service';
import { AppointmentExtensionMockService } from 'src/services/appointmentextension.mock.service';
import { MyMedAttachmentsService } from 'src/services/mymedattachments.service';
import { ShareService } from 'src/services/share.service';
import { MapsService } from 'src/services/maps.service';
import { WebNavigationService } from 'src/services/webnavigation.service';
import { MapperRepository } from 'src/repositories/mapper.repository';
import { ContraindicationsService } from 'src/services/contraindications.service';
import { AttachmentSelectorModalComponent } from '../components/attachment-selector-modal/attachment-selector-modal.component';
import { AttachmentViewerModalComponent } from '../components/attachment-viewer-modal/attachment-viewer-modal.component';
import { FileOpener } from '@ionic-native/file-opener/ngx';

const routes: Routes = [
  {
    path: '',
    component: MyHistoryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  entryComponents: [
    VitalSignsModalComponent,
    BriefDetailsModalComponent,
    BriefSelectorModalComponent,
    AttachmentSelectorModalComponent,
    AttachmentViewerModalComponent
  ],
  declarations: [
    MyHistoryPage,
    AssesmentsComponent,
    BasicsComponent,
    SingleAssesmentComponent,
    SingleHealthCardComponent,
    VitalSignsComponent,
    VitalSignsModalComponent,
    DownloadBarComponent,
    BriefDetailsModalComponent,
    BriefSelectorModalComponent,
    AttachmentSelectorModalComponent,
    AttachmentViewerModalComponent
  ],
  providers: [
    AssessmentMockService,
    AllergyMockService,
    AllergyService,
    ContraindicationMockService,
    ContraindicationsService,
    VitalSignsMockService,
    AppointmentExtensionMockService,
    MyMedAttachmentsService,
    ShareService,
    MapsService,
    WebNavigationService,
    MapperRepository,
    FileOpener
  ]
})
export class MyHistoryPageModule {}
