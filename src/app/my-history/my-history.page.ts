import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-my-history',
  templateUrl: './my-history.page.html',
  styleUrls: ['./my-history.page.scss'],
})
export class MyHistoryPage implements OnInit {
  /* Property used to control which tabs is selected and load only in
  demand. By default, the view will load Valoraciones */
  selected = 'medicalAssessments';

  constructor(private route: ActivatedRoute) {
    if (this.route && this.route.snapshot.paramMap.get('selectedTab')) {
      this.selected = this.route.snapshot.paramMap.get('selectedTab');
    }
  }

  ngOnInit() {
  }
}
