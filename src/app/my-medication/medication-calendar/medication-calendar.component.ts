import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { ModalController } from '@ionic/angular';
import { MedicationTakeModalComponent } from '../../components/medication-take-modal/medication-take-modal.component';
import { AlertService } from '../../../services/alert.service';
import { MedicationSQLService } from '../../../services/medications/medication.sqlservice';
import { TakesService } from 'src/services/takes/takes.services';
import { TakesSQLService } from '../../../services/takes/takes.sqlservice';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { MedicationService } from 'src/services/medications/medication.service';
import { mapToMedicationModel } from 'src/services/medications/utils/medication-mapper-model';
import { setMedicineStatus } from '../../../services/medications/utils/set-status-medicine';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

// Models
import {
  MedicationTake,
  MedicationTakeStatus,
  MedicationTakeRegisterRequest,
  MedicationTakeAction,
  MedicationState,
} from '../../models/medication';

@Component({
  selector: 'app-medication-calendar',
  templateUrl: './medication-calendar.component.html',
  styleUrls: ['./medication-calendar.component.scss'],
})
export class MedicationCalendarComponent implements OnInit {
  public today         : string = null;
  public selectedDay   : any = null;
  public todayInNumber : number = null;
  public todayString   : string = null;
  public dates = [];
  public medicines = null;
  public medicinesForToday = [];
  public week = [];

  public touchStartClientX = null;
  public touchDirection = 0;

  public selectCircle: any;

  public takes: MedicationTake[] = [];

  public takesOfTheWeek = [];

  public isLoading = true;

  public sortByHour = (take1, take2) => {
    const arr1: string[] = take1.time.split(':');
    const arr2: string[] = take2.time.split(':');

    const firtsDateTime = new Date(0, 0, 0, parseInt(arr1[0], 10), parseInt(arr1[1], 10)).getTime();
    const secondDateTime = new Date(0, 0, 0, parseInt(arr2[0], 10), parseInt(arr2[1], 10)).getTime();

    return firtsDateTime - secondDateTime;
  }

  constructor(
    private medicationSQLService: MedicationSQLService,
    private medicationSrvc     : MedicationService,
    private takesService: TakesService,
    private takesSQLService: TakesSQLService,
    private translate: TranslateService,
    private modalCtlr: ModalController,
    private alertSrvc: AlertService,
    protected currentConfig: ConfigGlobalModel,
    private activatedroute: ActivatedRoute,
    private location: Location,
  ) { }

  async ngOnInit() {
    this.isLoading = true;

    for (let weekDay = 0; weekDay < 7; weekDay++) {
      this.week.push(this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.DAY_' + weekDay));
    }
    this.selectedDay = moment();
    this.selectCircle = this.selectedDay.clone();
    this.renderDateTitle();
    this.medicationSrvc.fetchMedication().then(
      async (prescriptions) => {
        this.medicines = prescriptions; // TODO: remove this line
        // TODO: ver si podemos eliminar y crear los recordatorios en segundo plano, sin interferir en la carga de la página.
        await this.generateWeekArrayAndSetTakes();

        this.activatedroute.queryParams.subscribe(params => {
          console.log(params);
          this.getParamNavigation(params && params.data ? params.data : null);
        });
      }).catch(error => {
        console.error('No hay parámetros para las tomas', error);
        this.generateWeekArrayAndSetTakes();
      });

  }

  public async getParamNavigation(data): Promise<any> {
    if (!data) {
      return;
    }
    data = typeof data === 'string' ? JSON.parse(data) : data;
    if (data.triggerDate) {
    console.log(data);

    const takeMoment = moment(data.triggerDate);
    // evaluar el resultado de dayOfWek para un domingo.
    // para comprobar si coincide con el numero del arreglo de la semana.
    const dayOfWeek = takeMoment.isoWeekday() - 1;

    // Comprobar semana
    const currentWeek = moment().startOf('isoWeek');
    const weekDiff = currentWeek.diff(takeMoment);
    // Cuando el momento de la toma sea menor que el inicio de la semana vamos a cambiar a la semana correcta
    if (data.triggerDate && weekDiff > 0) {
      // Inicio y fin de una semana (cualquiera) -> en base a la toma.
      const curr = new Date(data.triggerDate); // get current date
      const first = curr.getDate() - (curr.getDay() === 0 ? 6 : (curr.getDay() - 1)); // First day is the day of the month - the day of the week
      const last = first + 6; // last day is the first day + 6
      const firstday = new Date(curr.setDate(first));
      const lastday = new Date(curr.setDate(last));
      firstday.setHours(0, 0, 0, 0);
      lastday.setHours(23, 59, 59, 999);
      const startOfWeek = moment(firstday);
      const endOfWeek = moment(lastday);
      console.log(startOfWeek);
      console.log(endOfWeek);
      await this.generateWeekArrayAndSetTakes(0, startOfWeek, endOfWeek);
    } else {
      await this.backToToday();
    }

    this.changeDay({dayNumber: takeMoment});
    const foundThisId = data.id ? data.id : data.takeNotification.id;
    const foundThisTime = data.time ? data.time : data.takeNotification.time;
    this.takesOfTheWeek[dayOfWeek].forEach((takeItem: MedicationTake) => {
      if (Number(takeItem.id) === Number(foundThisId) && takeItem.time === foundThisTime) {
        console.log('Abrir esta take: ', takeItem);
        this.openModal(takeItem);
      }
    });
  }
    // Eliminamos los queryparams de la url
    this.location.replaceState(this.location.path().split('?')[0], '');

  }

  public getOneMedicineAndGenerateTake(take) {
    // TODO: mover la fecha a la toma en concreto
    take.codPaciente = this.currentConfig.config.codPaciente;
    this.medicationSQLService.getSQLiteMedicine(take.medicationId).then((medication) => {
      take.medicationName = medication.name;
      take.medicationStatus = MedicationState.ACTIVO;
      take.status = MedicationTakeStatus.PENDING; // TODO: refactorizar la función de setear el estado
      this.openModal(take);
    });
  }

  public async generateWeekArrayAndSetTakes(next = 0, start?, end?) {
    await this.generateWeekArray(next, start ? start : null, end ? end : null);
    this.takes = this.takesOfTheWeek[this.selectedDay.isoWeekday() - 1].filter(element => !(element.medicationStatus === MedicationState.ELIMINADO && element.status === MedicationTakeStatus.PENDING));
    this.isLoading = false;
  }

  public renderDateTitle() {
    const today = moment();
    const tomorrow = moment().add(1, 'days');
    const yesterday = moment().add(-1, 'days');
    let literal;
    const date = this.selectCircle.locale('es').format('dddd D MMM').slice(0, -1);

    switch (this.selectCircle.format('DD-MM-YYYY')) {
      case tomorrow.format('DD-MM-YYYY'):
        literal = 'MY-MEDICATION.MEDICATION-CALENDAR.TOMORROW';
        break;
      case yesterday.format('DD-MM-YYYY'):
        literal = 'MY-MEDICATION.MEDICATION-CALENDAR.YESTERDAY';
        break;
      case today.format('DD-MM-YYYY'):
        literal = 'MY-MEDICATION.MEDICATION-CALENDAR.TODAY';
        break;
    }

    this.today = literal ? this.translate.instant(literal, { date }) : date;
  }

  public async generateWeekArray(next: number = 0, setStartOfWeek?, setEndOfWeek?) {
    let startOfWeek = setStartOfWeek ? setStartOfWeek : moment().startOf('isoWeek');
    let endOfWeek = setEndOfWeek ? setEndOfWeek : moment().endOf('isoWeek');

    let nextMonday;
    // nos movemos de semana al deslizar el calendario,
    // solo cuando no esten seteados el inicio y fin de la semana
    // el inicio y fin de una semana concreta pudieran venir seteados por el registro de una toma.
    if (this.dates && this.dates.length > 0 && !setStartOfWeek && !setEndOfWeek) {
      if (next > 0) {
        nextMonday = this.dates[0].dayNumber.add(7, 'd');
      }
      if (next < 0) {
        nextMonday = this.dates[0].dayNumber.subtract(7, 'd');
      }
      startOfWeek = nextMonday;
      endOfWeek = nextMonday.clone().add(7, 'd');
    }

    // TODO: refactor
    const medicines = await
    this.medicationSQLService.getSQLiteMedication(
      startOfWeek.format('YYYY-MM-DD'),
      endOfWeek.format('YYYY-MM-DD'), 'Yes');

    this.medicines = medicines;

    await Promise.all(this.medicines.map(async med => {
      this.parseDateToMoment(med);
      const res: MedicationTake[] = await this.takesSQLService.getSQLiteTakes(med.medicationId);
      med.takes = res;
    }));
    await this.callTakesFromBackEnd(startOfWeek, endOfWeek);

    this.dates = [];
    while (startOfWeek < endOfWeek) {
      this.dates.push({
        dayNumber: startOfWeek,
        selected: false,
        isToday: startOfWeek.isSame(new Date(), 'days'),
        thereIsATake: this.checkIfThereIsATake(startOfWeek, endOfWeek),
      });
      startOfWeek = startOfWeek.clone().add(1, 'd');
    }
  }

  public setDatesToTakes(s, e) {
    let start = s.clone();
    const end = e.clone();
    this.takesOfTheWeek = [];
    while (start < end) {
      this.takesOfTheWeek.push(this.findMedicinesForTheDay(start, end));
      start = start.clone().add(1, 'd');
    }
  }



  async callTakesFromBackEnd(startDate, endDate) {
    const parameters = {
      _format : 'json',
      _query : 'tomas',
      codPaciente: this.currentConfig.config.codPaciente,
      fechaInicio: `${moment(startDate).format('DD/MM/YYYY')}`,
      fechaFin:  `${moment(endDate).format('DD/MM/YYYY')}`,
    };
    await this.takesService.getMedicationTakesLog(parameters).then( response => {
      this.mergeData(response, startDate, endDate);
    }).catch(() => {
      this.setDatesToTakes(startDate, endDate);
    });
  }


  mergeData(serverData, startDate, endDate) {
    if (serverData && serverData.entry) {
      const deletedTakes = serverData.entry.filter(ent => !ent.resource.recordatorioActivo );
      let deletedMedications = mapToMedicationModel(deletedTakes.map(deletedTake => deletedTake.resource.prescripcion), this.translate);
      deletedMedications = deletedMedications.map(medication => {
        this.parseDateToMoment(medication);
        if (medication && medication.takes) {
          for (const singleTake of medication.takes) {
            singleTake.medicationId = medication.medicationId.toString();
            singleTake.medicationStatus =  MedicationState.ELIMINADO;
          }
        }
        return medication;
      });
      this.medicines = this.medicines.concat(deletedMedications);
      this.setDatesToTakes(startDate, endDate);
      for (const resource of serverData.entry) {
        const codDefToma = resource.resource.codDefToma;
        const time = moment(resource.resource.fchToma).format('HH:mm');
        const position = moment(resource.resource.fchToma).isoWeekday() - 1;
        const action = resource.resource.accion;
        const registerId = resource.resource.id;
        this.matchServerDataWithTake(codDefToma, position, time, action, registerId);
      }
    } else { this.setDatesToTakes(startDate, endDate); }
  }

  public parseDateToMoment(med) {
    med.startDate = moment(med.startDate);
    med.endDate = med.endDate ? moment(med.endDate) : '';
  }

  public matchServerDataWithTake(codDefToma, position, time, action, registerId) {
    this.takesOfTheWeek[position].map((take: MedicationTake) => {
      if (Number(take.id) === Number(codDefToma) && take.time === time) {
        take.registerId = registerId;
        this.switchTakeStatus(take, action);
      }
      return take;
    });


  }

  public checkIfThereIsATake(day: any, endOfWeek): boolean {
    let thereIsATake = false;
    if (this.medicines && this.medicines.length) {
      this.medicines.forEach(medicine => {
        if (this.checkMedicationFrequency(medicine, day, endOfWeek)) {
          const match = this.takesOfTheWeek[day.isoWeekday() - 1].find((take: any) => {
            return take.medicationId === medicine.medicationId &&
            (
              (
                take.medicationStatus === MedicationState.ELIMINADO &&
                take.status !== MedicationTakeStatus.PENDING
              ) ||
              take.medicationStatus !== MedicationState.ELIMINADO
            );
          });
          if (match) {
            thereIsATake = true;
          }
        }
      });
    }

    return thereIsATake;
  }

  public onCalendarTouchStart(event) {
    event.stopPropagation();
    this.touchStartClientX = event.changedTouches[0].clientX;
  }
  public onCalendarTouchMove(event) {
    event.stopPropagation();
    const touchMove = event.changedTouches[0].clientX;
    if (touchMove > this.touchStartClientX + 5) {
      this.touchDirection = -1;
    } else if (touchMove < this.touchStartClientX - 5) {
      this.touchDirection = 1;
    }
  }
  public onCalendarTouchEnd() {
    if (this.touchDirection !== 0) {
      this.generateWeekArray(this.touchDirection);
      this.touchDirection = 0;
    }
  }

  public findMedicinesForTheDay(day, endOfWeek) {
    this.medicinesForToday = [];
    // * Paso 1 Determinar si la medicina toca hoy partiendo de su fecha inicial y final
    if (!this.medicines || this.medicines.length <= 0) {
      return [];
    }
    this.medicines.forEach(medicine => {
      if (this.checkMedicationFrequency(medicine, day, endOfWeek)) {
        this.medicinesForToday.push(medicine);
      }
    });

    let takesMerge = [];
    this.medicinesForToday.forEach((medicine: any) => {
      if (medicine.takes) {
        takesMerge = takesMerge.concat(...medicine.takes.map(toma => {
          toma.status = toma.status ? toma.status : MedicationTakeStatus.PENDING;
          toma.medicationName = medicine.name;
          if (toma.medicationStatus !== MedicationState.ELIMINADO) {
            setMedicineStatus(medicine);
            toma.medicationStatus = medicine.textStatus;
          }
          return {...toma};
        }));
      }
    });

    return [ ...takesMerge.sort(this.sortByHour) ];
  }

  public checkMedicationFrequency(medicine: any, day, endOfWeek): boolean {
    if (!day.isBetween(medicine.startDate, endOfWeek, undefined, '[]')) {
      return false;
    }
    if (medicine.frequency === '1' && medicine.endDate && medicine.endDate >= day) { // * Se toma cada día
      return true;
    } else if (medicine.frequency === '1' && !medicine.endDate) {
      return true;
    } else if (medicine.frequency === '2' &&
        ((!medicine.endDate)
                ||
    (medicine.endDate && medicine.endDate >= day))) {
      // añadir si no tiene fecha fin o si la fecha fin es aún válida (mayor al dia que se está evaluando) 
      // * Se toma días específicos ej lunes - miércoles - viernes
      const frequencyValuesToArray = typeof medicine.frequencyValues === 'object' ?
        medicine.frequencyValues :
        medicine.frequencyValues.split(',');

      let position = day.day() - 1;
      position = position === -1 ? 6 : position;

      // * Comprueba si la medicina se toma según el array de días de la semana con valores booleanos
      if (frequencyValuesToArray[position] && frequencyValuesToArray[position] !== 'false') {
        return true;
      }
    } else { // * Se toma a intervalos, cada 3 días por ejemplo
      // Importante: puede no tener fecha fin
      if (  (!medicine.endDate) || (medicine.endDate && medicine.endDate >= day) ) {
        const start = moment(medicine.startDate);
        const difference = day.diff(start, 'days');
        if (difference % medicine.frequencyInterval === 0) {
          return true;
        }
      }
    }
  }


  public changeDay(day) {
    this.selectCircle = day.dayNumber ? day.dayNumber.clone() : null;
    // * 2) Find the medicines for the day
    this.renderDateTitle();
    this.takes = this.takesOfTheWeek[day.dayNumber.isoWeekday() - 1].filter(element => !(element.medicationStatus === MedicationState.ELIMINADO && element.status === MedicationTakeStatus.PENDING));
  }

  public async backToToday() {
    this.selectCircle = moment();
    this.dates = [];
    this.renderDateTitle();
    await this.generateWeekArray(0);
    this.takes = this.takesOfTheWeek[moment().isoWeekday() - 1].filter(element => !(element.medicationStatus === MedicationState.ELIMINADO && element.status === MedicationTakeStatus.PENDING));
  }

  public async openModal(take: MedicationTake) {
    if (take && this.checkFutureTakes()) {
      this.modalCtlr.getTop().then((modal) => {
        if (modal) {
          modal.dismiss();
        }
        this.modalCtlr.create({
          component: MedicationTakeModalComponent,
          componentProps: { take },
          cssClass: take.status === MedicationTakeStatus.PENDING ? 'medication-take-modal' : 'medication-take-modal--small',
        }).then((modal) => {
          modal.present();
          modal.onDidDismiss().then((data) => {
            if (data.role !== 'backdrop') {
              switch (data.data) {
                case MedicationTakeAction.DISMISS:
                  this.postMedicationTake(take, data.data);
                  break;
                case MedicationTakeAction.TAKE:
                  this.postMedicationTake(take, data.data);
                  break;
                case MedicationTakeAction.UNDO:
                  this.presentUndoAlert(take);
                  break;
                case MedicationTakeAction.DELETE:
                  this.presentDestroyAlert(take);
                  break;
              }
            }
          });
        });
      });
    }
  }

  public checkFutureTakes(): boolean {
    if (this.selectCircle.diff(moment().startOf('day'), 'days') > 0) {
      this.alertSrvc.presentToast(this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.FUTURE_TAKE'));
      return false;
    } else {
      return true;
    }
  }

  public postMedicationTake(take: MedicationTake, action: number) {

    const fchAccionToma = this.selectCircle.clone();
    fchAccionToma.hour(take.time.split(':')[0]);
    fchAccionToma.minute(take.time.split(':')[1]);

    const payload: MedicationTakeRegisterRequest = {
      resourceType: 'TomaDTO',
      fchToma: fchAccionToma.format('YYYY-MM-DDTHH:mm:ss:SSS') ,
      fchAccionToma: this.selectCircle.format('YYYY-MM-DDTHH:mm:ss:SSS'),
      accion: action,
      codPaciente: take.codPaciente,
      codPrescripcion: Number(take.medicationId),
      codDefToma: take.id,
      activo: true,
    };
    this.takesService.postMedicationTakeRegister(payload)
      .then(
        res => {
          take.registerId = res.id;
          this.alertSrvc.presentToast(this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.TAKE_REGISTER_SUCCESS'));
          this.switchTakeStatus(take, action);
        }
      )
      .catch(err => {
        this.alertSrvc.presentToast(this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.TAKE_REGISTER_ERROR'));
      });
  }

  public switchTakeStatus(take: MedicationTake, action?: number) {
    switch (action) {
      case 0:
        take.status = MedicationTakeStatus.REJECTED;
        break;
      case 1:
        take.status = MedicationTakeStatus.TAKEN;
        break;
      default:
        take.status = MedicationTakeStatus.PENDING;
        break;
    }
  }

  public presentUndoAlert(take: MedicationTake) {
    this.alertSrvc.presentAlertWithButtons(
      null,
      this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.ALERT_MESSAGE'),
      [{
        text: this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.ALERT_CANCEL'),
        role: 'cancel',
      }, {
        text: this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.ALERT_UNDO'),
        handler: () => {
          this.deleteTake(take, false);
        }
      }],
      null,
      'single-med-card-alert--neutral',
    );
  }

  public presentDestroyAlert(take: MedicationTake) {
    this.alertSrvc.presentAlertWithButtons(
      null,
      this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.ALERT_MESSAGE_DELETE'),
      [{
        text: this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.ALERT_CANCEL'),
        role: 'cancel',
      }, {
        text: this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.ALERT_DELETE'),
        handler: () => {
          this.deleteTake(take, true);
        }
      }],
      null,
      'single-med-card-alert',
    );
  }

  public deleteTake(take: MedicationTake, destroy) {
    this.takesService.deleteMedicationTakeRegister(take.registerId)
      .then(
        async res => {
          this.switchTakeStatus(take);
          // si es un borrado de verdad ... quitar la toma de la lista
          if (destroy) {
            this.takes = this.takes.filter(toma => {
              // retorna todas las tomas que no sean la que acabamos de eliminar.
              if (!(Number(take.id) === Number(toma.id) && take.time === toma.time)) {
                return toma;
              }
            });
          }
          this.alertSrvc.presentToast(this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.TAKE_DELETE_SUCCESS'));
        }
      )
      .catch(err => {
        this.alertSrvc.presentToast(this.translate.instant('MY-MEDICATION.MEDICATION-CALENDAR.TAKE_DELETE_ERROR'));
      });
  }
}
