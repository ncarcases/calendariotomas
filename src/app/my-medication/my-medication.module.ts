import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { SQLite } from '@ionic-native/sqlite/ngx';

import { MyMedicationPage } from './my-medication.page';
import { SharedModule } from 'src/modules/shared/shared.module';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';

import { MedicationCalendarComponent } from './medication-calendar/medication-calendar.component';
import { OverviewComponent } from './overview/overview.component';

import { SingleMedCardComponent } from '../components/single-med-card/single-med-card.component';
import { MedicationDetailsModalComponent } from '../components/medication-details-modal/medication-details-modal.component';
import { MedicationTakeModalComponent } from '../components/medication-take-modal/medication-take-modal.component';
import { MedicationTakeCardComponent } from '../components/medication-take-card/medication-take-card.component';

import { AuthGuard } from '../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: MyMedicationPage,
    children: [
      {
        path: 'overview',
        component: OverviewComponent,
        canActivate: [AuthGuard],
      },
      {
        path: 'reminders',
        component: MedicationCalendarComponent,
        canActivate: [AuthGuard],
      },
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedModule,
  ],
  entryComponents: [
    MedicationDetailsModalComponent,
    MedicationTakeModalComponent,
  ],
  declarations: [
    MyMedicationPage,
    MedicationCalendarComponent,
    OverviewComponent,
    SingleMedCardComponent,
    MedicationDetailsModalComponent,
    MedicationTakeModalComponent,
    MedicationTakeCardComponent,
  ],
  providers: [
    LocalNotifications,
    SQLite
  ]
})
export class MyMedicationPageModule {}
