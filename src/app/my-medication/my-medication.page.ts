import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

@Component({
  selector: 'app-my-medication',
  templateUrl: './my-medication.page.html',
  styleUrls: ['./my-medication.page.scss'],
})
export class MyMedicationPage implements OnInit, OnDestroy {

  public selectedSegment: any = null;
  private routerSubscription: Subscription;

  constructor(
    private router: Router,
  ) { }

  public ngOnInit() {
    // a lo mejor es suficiente con el router.url de la línea de arriba
    this.routerSubscription = this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      map((ev: NavigationEnd) => {
        this.selectedSegment = ev.urlAfterRedirects.split('/').pop().split('?')[0];
      }),
    ).subscribe();
  }

  public ngOnDestroy() {
    this.routerSubscription.unsubscribe();
  }
}
