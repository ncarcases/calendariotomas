import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MedicationModel } from 'src/models/medication.model';
import { MedicationService } from 'src/services/medications/medication.service';
import { MedicationRemindersService } from 'src/services/medications/medication.reminders.service';
import { MedicationSQLService } from '../../../services/medications/medication.sqlservice';
import { TakesSQLService } from '../../../services/takes/takes.sqlservice';
import { TranslateService } from '@ngx-translate/core';
import { setMedicineStatus } from '../../../services/medications/utils/set-status-medicine';
import { AlertService } from '../../../services/alert.service';
import { MedicationTake } from '../../models/medication';
import { ModalController } from '@ionic/angular';
import { MedicationDetailsModalComponent } from '../../components/medication-details-modal/medication-details-modal.component';
import { take, takeUntil } from 'rxjs/operators';

import {
  Medication,
  SwitchState,
  MedicationState,
  MedicationPushState,
} from '../../models/medication';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-medication-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss'],
})
export class OverviewComponent implements OnInit {
  isLoading = true;
  nextReminders: any = null;

  medicines: MedicationModel[] = [];
  // medicinesFromServer: MedicationModel[];
  filteredMedicinesArr: MedicationModel[] = null;

  filterButtons = [
    {
      label: 'Activos',
      filterTag: MedicationState.ACTIVO,
    }, {
      label: 'Suspendidos',
      filterTag: MedicationState.SUSPENDIDO,
    }, {
      label: 'Finalizados',
      filterTag: MedicationState.FINALIZADO,
    }, {
      label: 'Sin recordatorio',
      filterTag: MedicationState.SIN_RECORDATORIO,
    }
  ];
  public filter = null;


  private queryparamsMedication = null;
  private unsubscribe: Subject<void>;

  constructor(
    private medicationSrvc     : MedicationService,
    private medicationSQLService: MedicationSQLService,
    private remindersSrvc      : MedicationRemindersService,
    private takesSQLService : TakesSQLService,
    private translate: TranslateService,
    private alertSrvc: AlertService,
    private route: ActivatedRoute,
    private modalCtlr: ModalController,
    private cd: ChangeDetectorRef,
  ) {
  }

  async ngOnInit() {
    this.route.queryParams
    // .pipe(takeUntil(this.unsubscribe))
        .subscribe( async params =>    {
        console.log('OVERVIEW PARAMS RECEIVED', params);
      // TODO: si vienen params actualizo sólo la medicación correspondiente.
        if (params && params.idPrescription) {
        this.queryparamsMedication = params;
        await this.alertSrvc.presentLoadingDialog(this.translate.instant('MY-MEDICATION.PUSH_UPDATE'));
      }
        this.fetchMedication();
    });
  }

//   async ionViewWillEnter() {
//     this.unsubscribe = new Subject();
//   }

//   ionViewWillLeave() {
//     this.unsubscribe.next();
//     this.unsubscribe.complete();
// }

  public fetchMedication() {
    this.isLoading = true;
    this.medicationSrvc.fetchMedication().then( (medications) => {
      console.log('FETCH MEDICATIONS COMPLETE', medications);
      this.buildLocalMedicinesObject(true);
    } ).catch( (err) =>  {
        console.log('FETCH MEDICATIONS ERROR', err);
        this.buildLocalMedicinesObject(true);
    });
  }

  buildLocalMedicinesObject(loading: boolean) {
    this.medicationSQLService.getSQLiteMedication() // TODO: ya se está devolviendo en fetchMedication
        .then( async ( prescriptions: Medication[] ) => {
        console.log('return for medicationSQL service');
        this.medicines = [];
        const medicinesArrCopy = [];
        for (const medicine of prescriptions) {
        // TODO: tener en cuenta que al cargar la table de SQLite el toggle se pone por defecto a on
        setMedicineStatus(medicine);
        // medicine.toggleNotificationButtonState = medicine.textStatus === MedicationState.ACTIVO ? SwitchState.ON : SwitchState.OFF;
        const res = await this.getSQLiteTakes(medicine);
        medicinesArrCopy.push(res);
      }
        this.medicines = [...medicinesArrCopy];
        this.medicines.sort((a, b) => a.name.localeCompare(b.name));
        this.setMedicinesFilter();

        if (loading) {this.isLoading = false; }
        this.alertSrvc.dismissLoadingDialog();

        this.cd.detectChanges(); // TODO: comprobar si hace falta
        this.matchMedicationAndOpenModal();
    }).catch(error => {
      console.log(error);
      this.isLoading = false;
    });

  }

  public matchMedicationAndOpenModal() { // TODO: use one source of truth to update info (BehaviourSubject)
    this.modalCtlr.getTop().then((modal) => {
      if (modal) {
        modal.dismiss();
      }
      if (
        this.queryparamsMedication
        && this.queryparamsMedication.typeNotification !== MedicationPushState.DELETEPRESCRIPTION
      ) {
        // TODO: si se ha eliminado un medicamento hacemos el fetch?
        // TODO: revisar que si es de creación no debe abrir el detalle ya que no hay id de recordatorio
        const medication = this.medicines.find((med) => med.medicationId === this.queryparamsMedication.idPrescription);
        if (medication) {
          this.showMedicationDetailsInModal(medication);
        }
        this.queryparamsMedication = null;
      }
    });
  }

  public getSQLiteTakes(medicine) {
    return this.takesSQLService.getSQLiteTakes(medicine.medicationId)
      .then((res) => {
        medicine.takes = res;
        return medicine;
        // this.medicines.push(medicine);
      })
      .catch(err => console.log(err));
  }

  toggleFilter(button) {
    this.filter = (button === this.filter) ? null : button;
    this.setMedicinesFilter();
  }
  public setMedicinesFilter() {
    this.filteredMedicinesArr = this.medicines.filter(medicine => {
      return medicine.textStatus === this.filter;
    });
  }

  showAllReminders() {
    this.remindersSrvc.checkAllCreatedReminders()
    .then(res => {
      this.nextReminders = res;
    });
  }

  deleteAllReminders() {
    this.remindersSrvc.deleteAllReminders()
    .then(res => {
    });
  }

  public async switchOnReminders(medItem) {

    this.medicationSQLService.updateSQLiteMedicine(medItem.medicationId, SwitchState.ON).then(async () => {
      this.alertSrvc.presentToast(this.translate.instant('MY-MEDICATION.TOAST_MESSAGE_ACTIVE_OK', { name: medItem.name }));
      this.remindersSrvc.createReminders(medItem);
      // Actualizamos la lista con la base de datos local
      this.isLoading = true;
      this.buildLocalMedicinesObject(this.isLoading);
      medItem.toggleNotificationButtonState = SwitchState.ON;
      setMedicineStatus(medItem);
      setTimeout(() => {
        this.remindersSrvc.checkAllCreatedReminders().then( resp => {
          console.error('RECORDATORIOS CREADOS', resp);
        });
      }, 3000);
    })
    .catch(err => {
      this.alertSrvc.presentToast(this.translate.instant('MY-MEDICATION.TOAST_MESSAGE_ACTIVE_KO', { name: medItem.name }));
    });
  }

  public async switchOffReminders(medItem) {
    // * Recorrerá todos los recordatorios de un medicamento y los elimina de uno en uno
    await this.medicationSQLService.updateSQLiteMedicine(medItem.medicationId, SwitchState.OFF).then(async () => {
      this.alertSrvc.presentToast(this.translate.instant('MY-MEDICATION.TOAST_MESSAGE_OK', { name: medItem.name }));
      // // Actualizamos la lista con la base de datos local
      this.isLoading = true;
      this.buildLocalMedicinesObject(this.isLoading);
      medItem.toggleNotificationButtonState = SwitchState.OFF;
      setMedicineStatus(medItem);
    })
    .catch(err => {
      this.alertSrvc.presentToast(this.translate.instant('MY-MEDICATION.TOAST_MESSAGE_KO', { name: medItem.name }));
    });

    if (!medItem.takeIdsAsString) {
      return;
    }
    const ids = medItem.takeIdsAsString.split(',');

    if (ids) {
        for (const id of ids) {
          await this.remindersSrvc.deleteReminderById(id);
        }
      }
  }

  showMedicationDetailsInModal(medItem: Medication | MedicationModel) {
      return this.modalCtlr.create({
          component: MedicationDetailsModalComponent,
          componentProps: { medItem }
        }).then((modal) => {
        if (modal) {
          modal.present();
        }
    });
  }
}
