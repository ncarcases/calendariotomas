import {
  Component,
  OnInit,
} from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {
  FormGroup,
  FormBuilder,
  Validators,
} from '@angular/forms';
import {
  LoginService,
} from 'src/services/login.service';
import { AlertService } from '../../services/alert.service';
import { NavController } from '@ionic/angular';

import {
  ValidatorEmail,
} from '../utils/custom-validators';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.page.html',
  styleUrls: ['./recover-password.page.scss'],
})
export class RecoverPasswordPage implements OnInit {

  public recoverForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private loginService: LoginService,
    private alertSrvc: AlertService,
    private translate: TranslateService,
    private nav: NavController,
  ) { }

  public ngOnInit() {
    this.recoverForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, ValidatorEmail])],
    });
  }

  get email() { return this.recoverForm.get('email'); }

  public ionViewWillEnter() {
    this.recoverForm.reset();
  }

  public toRecover() {
    if (!this.recoverForm.valid) {
      return;
    }
    const email = this.recoverForm.get('email').value;
    this.recoverForm.reset();
    this.loginService.recoverPassword(email).then(
      (res) => {
        console.log(res);
        this.alertSrvc.presentToast(this.translate.instant('RECOVER_PASSWORD.REQUEST_OK'));
        this.nav.navigateForward('\login');
      },
      (err) => {
        this.alertSrvc.presentAlertWithButtons(
          this.translate.instant('COMMON.ERROR'),
          this.translate.instant('RECOVER_PASSWORD.USER_NOT_FOUND'),
          [this.translate.instant('COMMON.OK')]
        );
        this.nav.navigateForward('\login');
      }
    );
  }

}
