import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

import { ToastService } from 'src/services/toast.service';
import { TranslateService } from '@ngx-translate/core';
import { RegisterService } from 'src/services/register.service';
import { AlertService } from 'src/services/alert.service';
import { NavController } from '@ionic/angular';
import { LocalStorageKeys } from 'src/models/localstorage.keys.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  registerForm: FormGroup = null;
  emailValidator: RegExp = new RegExp('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$');
  passwordIsVisible = false;

  errorMessages = {
    mail: [{
      type: 'required',
      message: this.translate.instant('REGISTER.ERROR-MESSAGES.USERNAME-REQUIRED')
    }, {
      type: 'pattern',
      message: this.translate.instant('REGISTER.ERROR-MESSAGES.BAD-PATTERN')
    }],
    password: [{
      type: 'required',
      message: this.translate.instant('REGISTER.ERROR-MESSAGES.PASSWORD-REQUIRED')
    }, {
      type: 'minlength',
      message: this.translate.instant('REGISTER.ERROR-MESSAGES.MIN-LENGTH')
    }]
  };

  constructor(
    public formBuilder: FormBuilder,
    public toast: ToastService,
    public translate: TranslateService,
    public registerService: RegisterService,
    public alertService: AlertService,
    private navCtlr: NavController,
    private storage: Storage
  ) { }

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      mail: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(this.emailValidator)
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6)
      ])),
      checkbox: new FormControl(null, Validators.requiredTrue)
    });
  }

  async launchRegister() {
    // * Show loading dialog
    // this.alertService.presentLoadingDialog(this.translate.instant('REGISTER.REGISTERING-IN-PROGRESS'));
    // * Register user in service and show message
    try {
      const response = await this.registerService.register(this.registerForm);
      if (response.error.includes('Usuario ya activo')) {
        this.presentAlertError();
      } else if (response) {
        this.storage.remove(LocalStorageKeys.KEY_USERNAME).then(() => {
          this.presentAlert();
          this.navCtlr.navigateRoot('\login');
        });
      } else {
        this.toast.presentToast((this.translate.instant('REGISTER.GENERIC-ERROR')));
      }
    } catch (err) {
      console.error(JSON.stringify(err));
    }
  }

  public presentAlert() {
    this.alertService.presentAlert(
      this.translate.instant('REGISTER.SUCCESS-IN-REGISTER'),
      this.translate.instant('REGISTER.SUCCESS-IN-REGISTER-MSG'),
      ['OK']
    );
  }

  public presentAlertError() {
    this.alertService.presentAlert(
      this.translate.instant('REGISTER.ERROR-IN-REGISTER'),
      this.translate.instant('REGISTER.ERROR-IN-REGISTER-MSG'),
      ['OK']
    );
  }

}
