import { AppointmentExtension } from './appointmentextension.model';
import { AssesmentModel } from './assesment.model';

export class AppointmentModel {
  id: number;
  time: string;
  date: Date;
  speciality: string;
  doctor: string;
  hospital: string;
  address: string;
  phone: string;
  type: string;
  assessments?: AssesmentModel[];
  isNearestElement: boolean;
  appointmentExtension: AppointmentExtension[];
}
