import { BriefModel } from './brief.model';
import { InformationModel } from './information.model';

export class AppointmentExtension {
    briefs: BriefModel[]; // the brief contains the attachment data.
    information: InformationModel;
}
