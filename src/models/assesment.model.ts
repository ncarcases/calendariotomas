export class AssesmentModel {
  id: number;
  healthCenter: string;
  serviceType: string;
  date: any;
  numberValoraciones: number;
  valorationType: string;
  valorationTitle: string;
  createDate: Date;
  requestDate: Date;
  observations: string;
  fields: {label: string, value: string}[];
}
