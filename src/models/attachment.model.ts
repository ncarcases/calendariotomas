export class AttachmentModel {
    title: string; // i.e: 'Informe Radiología', (tituloValoracion)
    idCard: string; // User Card's id: ACxxxxxxx (cita.paciente.nhc)
    id: string; // File info (from filesInfo node in Assestment response) (fileInfo[index].id)
    studyUID: string; // from Assestment response (fileInfo[index].studyUID)
    serieUID: string; // from Assestment response (filesInfo[index].serieUID)
    objectUID: string; // from Assestment response (filesInfo[index].objectUID)
    fileExtension: string; // from Assestment response (filesInfo[index].fileExtension)
    fileName: string; // from Assestment response (filesInfo[index].fileName)
}
