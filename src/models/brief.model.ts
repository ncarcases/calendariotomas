import { AttachmentModel } from './attachment.model';

export class BriefModel {
    title: string;
    date: Date;
    status: number;
    hospitalInfo: string;
    hospitalDestino: string;
    fields: Map<string, string>;
    attachment: AttachmentModel[];
}
