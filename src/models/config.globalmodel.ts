import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable({
    providedIn: 'root'
})
export class ConfigGlobalModel {
    // TODO - ELIMINAR LA URL BASE ESTO FUE PUESTO SOLO PARA AGILIZAR DESARROLLO
    config: any = {
        baseUrl: 'https://plataforma.prevencionesbida.es/',
        };
    user: string;
    token: string;
    codPaciente: string;
    mail: string;

    constructor(
        public storage: Storage,
    ) {
        this.storage.get('codPaciente').then(value => {
            if (value) {
                this.config.codPaciente = value;
            }
        });
        this.storage.get('mail').then(value => {
            if (value) {
                this.config.mail = value;
            }
        });
        this.storage.get('token').then(value => {
            if (value) {
                this.config.token = value;
            }
        });
    }
}
