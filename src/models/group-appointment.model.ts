import { AppointmentModel } from './appointment.model';

export class GroupAppointmentModel {
  date: Date;
  appointments: AppointmentModel[];
}
