import { AppointmentModel } from './appointment.model';

export class GroupedAppointmentModel {
  type: string;
  month: any;
  appointments: AppointmentModel[];
}
