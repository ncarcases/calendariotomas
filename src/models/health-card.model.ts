export class HealthCardModel {
  public category: string;
  public cssClassIcon: string;
  public cssBackgroundColor: string;
  public cssIconColor: string;
  public lastUpdated: Date;
  public details: string[];
}
