import { ErrorHandler } from '@angular/core';

export class InformationModel {
    title: string;
    fields: {
      label: string,
      value: string,
      type: string,
      cssIconClass: string,
    }[];
    // DUDA: Cómo añadir aquí un método para no hacerlo en la plantilla
}
