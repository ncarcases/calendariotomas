import { Injectable } from '@angular/core';

@Injectable()
export class LocalStorageKeys {
    public static KEY_USERNAME = 'username';
    public static KEY_ENVIRONMENT = 'environment';
}
