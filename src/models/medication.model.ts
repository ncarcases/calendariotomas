import { MedicationTakeStatus, MedicationState } from '../app/models/medication';
export class MedicationModel {
  // * Id único de la medicación en MyMed
  medicationId: number | string;

  // * Nombre de la medicación en MyMed
  name: string;

  // * Categoría general para determinar si la medicación tiene recordatorios o no (usada en el filtro con botones)
  hasReminders: boolean | string;

  // * Identificador único de cada recordatorio en MyMed. Cambia cuando se actualiza la información en MyMed, así que es útil para hacer comparaciones entre la información local y la que viene del servidor
  reminderId? : string;

  // * Esto averigua si el medicamento tiene fecha de finalización, para hacer un filtrado previo
  hasEndDate? : boolean;

  // * Fecha de inicio de la medicación
  startDate?: string;

  // * Fecha de finalización de las tomas
  endDate?: string;

  // * La duración de la medicación en días
  durationInDays?: number;

  // * Parámetro local, se usa para determinar el estado de ciertos elementos gráficos (iconos de campana) y de la UI (si el toggle el check está en una posición o en otra)
  toggleNotificationButtonState?: any;

  // * Valor que mira si en en MyMed el recordatorio está activado por el médico o farmacéutico, de forma que se puedan activar o no en la app
  remoteNotificationActive?: string;

  // * Valores 1, 2 o 3 que equivalen a "todos los días", "días específicos" o "intervalos", respectivamente
  frequency?: string;

  // *El valor anterior pero expresado en formato string: "todos los días", "días específicos" o "intervalos"
  frequencyDescription?: string;

  // * Número (días) entre cada toma (Ej.: 15)
  frequencyInterval?: string;

  // * Array de booleanos para cada día de la semana (L-D) que indica si ese día se toma la medicación o no. Solo presente cuando frequencyDescription == 2;
  frequencyValues?: boolean[];

  // * Campo que se muestra solo en la UI, en la modal de detalle del medicamento, indicando si se toma todos los días, en días alternos o cada X días (en función del valor del campo anterior "frequency")
  frequencyString?: string;

  // * Determina si la medicación está activa, finalizada, suspendida o si no tiene recordatorio. Se usa luego en el filtro.
  textStatus?: string;

  // * El número de veces al día que se toma la medicación (Ej.: "1 vez al día" o "cada 8 horas")
  numberDailyTakeDescription?: string;
  takes?: MedicationTakeModel[];
}

export class MedicationTakeModel {
  codPaciente: string;
  id: number;
  medicationId: string;
  medicationName: string;
  posology: string;
  quantity: string;
  time: string;
  status?: MedicationTakeStatus;
  registerId?: number;
  medicationStatus?: MedicationState;
}

