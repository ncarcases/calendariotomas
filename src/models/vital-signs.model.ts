export class VitalSignsMeasuresList {
  public key: string;
  public value: any;
}

export class VitalSignsMeasures {
  public lastUpdatedLocal: Date;
  public list: VitalSignsMeasuresList[];
}

export class VitalSignsModel {
  public category: string;
  public cssClassIcon: string;
  public cssBackgroundColor: string;
  public cssIconColor: string;
  public lastUpdated: Date;
  public entries: VitalSignsMeasures[];
}
