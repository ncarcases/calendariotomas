import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {

  constructor(
    private translate: TranslateService
    ) {}

  transform(inputDate: any, format: string): string {
    // Check wether inputDate argument is a string and convert it to a Date Object
    const formatedDate = typeof inputDate === 'string' ? new Date(inputDate) : inputDate;

    // * Get months from translate file and transform it into array
    const daysString = this.translate.instant('COMMON.DAYS');
    const weekdaysList = daysString.split(',');
    const weekday = weekdaysList[formatedDate.getDay()];

    // * get number of the month as a number, no more transformation needeed
    const day = formatedDate.getDate();

    // * get month as a string from translate file
    const monthsString = this.translate.instant('COMMON.MONTHS_ABBR');
    const monthsList = monthsString.split(',');
    const month = monthsList[formatedDate.getMonth()];

    const year = formatedDate.getFullYear();

    // * output with day in number and month name
    let stringDate = '';
    if (format === 'short') {
      stringDate = `${day} ${month} ${year}`;
    } else {
      stringDate = `${weekday}, ${day} ${month} ${year}`;
    }
    return stringDate;
  }

}
