import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';

import { ToastService } from '../../services/toast.service';
import { IonicStorageModule } from '@ionic/storage';
import { ShareService } from 'src/services/share.service';
import { AlertService } from 'src/services/alert.service';
import { MedicationRemindersService } from 'src/services/medications/medication.reminders.service';

import { EmptyViewComponent } from 'src/app/components/empty-view/empty-view.component';
import { SkeletonScreenComponent } from 'src/app/components/skeleton-screen/skeleton-screen.component';
import { ShortenTextPipe } from './pipes/shorten-text.pipe';
import { CustomDatePipe } from './pipes/custom-date.pipe';


@NgModule({
  declarations: [
    EmptyViewComponent,
    SkeletonScreenComponent,
    ShortenTextPipe,
    CustomDatePipe,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    IonicStorageModule.forRoot(),
    IonicModule,
  ],
  exports: [
    TranslateModule,
    EmptyViewComponent,
    SkeletonScreenComponent,
    ShortenTextPipe,
    CustomDatePipe
  ],
  providers: [
    ToastService,
    ShareService,
    AlertService,
    TranslateService,
    MedicationRemindersService,
  ]
})
export class SharedModule { }
