import { Injectable } from '@angular/core';
import { HealthCardModel } from 'src/models/health-card.model';
import { VitalSignsModel, VitalSignsMeasures, VitalSignsMeasuresList } from 'src/models/vital-signs.model';
import { TranslateService } from '@ngx-translate/core';
import { AppointmentModel } from 'src/models/appointment.model';
import { AssesmentModel } from 'src/models/assesment.model';
import { BriefModel } from 'src/models/brief.model';
import { AttachmentModel } from 'src/models/attachment.model';
import { GroupedAppointmentModel } from 'src/models/grouped-appointment.model';


@Injectable({
  providedIn: 'root'
})
export class MapperRepository {

  vitalSignsTranslateKeys = {
    weight: 'BASIC-DATA.VITAL-SIGNS.CATEGORIES.WEIGHT',
    height: 'BASIC-DATA.VITAL-SIGNS.CATEGORIES.HEIGHT',
    SBP: 'BASIC-DATA.VITAL-SIGNS.CATEGORIES.SBP',
    DBP: 'BASIC-DATA.VITAL-SIGNS.CATEGORIES.DBP',
    HR: 'BASIC-DATA.VITAL-SIGNS.CATEGORIES.HR',
    glucose: 'BASIC-DATA.VITAL-SIGNS.CATEGORIES.GLUCOSE'
  };

  constructor(
    public translate: TranslateService
    ) {}

    mapToHealthCardModel(data: any, options: any): HealthCardModel {
    // Output
    const result = new HealthCardModel();
    result.category = options.category;
    result.cssClassIcon = options.cssClassIcon;
    result.cssBackgroundColor = options.cssBackgroundColor;
    result.cssIconColor = options.cssIconColor;
    result.details = [];
    result.lastUpdated = data.entry[0].resource.meta.lastUpdated;

    // Fill the result.details object with data depending on the keyword sent to the mapper service
    switch (options.keyword) {
      case 'alergias':
        if (data.entry[0].resource.alergias) {
          data.entry[0].resource.alergias.forEach(entry => {
            result.details.push(entry.nombre);
          });
        }
        break;

      case 'contraindicaciones':
        if (data.entry[0].resource.contraindicaciones) {
          data.entry[0].resource.contraindicaciones.forEach(entry => {
            result.details.push(entry.nombre);
          });
        }
        break;

      case 'medicacion':
        if (data.entry[0].resource.prescripciones) {
          data.entry[0].resource.prescripciones.forEach(entry => {
            result.details.push(entry.nombre);
          });
        }
        break;
    }
    // return the output
    return result;
  }

  mapToVitalSignsModel(data, options): VitalSignsModel {
    const result = new VitalSignsModel();

    // Pass options for the icon and category from the options object
    result.category = options.category;
    result.cssClassIcon = options.cssClassIcon;
    result.cssBackgroundColor = options.cssBackgroundColor;
    result.cssIconColor = options.cssIconColor;
    // get the latest update
    result.lastUpdated = data.entry[0].resource.meta.lastUpdated;

    result.entries = [];

    // get the entry for each constant period
    if (data.entry[0].resource.constantes) {
      data.entry[0].resource.constantes.forEach(entry => {
        const constantesObject = new VitalSignsMeasures();

        constantesObject.lastUpdatedLocal = entry.fechaConstante;

        constantesObject.list = [];
        constantesObject.list.push({
          key: this.translate.instant(this.vitalSignsTranslateKeys.weight),
          value: entry.peso || null
        }, {
          key: this.translate.instant(this.vitalSignsTranslateKeys.height),
          value: entry.estatura || null
        }, {
          key: this.translate.instant(this.vitalSignsTranslateKeys.SBP),
          value: entry.tensionSistolica || null
        }, {
          key: this.translate.instant(this.vitalSignsTranslateKeys.DBP),
          value: entry.tensionDiastolica || null
        }, {
          key: this.translate.instant(this.vitalSignsTranslateKeys.HR),
          value: entry.pulso || null
        }, {
          key: this.translate.instant(this.vitalSignsTranslateKeys.glucose),
          value: entry.glucosa || null
        });

        result.entries.push(constantesObject);
      });
    }
    return result;
  }

  mapToAssesmentModel(data): any {
    const result = [];
    data.entry.forEach(assesment => {
      // * Filter assesments by the property numerosValoraciones on each of them. If this number is > 0, continue because the assesment will be visible. If not, we don't need more processing on this asessment. Also, for now the assesment with "descripcionServicio" equal to "Inhacheck" will not show
      if (assesment.resource.numerosValoraciones === 0 || assesment.resource.descripcionServicio === 'Inhacheck') {
        return;
      }
      const assesmentElement = new AssesmentModel();
      assesmentElement.id = Number(assesment.resource.id);
      assesmentElement.healthCenter = assesment.resource.descripcionCentro;
      assesmentElement.numberValoraciones = assesment.resource.numerosValoraciones;

      const dateStringsArray = assesment.resource.fechaCita.split(' ');
      const newDate = `${dateStringsArray[0].slice(3, 5)}/${dateStringsArray[0].slice(0, 2)}/${dateStringsArray[0].slice(6, 10)}`;
      assesmentElement.date = Date.parse(newDate);

      assesmentElement.serviceType = assesment.resource.descripcionServicio;
      result.push(assesmentElement);
    });
    return result;
  }

  mapToBriefModel(rawBrief) {
    let filenameIndex = 0;
    const noInfoMessage = this.translate.instant('COMMON.NO-ENTRY');
    const brief = new BriefModel();
    brief.title = rawBrief.tituloValoracion;
    brief.date = rawBrief.fechaCreacion;
    brief.hospitalInfo = rawBrief.cita.descripcionCentro;
    brief.hospitalDestino = rawBrief.centroDestinoDesc;
    brief.status = rawBrief.estado;
    brief.fields = new Map();
    brief.attachment = [];

    // If there's no "campos" in the rawBrief, return
    if (!rawBrief.campos) {
      return brief;
    }

    // Populate the fields array
    // console.log(rawBrief.campos);
    rawBrief.campos.forEach(campo => {
      if (campo.visible && campo.tipo) {
        // if (campo.tipo.titulo === 'Sintomatología') { debugger; }
        switch (campo.tipo.tipoCampo) {
          case 'B':
          case 'R':
            let value = noInfoMessage;
            if (!campo.valor) { return; }
            // Asign the correct value by matching the campo.valor entry with the valoresPosibles array of values
            campo.tipo.valoresPosibles.forEach(valorPosible => {
              // tslint:disable-next-line: triple-equals
              if (valorPosible.value == campo.valor) {
                value = valorPosible.label;
              }
            });

            // Check if the property is repeated, and if so add string to the value of the map to show all of them
            // tslint:disable-next-line: triple-equals
            if (brief.fields.has(campo.tipo.titulo)) {
              // Get the current value, it'll be the first in the list
              const prevValue = brief.fields.get(campo.tipo.titulo);
              // transform the next value to lowercase to append to the list
              const nextValue = value.toLowerCase();
              // and append
              brief.fields.set(campo.tipo.titulo, `${prevValue}, ${nextValue}`);
            } else {
              brief.fields.set(campo.tipo.titulo, value);
            }
            break;
          case 'C':
          case 'T':
          case 'L':
          default:
            brief.fields.set(campo.tipo.titulo, campo.valor || noInfoMessage);
        }
      }
    });

    if (!rawBrief.filesInfo) {
      return brief;
    }

    rawBrief.filesInfo.forEach(fileDetails => {
      filenameIndex++;
      const singleFileInfo = new AttachmentModel();
      singleFileInfo.title = null;
      singleFileInfo.idCard = null;
      singleFileInfo.id = fileDetails.id;
      singleFileInfo.studyUID = fileDetails.studyUID;
      singleFileInfo.serieUID = fileDetails.serieUID;
      singleFileInfo.objectUID = fileDetails.objectUID;
      singleFileInfo.fileExtension = fileDetails.fileExtension;
      singleFileInfo.fileName = `Adjunto-${filenameIndex}`;

      brief.attachment.push(singleFileInfo);
    });
    return brief;
  }

  mapToAppointmentModel(data, needsLabel: boolean) {
    let listOfAppointments = [];
    let nearestFlag = needsLabel ? true : false;

    if (!data.entry) {
      return listOfAppointments;
    }

    data.entry.forEach(singleEntry => {
      const appointment = new AppointmentModel();
      appointment.id = singleEntry.resource.id;

      // Get date in Date() format and time from datetime string
      const splitDate = singleEntry.resource.fechaCita.split(' ');
      // Reorder the date from dd/mm/yyyy format to yyyy/mm/dd
      let internationalFormatDate = splitDate[0].split('/');
      internationalFormatDate = internationalFormatDate.reverse().join('-');
      // Extract the right values from the processed original dateString
      appointment.date = new Date(internationalFormatDate);
      appointment.time = splitDate[1];

      appointment.speciality = singleEntry.resource.descripcionServicio;
      appointment.doctor = null;
      appointment.hospital = singleEntry.resource.descripcionCentro;
      appointment.address = null;
      appointment.phone = null;
      appointment.type = singleEntry.resource.codigoServicio;
      appointment.assessments = null;
      appointment.isNearestElement = nearestFlag ? true : false;
      appointment.appointmentExtension = null;

      // Set this flag to fase so only the first element in the mapping will be the nearest
      nearestFlag = false;
      listOfAppointments.push(appointment);
    });

    // When the list of appointments is full, group by month with this function. But the list can also be returned ungrouped
    listOfAppointments = this.sortAppointmentsByMonth(listOfAppointments);

    return listOfAppointments;
  }

  MapPushNotificationToMedicationModel(data) {
    // console.log(data);
    const split = data.tipo[0].antirrabia.split('#');
    // console.log(split);
    // console.log('===//===');
    return split;
  }

  public sortAppointmentsByMonth(listOfAppointments) {
    const result = [];
    const monthsArray = [];

    // Extract all the months as strings from the response
    for (const appointment of listOfAppointments) {
      monthsArray.push(this.getDateAsMonthYearString(appointment));
    }
    // Filter the array to show each value only once
    const uniqueMonths = Array.from(new Set(monthsArray));

    // Now, iterate over listOfAppointments and create the object
    uniqueMonths.forEach(uniqueMonth => {
      const group = new GroupedAppointmentModel();
      group.month = uniqueMonth;
      group.appointments = [];

      listOfAppointments.forEach(appointment => {
        const appointmentDate = this.getDateAsMonthYearString(appointment);

        if (group.month === appointmentDate) {
          group.appointments.push(appointment);
        }
      });
      result.push(group);
    });

    return result;
  }

  getDateAsMonthYearString(appointment): string {
    // Get a string with names and transform it into an array
    const monthsAsString = this.translate.instant('COMMON.MONTHS');
    const months = monthsAsString.split(' ');

    return `${months[appointment.date.getMonth()]} ${appointment.date.getFullYear()}`;
  }
}
