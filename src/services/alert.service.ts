import { Injectable, OnDestroy } from '@angular/core';
import { AlertController, LoadingController } from '@ionic/angular';
import { ToastService } from 'src/services/toast.service';
import { fromEvent, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AlertService implements OnDestroy {
  loading: any = null;

  private backbuttonSubscription: Subscription;

  constructor(
    private alertCtlr: AlertController,
    private loadCtlr: LoadingController,
    private toast: ToastService,
  ) {
    const backButtonEvent = fromEvent(window, 'popstate');
    this.backbuttonSubscription = backButtonEvent.subscribe(() => {
      this.alertCtlr.getTop().then((alert) => {
        if (alert) {
          this.alertCtlr.dismiss();
        }
      });
    });
  }

  async presentAlert(header: string, message: string, buttons: any[], subHeader?: string) {
    const alert = await this.alertCtlr.create({
      header,
      subHeader,
      message,
      buttons
    });
    await alert.present();
  }

  async presentAlertWithButtons(
    header: string,
    message: string,
    buttons: any,
    subHeader?: string,
    cssClass?: string,
    backdropDismiss?: boolean,
  ) {
    const alert = await this.alertCtlr.create({
      header,
      subHeader,
      message,
      buttons,
      backdropDismiss,
      cssClass,
    });
    await alert.present();
  }

  async presentLoadingDialog(message) {
    this.loading = await this.loadCtlr.create({
      message,
      spinner: 'bubbles'
    });
    await this.loading.present();
  }

  async dismissLoadingDialog() {
    if (this.loading) {
      this.loading.dismiss();
    }
  }

  public presentToast(message, time = 4000, cssClass = 'med-toast-info') {
    this.toast.presentToast(message, time, cssClass);
  }
  public presentToastWithOnDismiss(message, time = 4000, cssClass = 'med-toast-info', cb?: any) {
    this.toast.presentToastWithOnDismiss(message, time, cssClass, cb);
  }

  public ngOnDestroy() {
    if (this.backbuttonSubscription) {
      this.backbuttonSubscription.unsubscribe();
    }
  }
}
