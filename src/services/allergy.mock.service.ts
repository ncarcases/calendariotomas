import { Injectable } from '@angular/core';
import { HealthCardModel } from 'src/models/health-card.model';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class AllergyMockService {
    constructor(private translate: TranslateService) {}

    public getAllegies(): HealthCardModel {
        const result: HealthCardModel = new HealthCardModel();

        result.category = this.translate.instant('BASIC-DATA.ALLERGIES.LABEL');
        result.cssClassIcon = 'icon-bida-alergias';
        result.cssBackgroundColor = 'rgba(47, 165, 226, 0.1)';
        result.cssIconColor = 'rgba(47, 165, 226)';
        result.details = ['Polen de olivo', 'Ibuprofeno', 'Uranio'];
        result.lastUpdated = new Date('2019/08/20');

        return result;
    }
}
