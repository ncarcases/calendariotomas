import { Injectable } from '@angular/core';
import { BaseHttpService } from './basehttp.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AllergyService extends BaseHttpService {

  allergiesUrl = 'bidafarma/fhir/rest/AlergiaDTO?_format=json&_query=alergias&codPaciente=';

  constructor(
    protected currentConfig: ConfigGlobalModel,
    protected httpClient: HttpClient) {
      super(currentConfig, httpClient);
    }

    public async getAllegies(): Promise<any> {
      const url = `${this.currentConfig.config.baseUrl}${this.allergiesUrl}${this.currentConfig.config.codPaciente}`;
      const response: any = await this.getRequest(url, null);
      return response;
    }
}


