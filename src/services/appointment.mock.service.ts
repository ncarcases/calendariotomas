import { Injectable } from '@angular/core';
import { AppointmentModel } from '../models/appointment.model';

@Injectable()
export class AppointmentMockService {
    getAppointments(): AppointmentModel[] {
        const result: AppointmentModel[] = [];

        const aux1: AppointmentModel = new AppointmentModel();
        aux1.address = 'Calle Velázquez 3';
        aux1.date = new Date('10/01/2019');
        aux1.doctor = 'Enrique Ojeda García';
        aux1.hospital = 'Farmacia IN-PLAYA C.B. (Isabel Blázquez Jerez)';
        aux1.id = 100;
        aux1.isNearestElement = true;
        aux1.phone = '914805000';
        aux1.speciality = 'Dermatología';
        aux1.time = '9:00';
        aux1.type = 'Dermatología';
        result.push(aux1);

        const aux2: AppointmentModel = new AppointmentModel();
        aux2.address = 'Calle Ribera 4';
        aux2.date = new Date('10/23/2019');
        aux2.doctor = 'Farmacia Guillén Aspilche';
        aux2.hospital = 'Farmacia Guillén Aspilche';
        aux2.id = 100;
        aux2.isNearestElement = false;
        aux2.phone = '914805001';
        aux2.speciality = 'Dermatología';
        aux2.time = '13:30';
        aux2.type = 'Dermatología';
        result.push(aux2);

        return result;
    }

    getHistory(): AppointmentModel[] {
        const result: AppointmentModel[] = [];
        const aux1: AppointmentModel = new AppointmentModel();
        aux1.address = 'Calle Velázquez 3';
        aux1.date = new Date('05/06/2019');
        aux1.doctor = 'Gema Ortega';
        aux1.hospital = 'Gema Ortega';
        aux1.id = 100;
        aux1.isNearestElement = true;
        aux1.phone = '914805000';
        aux1.speciality = 'Dermatología';
        aux1.time = '12:00';
        aux1.type = 'Dermatología';
        result.push(aux1);

        const aux2: AppointmentModel = new AppointmentModel();
        aux2.address = 'Calle Ribera 4';
        aux2.date = new Date('03/04/2019');
        aux2.doctor = 'Alonso Quijano';
        aux2.hospital = 'Fcia. Valdivia Duro, C.B.';
        aux2.id = 100;
        aux2.isNearestElement = false;
        aux2.phone = '914805001';
        aux2.speciality = 'Dermatología';
        aux2.time = '13:30';
        aux2.type = 'Dermatología';
        result.push(aux2);

        const aux3: AppointmentModel = new AppointmentModel();
        aux3.address = 'Calle Ribera 4';
        aux3.date = new Date('01/15/2019');
        aux3.doctor = 'Eduardo Dato';
        aux3.hospital = 'Eduardo Dato';
        aux3.id = 100;
        aux3.isNearestElement = false;
        aux3.phone = '914805001';
        aux3.speciality = 'Dermatología';
        aux3.time = '13:30';
        aux3.type = 'Dermatología';
        result.push(aux3);

        // const aux4: AppointmentModel = new AppointmentModel();
        // aux4.address = 'Calle Ribera 4';
        // aux4.date = new Date('03/02/2019');
        // aux4.doctor = 'Rocío García Gómez';
        // aux4.hospital = 'Hospital de las Pruebas Mock3';
        // aux4.id = 100;
        // aux4.isNearestElement = false;
        // aux4.phone = '914805001';
        // aux4.speciality = 'Traumatología';
        // aux4.time = '11:30';
        // aux4.type = 'Traumatología';
        // result.push(aux4);

        return result;
    }
}
