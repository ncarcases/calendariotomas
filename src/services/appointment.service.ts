import { Injectable } from '@angular/core';
import { BaseHttpService } from './basehttp.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService extends BaseHttpService {

  date = new Date();
  currentDay: any = this.convertToTwoDigitsFormat(this.date.getDate());
  currentMonth: any = this.convertToTwoDigitsFormat(this.date.getMonth() + 1);
  past = `${this.currentDay}/${this.currentMonth}/${this.date.getFullYear() - 2}`;
  today = `${this.currentDay}/${this.currentMonth}/${this.date.getFullYear()}`;
  future = `${this.currentDay}/${this.currentMonth}/${this.date.getFullYear() + 2}`;

  constructor(
    protected currentConfig: ConfigGlobalModel,
    protected httpClient: HttpClient) {
      super(currentConfig, httpClient);
    }

  public async getNextAppointments(): Promise<any> {
    const nextAppointmentUrl = `bidafarma/fhir/rest-pag/CitaDTO?_format=json&_sort:asc=fechaCita&tipoFecha=C&fechaInicio=${this.today}&fechaFin=${this.future}&codPaciente=${this.currentConfig.config.codPaciente}`;

    const url = `${this.currentConfig.config.baseUrl}${nextAppointmentUrl}`;
    const response: any = await this.getRequest(url, null);
    return response;
  }

  public async getPastAppointments(): Promise<any> {
    const pastAppointmentUrl = `bidafarma/fhir/rest-pag/CitaDTO?_format=json&_sort:desc=fechaCita&tipoFecha=C&fechaInicio=${this.today}&fechaFin=${this.past}&codPaciente=${this.currentConfig.config.codPaciente}`;

    const url = `${this.currentConfig.config.baseUrl}${pastAppointmentUrl}`;
    const response: any = await this.getRequest(url, null);
    return response;
  }

  convertToTwoDigitsFormat(digit) {
    return digit < 10 ? `0${digit}` : digit;
  }
}



