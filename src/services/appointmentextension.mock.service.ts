import { Injectable } from '@angular/core';
import { AppointmentExtension } from 'src/models/appointmentextension.model';
import { BriefModel } from 'src/models/brief.model';
import { AttachmentModel } from 'src/models/attachment.model';
import { InformationModel } from 'src/models/information.model';

@Injectable()
export class AppointmentExtensionMockService {
    public getRandomAppointmentExtensionData(): AppointmentExtension {
        const result: AppointmentExtension = new AppointmentExtension();

        const auxLength = Math.floor((Math.random() * 10) + 1);
        result.briefs = [];
        for (let index = 0; index < auxLength; index++) {
            const aux: BriefModel = new BriefModel();

            // Title and subtitle.
            aux.title = 'Título Informe ' + index;

            // briefs.
            aux.fields = new Map();
            let auxNumberOfElements = Math.floor((Math.random() * 10) + 1);
            while (auxNumberOfElements-- > 0) {
                aux.fields.set(`label ${auxNumberOfElements.toString()}`, `value ${auxNumberOfElements.toString()}`);
            }

            // attachment.
            aux.attachment = [];
            auxNumberOfElements = Math.floor((Math.random() * 10) + 1);
            while (auxNumberOfElements-- > 0) {
                const auxAttachment: AttachmentModel = new AttachmentModel();
                auxAttachment.fileExtension = 'pdf';
                auxAttachment.fileName = 'Nombre ' + auxNumberOfElements.toString();
                auxAttachment.id = auxNumberOfElements.toString();
                auxAttachment.idCard = 'IdTarjeta ' + auxNumberOfElements.toString();
                auxAttachment.objectUID = 'ObjectUID';
                auxAttachment.serieUID = 'serieUID';
                auxAttachment.studyUID = 'studyUID';
                auxAttachment.title = 'Anexo ' + index.toString() + ' ' + auxNumberOfElements.toString();
                aux.attachment.push(auxAttachment);
            }

            // append briefs.
            result.briefs.push(aux);
        }

        // more info.
        result.information = new InformationModel();
        result.information.title = 'Estudio realizado por';
        result.information.fields = [];
        result.information.fields.push({
          label: 'Dirección',
          value: 'Hospital VIAMED Santa Ángela de la Cruz – Telederma',
          type: 'ADDRESS',
          cssIconClass: 'icon-hospital-o'
        });
        result.information.fields.push({
          label: 'Teléfono',
          value: '958 784 741',
          type: 'PHONE',
          cssIconClass: 'icon-bida-phone'
        });
        result.information.fields.push({
          label: 'Web',
          value: 'https://www.dermavitsalud.com',
          type: 'URL',
          cssIconClass: 'icon-bida-red'
        });
        return result;
    }
}
