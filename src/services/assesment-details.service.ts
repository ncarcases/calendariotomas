import { Injectable } from '@angular/core';
import { BaseHttpService } from './basehttp.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class AssesmentDetailsService extends BaseHttpService {

  constructor(
    protected currentConfig: ConfigGlobalModel,
    protected httpClient: HttpClient
    ) {
      super(currentConfig, httpClient);
    }

  async getAssesmentDetails(assesmentID) {
    // console.log('id de la cita para petición: ' + assesmentID);

    const assesmentDetailsUrl = `bidafarma/fhir/rest/ValoracionDTO?_format=json&idCita=${assesmentID}&_sort:desc=fchCreacion`;

    const url = `${this.currentConfig.config.baseUrl}${assesmentDetailsUrl}`;
    const response: any = await this.getRequest(url, null);
    return response;
  }

  async getBriefDetails(briefID) {
    // console.log(`Pidiendo datos del informe con identificador ${briefID}`);
    /*
    every assesment has briefs inside, could be 0, could be multiple
    When one assesment is clicked, the bar appears.
    When clicked in the file icon, brief data is downloaded
    It is returned to the component, which should send it to mapper repository and get back the curated information.
    */
    const briefDetailsUrl = `bidafarma/fhir/rest/ValoracionDTO/`;
    const url = `${this.currentConfig.config.baseUrl}${briefDetailsUrl}${briefID}`;
    const response: any = await this.getRequest(url, null);
    return response;
  }

  async getDataForInfoModal(cita) {
    const infoForModalUrl = `bidafarma/fhir/rest-pag/CitaDTO/`;
    const idCita = cita.id;

    const url = `${this.currentConfig.config.baseUrl}${infoForModalUrl}${idCita}`;
    const response: any = await this.getRequest(url, null);
    return response;
  }


}
