import { Injectable } from '@angular/core';
import { BaseHttpService } from './basehttp.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AssesmentService extends BaseHttpService {

  assesmentsUrl = 'bidafarma/fhir/rest-pag/CitaDTO?_format=json&_sort:desc=fechaCita&codPaciente=';
  constructor(
    protected currentConfig: ConfigGlobalModel,
    protected httpClient: HttpClient
  ) {
      super(currentConfig, httpClient);
    }

  public async getmedicalAssestments() {
    const url = `${this.currentConfig.config.baseUrl}${this.assesmentsUrl}${this.currentConfig.config.codPaciente}`;

    const response: any = await this.getRequest(url, null);
    return response;
  }

}
