import { Injectable } from '@angular/core';
import { AssesmentModel } from '../models/assesment.model';

@Injectable()
export class AssessmentMockService {
    getMedicalAssesment(): AssesmentModel[] {
      const result: AssesmentModel[] = [];

      const aux1: AssesmentModel = new AssesmentModel();
      aux1.id = 100;
      aux1.healthCenter = 'Fake Hospital';
      aux1.serviceType = 'Dermatología';
      aux1.date = new Date('05/06/2019');
      result.push(aux1);

      const aux2: AssesmentModel = new AssesmentModel();
      aux2.id = 101;
      aux2.healthCenter = 'Hospital Central';
      aux2.serviceType = 'Dermatología';
      aux2.date = new Date('03/04/2019');
      result.push(aux2);

      const aux3: AssesmentModel = new AssesmentModel();
      aux3.id = 102;
      aux3.healthCenter = 'Hospital Random';
      aux3.serviceType = 'Cardiología';
      aux3.date = new Date('01/15/2019');
      result.push(aux3);

      const aux4: AssesmentModel = new AssesmentModel();
      aux4.id = 103;
      aux4.healthCenter = 'Hospital Hospitalario',
      aux4.serviceType = 'Endocrinología',
      aux4.date = new Date('08/26/1998');
      result.push(aux4);

      return result;
    }
}
