import { Injectable } from '@angular/core';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { Storage } from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    protected currentConfig: ConfigGlobalModel,
    public storage: Storage,
  ) { }

  public async isAuthenticated() {
    const storageToken = await this.storage.get('token');
    const temporaryToken = this.currentConfig.config.token;
    if (!this.currentConfig.config.token) {
      if (!storageToken) {
        return false;
      }
    }
    const { exp } = this.parseJwt(temporaryToken ? temporaryToken : storageToken);
    const now = new Date().getTime();
    if (exp && now < exp * 1000) {
      return true;
    }
    return false;
  }

  private parseJwt(token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(
        atob(base64)
            .split('')
            .map((c) => {
                return (
                    '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
                );
            })
            .join('')
    );
    return JSON.parse(jsonPayload);
  }
}
