import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SessionHttp } from './sessionhttp.abstract';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';

/**
 * This class provides a common token expiration threatment.
 * When there is an expired error, it request for a new token and
 * it launches the request again. Transparent to the service provider which simply
 * delegates the HttpRequest.
 * This class is dumb, if something breaks; it sends an exception to the upper layer.
 */
export class BaseHttpService implements SessionHttp {
  private MAX_RETRIES = 2;

  constructor(
    protected currentConfig: ConfigGlobalModel,
    protected httpClient: HttpClient) {
  }

  async getRequest(url: string, inputheader: { headers: HttpHeaders }, responseTypeInput?: string): Promise<any> {
    if (!this.currentConfig || !this.currentConfig.config.token) {
      throw new Error('No token available');
    }

    let response: any;
    let retries: number = this.MAX_RETRIES;
    let currentHeader: any;
    if (inputheader) {
      // received header in the args, use it
      currentHeader = inputheader;
    } else {
      // create default header
      currentHeader = new HttpHeaders()
      .set('Authorization', `Bearer ${this.currentConfig.config.token}`)
      .set('Content-type', 'application/json');
    }

    while (retries-- > 0) {
      try {
        if (responseTypeInput) {
          response = await this.httpClient.get(url, {headers: currentHeader, responseType: 'blob'}).toPromise();
        } else {
          response = await this.httpClient.get(url, { headers: currentHeader }).toPromise();
        }

        if (response != null) {
          break;
        }
      } catch (err) {
        console.error('HTTP Get Request error: ' + JSON.stringify(err));
        if (retries === 0) {
          // there is and error, but not derived from expired token.
          throw new Error('Max. retries consumed');
        } else {
          // auth error, request token.
          try {
            // TODO: try to refresh token.
            throw new Error('Method not implemented.');
          } catch (err) {
            // no token, cannot request, throw error.
            throw new Error(err);
          }
        }
      }
    }
    return response;
  }

  async postRequest(url: string, postData: any, inputheader?: { headers: HttpHeaders; }): Promise<any> {
    if (!this.currentConfig || !this.currentConfig.config.token) {
      throw new Error('No token available');
    }

    let response: any;
    let retries: number = this.MAX_RETRIES;
    let currentHeader: any;
    if (inputheader) {
      // there is an special header that was sent from the above layer, take it.
      currentHeader = inputheader;
    } else {
      // default security header
      currentHeader = { headers: new HttpHeaders()
        .set('Authorization', `Bearer ${this.currentConfig.config.token}`)
        .set('Content-type', 'application/json')};
    }

    while (retries-- > 0) {
      try {
        response = await this.httpClient.post(url, postData, { ...currentHeader }).toPromise();
        if (response != null) {
          break;
        }
      } catch (err) {
        console.error('HTTP Post Request error: ' + JSON.stringify(err));
        if (retries === 0) {
          // there is and error, but not derived from expired token.
          throw new Error('Max. retries consumed');
        } else {
          try {
          // TODO: try to refresh token.
            throw new Error('Method not implemented.');
          } catch (err) {
            // no token, cannot request, throw error.
            throw new Error(err);
          }
        }
      }
    }
    return response;
  }

  async deleteRequest(url: string, inputheader?: { headers: HttpHeaders }, responseTypeInput?: string) {
    if (!this.currentConfig || !this.currentConfig.config.token) {
      throw new Error('No token available');
    }

    let response: any;
    let retries: number = this.MAX_RETRIES;
    let currentHeader: any;
    if (inputheader) {
      // received header in the args, use it
      currentHeader = inputheader;
    } else {
      // create default header
      currentHeader = new HttpHeaders()
      .set('Authorization', `Bearer ${this.currentConfig.config.token}`)
      .set('Content-type', 'application/json');
    }

    while (retries-- > 0) {
      try {
        if (responseTypeInput) {
          response = await this.httpClient.delete(url, {headers: currentHeader, responseType: 'blob'}).toPromise();
        } else {
          response = await this.httpClient.delete(url, { headers: currentHeader }).toPromise();
        }

        if (response != null) {
          break;
        }
      } catch (err) {
        console.error('HTTP Delete Request error: ' + JSON.stringify(err));
        if (retries === 0) {
          // there is and error, but not derived from expired token.
          throw new Error('Max. retries consumed');
        } else {
          // auth error, request token.
          try {
            // TODO: try to refresh token.
            throw new Error('Method not implemented.');
          } catch (err) {
            // no token, cannot request, throw error.
            throw new Error(err);
          }
        }
      }
    }
    return response;
  }
}
