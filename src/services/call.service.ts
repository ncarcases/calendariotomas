import { Injectable } from '@angular/core';

@Injectable()
export class CallService {
    public makePhoneCall(phoneNumber: string): boolean {
        let result = false;

        if (phoneNumber) {
            try {
                window.open('tel:' + phoneNumber, '_system');
                result = true;
            } catch (err) {
                console.error('Phone call: ' + JSON.stringify(err));
            }
        } else {
            console.error('No phonenumber to call.');
        }

        return result;
    }
}
