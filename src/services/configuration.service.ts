import { Injectable } from '@angular/core';
import { AlertInput } from '@ionic/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AlertController } from '@ionic/angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { Storage } from '@ionic/storage';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageKeys } from 'src/models/localstorage.keys.model';

@Injectable()
export class ConfigurationService {
    public config: any;
    constructor(
      private httpClient: HttpClient,
      private alertController: AlertController,
      private currentEnvironment: ConfigGlobalModel,
      public storage: Storage,
      private translate: TranslateService
    ) {}

    public loadJson(source: string): Observable<any> {
      const headers = new HttpHeaders();
      headers.append('Cache-control', 'no-cache');
      headers.append('Cache-control', 'no-store');
      headers.append('Expires', '0');
      headers.append('Pragma', 'no-cache');
      return this.httpClient.get(source, {headers}).pipe(map(response => {
        if (response) {
          this.config = response;
        } else {
          throw new Error('No data');
        }
      }));
    }

    public async presentEnvironmentSelector() {
      // pop-up must appear only when 2 or more options are available.
      const availableInputs: any = this.getOptions(this.config.environments);
      if (availableInputs && availableInputs.length > 1) {
        console.debug('More than 1 option, present environment selector.');
        const alert = await this.alertController.create({
          header: this.translate.instant('ENVIRONMENT-SELECTOR.ENVIROMENTS'),
          subHeader: this.translate.instant('ENVIRONMENT-SELECTOR.SUBHEADER'),
          message: this.translate.instant('ENVIRONMENT-SELECTOR.SELECT'),
          inputs: availableInputs,
          buttons: [
            {
              text: this.translate.instant('COMMON.OK'),
              handler: (data: string) => {
                this.currentEnvironment.config = this.config[data];
                // Save selected environment to app data
                this.storage.set(LocalStorageKeys.KEY_ENVIRONMENT, data);
                console.log('New environment selected:', data);
                console.log('BaseUrl:', this.currentEnvironment.config.baseUrl);
              }
            }
          ],
          backdropDismiss: false
        });
        await alert.present();
      } else {
        console.debug('Only one option, use it');
        if (availableInputs && availableInputs[0] && availableInputs[0].value) {
          this.currentEnvironment.config = this.config[availableInputs[0].value];
          // Save selected environment to app data
          this.storage.set(LocalStorageKeys.KEY_ENVIRONMENT, availableInputs[0].value);
          console.log('New environment selected:', availableInputs[0].value);
          console.log('BaseUrl:', this.currentEnvironment.config.baseUrl);
        } else {
          console.error('FATAL: see config.json an assure there is 1 env. at least.');
        }
      }
    }

    public getEnvironmentInfo(environment: string): any {
      let result: any = null;
      if (this.config) {
        result = this.config[environment];
      } else {
        result = null;
      }
      return result;
    }

    getOptions(environments: any): AlertInput[] {
      const result: AlertInput[] = [];
      let id = 0;
      environments.forEach(environment => {
        const aux: any = {
          type: 'radio',
          name: environment,
          value: environment,
          label: environment,
          checked: id === 0,
          disabled: false,
          id: environment
        };
        id++;
        result.push(aux);
      });
      return result;
    }
}
