import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { HealthCardModel } from 'src/models/health-card.model';

@Injectable()
export class ContraindicationMockService {
    constructor(private translate: TranslateService) {}

    public getContraindications(): HealthCardModel {
        const result: HealthCardModel = new HealthCardModel();

        result.category = this.translate.instant('BASIC-DATA.CONTRAINDICATIONS.LABEL');
        result.cssClassIcon = 'icon-bida-contraindicaciones';
        result.details = ['Ibuprofeno', 'Aspirina', 'Kriptonita'];
        result.cssBackgroundColor = 'rgba(253, 141, 154, 0.1)';
        result.cssIconColor = 'rgb(253, 141, 154)';
        result.lastUpdated = new Date('2019/06/14');

        return result;
    }
}
