import { Injectable } from '@angular/core';
import { BaseHttpService } from './basehttp.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ContraindicationsService extends BaseHttpService {
  contraindicationsUrl = 'bidafarma/fhir/rest/ContraindicacionDTO?_format=json&_query=contraindicaciones&codPaciente=';

  constructor(
    protected currentConfig: ConfigGlobalModel,
    protected httpClient: HttpClient) {
      super(currentConfig, httpClient);
    }

  public async getContraindications(): Promise<any> {
    const url = `${this.currentConfig.config.baseUrl}${this.contraindicationsUrl}${this.currentConfig.config.codPaciente}`;

    const response: any = await this.getRequest(url, null);
    return response;
  }
}
