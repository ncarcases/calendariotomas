import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { BehaviorSubject } from 'rxjs';
import { browserDBInstance } from '../app/config/browserdb';

declare var window: any;

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  private database: SQLiteObject;
  public dbInstance: any;
  private dbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private platform : Platform,
    private sqlite   : SQLite
  ) {
    this.init();
  }



  async init() {
    if (window.hasOwnProperty('cordova')) {

        this.platform.ready().then(() => {
            this.dbInstance = this.sqlite
                .create({
                    name: 'sqlite_bidafarma_native.db',
                    location: 'default',
                })
                .then((db: SQLiteObject) => {
                    db.executeSql('pragma foreign_keys = ON').then(dt => {
                      if (dt) {
                          console.log('Pragmas actualizados', dt);
                      }
                    }).catch(err => {
                        if (err) {
                            console.log('ERROR', err);
                        }
                    });
                    this.dbInstance = db;
                    this.dbReady.next(true);
                });
        });
    } else {
        const SQL_DB_NAME = 'sqlite_bidafarma_browser.db';
        const db = window.openDatabase(
            SQL_DB_NAME,
            '1.0',
            'DEV',
            5 * 1024 * 1024
        );
        this.dbInstance = browserDBInstance(db);
        this.dbReady.next(true);
    }
}

  private isReady(): Promise<void> {
    return new Promise((resolve, reject) => {
      if (this.dbReady.getValue()) {
        resolve();
      } else {
        this.dbReady.subscribe(ready => {
          if (ready) {
            resolve();
          }
        });
      }
    });
  }

  getDatabaseState() {
    return this.dbReady.asObservable();
  }
}
