// import { Injectable } from '@angular/core';
// import { ConfigGlobalModel } from '../models/config.globalmodel';
// import { HttpHeaders, HttpClient } from '@angular/common/http';
// import { BaseHttpService } from './basehttp.service';
// import { from, Observable } from 'rxjs';
// import { FCM } from '@ionic-native/fcm/ngx';
// import { switchMap, take } from 'rxjs/operators';
// import { generateUUID } from './medications/utils/uuid';
// import { AlertService } from './alert.service';
// import { TranslateService } from '@ngx-translate/core';
// import { Router } from '@angular/router';
// import { ModalController } from '@ionic/angular';

// @Injectable({
//   providedIn: 'root'
// })
// export class FirebaseTokenService extends BaseHttpService {

//   private firebaseToken: string;

//   constructor(
//     protected currentConfig: ConfigGlobalModel,
//     protected http: HttpClient,
//     private fcm: FCM,
//     private alertService: AlertService,
//     private translate: TranslateService,
//     private router: Router,
//     private modalController: ModalController,
//   ) {
//     super(currentConfig, http);
//   }

//     public getToken()
//     {
//         this.fcm.getToken().then( data =>
//         {
//             console.log( "FCM TOKEN", data );
//       })
//       this.fcm.onTokenRefresh().subscribe( token =>
//       {
//         console.log("firebase token", token)
//       this.addUserFirebaseToken(token);
//     });
//     return this.activateUser().pipe(
//       take(1),
//       switchMap(() => from(this.fcm.getToken())),
//         switchMap( ( token ) =>
//         {
//             console.log( "firebase token", token );
//         this.firebaseToken = token;
//         return this.addUserFirebaseToken(token);
//       }),
//       switchMap(() => {
//         return this.fcm.onNotification();
//       })
//     ).subscribe((notification) => {
//       this.updateMedicationInfo(notification);
//     });

//   }

//   public updateMedicationInfo(notification) {
//     // TODO: actualizar listado de medicamentos en background
//     // TODO: Poner un mensaje deferente en función del tipo de notificación:
//     console.log(notification);
//     this.alertService.presentToastWithOnDismiss(
//       notification.title,
//       6000,
//       'med-toast-info',
//       () => {
//         this.unsetModals();
//         this.router.navigate(['/my-medication', 'overview'], {
//           queryParams: {
//             ...notification,
//           }
//         });
//       }
//     );
//   }

//   public unsetModals() { // TODO: hacer un servicio de modales donde poner esta funcionalidad
//     this.modalController.getTop().then((modal) => {
//       if (modal) {
//         modal.dismiss();
//       }
//     });
//   }

//   public activateUser(): Observable<any> {
//     const { mail, baseUrl } = this.currentConfig.config;

//     const url = `${baseUrl}backend-api/users/activate?mail=${mail}`;
//     const headers = new HttpHeaders();
//     headers.set('Content-Type', 'application/json');

//     return this.http.get(url, { headers, responseType: 'text' });
//   }

//   // TODO: utilizar el mismo que el de push.service
//   public addUserFirebaseToken(token): Observable<any> {
//     if (this.currentConfig.config.token) {

//       const { mail, baseUrl } = this.currentConfig.config;

//       const url = `${baseUrl}backend-api/users/addToken`;
//       const headers = new HttpHeaders()
//         .set('X-Request-ID', generateUUID())
//         .set('Content-Type', 'application/json');

//       const body = { token, mail };

//       return from(this.postRequest(url, body, {headers}));
//     }
//   }

//   public removeToken(mail): Observable<any> {
//     const { baseUrl } = this.currentConfig.config;

//     const url = `${baseUrl}backend-api/users/removeToken`;
//     const headers = new HttpHeaders()
//       .set('Content-Type', 'application/json');

//     const body = { token: this.firebaseToken, mail };

//     return from(this.postRequest(url, body, {headers}));
//   }

//   public unsubscribeFromNotifications() {
//     this.fcm.clearAllNotifications();
//     return  this.fcm.unsubscribeFromTopic('notifications');
//   }

// }
