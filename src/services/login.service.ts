import { Injectable } from '@angular/core';
import { ConfigGlobalModel } from '../models/config.globalmodel';
import { LoginModel } from '../models/login.model';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { LocalStorageKeys } from '../models/localstorage.keys.model';

@Injectable()
export class LoginService {
  loginPath =
    'mmobile-pre/services/public/doctoresprime/1.0-PRE/mshcHTTP/user/login/';

    url;
  constructor(
    private currentConfig: ConfigGlobalModel,
    private httpClient: HttpClient,
    public storage: Storage,
  ) {}

  /**
   * It takes login data an requests a token session.
   * If no token,
   * @param data LoginModel with username and password.
   */
  public async login(data: LoginModel): Promise<string> {
    try {
      await this.getUrlAndConcat('login');
      if (data && data.mail && data.password && this.currentConfig.config) {
        // 0. Erase previous information.
        this.currentConfig.config.token = null;
        // 1. Build query.
        if (this.currentConfig.config.baseUrl && this.currentConfig.config.baseUrl.length > 0) {
          /*
          Don't use this url for now
          const url: string = this.currentConfig.config.baseUrl + this.loginPath;
          otros entornos
          const url = `${this.currentConfig.config.baseUrl}users/login`;
          desarrollo
          const url = 'http://192.168.146.201:8080/users/login';
          */
          const headers = new HttpHeaders()
            .set('Content-Type', 'application/json');
          const body = {
            mail: data.mail,
            password: data.password
          };
          // 2. Request info
          const response: any = await this.httpClient.post(this.url, JSON.stringify(body), { headers }).toPromise();
          return response;
        } else {
          this.currentConfig.token = this.currentConfig.config.token;
          this.currentConfig.user = this.currentConfig.config.user;
          this.currentConfig.codPaciente = this.currentConfig.config.codPaciente;
          this.currentConfig.config.mail = this.currentConfig.config.mail;
        }
      } else {
        throw new Error('No data');
      }
    } catch (err) {
      console.error('Error in login.service.ts file', err.message);
      return err;
    }
  }

  public async recoverPassword(email: string) {
    await this.getUrlAndConcat('recoverPassword');
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    const body = {
      mail: email,
    };
    return this.httpClient.post(this.url, JSON.stringify(body), { headers }).toPromise();
  }

  async getUrlAndConcat(params: string) {
    await this.storage.get(LocalStorageKeys.KEY_ENVIRONMENT).then(async selectedEnv => {
      if (selectedEnv === 'Development') {
       this.url = `http://192.168.146.201:8080/users/${params}`;
      } else {
        this.url = `${this.currentConfig.config.baseUrl}backend-api/users/${params}`;
      }
    });
  }
}
