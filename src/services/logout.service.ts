import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AlertService } from './alert.service';
import { TranslateService } from '@ngx-translate/core';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {
  header = this.translate.instant('LOGOUT.HEADER');
  message = this.translate.instant('LOGOUT.MESSAGE');
  buttons = [
    {
      text: this.translate.instant('LOGOUT.YES'),
      handler: () => this.confirmLogout()
    }, {
      text: this.translate.instant('LOGOUT.NO'),
      role: 'cancel',
      handler: () => this.rejectLogout()
    }
  ];

  constructor(
    private alert: AlertService,
    private router: Router,
    private translate: TranslateService,
    private currentConfig: ConfigGlobalModel,
    public storage: Storage,
  ) {}

  logout() {
    this.alert.presentAlertWithButtons(this.header, this.message, this.buttons);
  }

  confirmLogout() {
    // TODO: put all the code to forget token and do other related things
    console.log('logout confirmed');
    this.currentConfig.config.token = null;
    this.currentConfig.config.codPaciente = null;
    this.currentConfig.config.mail = null;
    // TODO: guardar esta info en el storage
    this.storage.remove('mail');
    this.storage.remove('codPaciente');
    this.storage.remove('token');

    this.alert.presentLoadingDialog(this.translate.instant('LOGIN.LOGGIN-OUT'));
    setTimeout(() => {
      // Remove loading message and navigate to login page
      this.alert.dismissLoadingDialog();
      this.router.navigateByUrl('/login');
    }, 2000);
  }

  rejectLogout() {
    console.log('logout rejected');
  }


}
