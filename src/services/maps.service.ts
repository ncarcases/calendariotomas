import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Injectable()
export class MapsService {
    constructor(
        private translate: TranslateService,
        private iab: InAppBrowser) {}

    public navigateToAddress(address: string): boolean {
        let result = false;
        const url = this.translate.instant('SPECIAL.BASE-MAP-URL') + address;

        try {
            this.iab.create(encodeURI(url), '_system');
            result = true;
        } catch (err) {
            console.error(JSON.stringify(err));
        }
        return result;
    }
}
