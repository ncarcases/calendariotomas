import { Injectable, LOCALE_ID } from '@angular/core';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import * as moment from 'moment';
import { MedicationSQLService } from './medication.sqlservice';
import { TranslateService } from '@ngx-translate/core';
import { Platform } from '@ionic/angular';
import { MedicationFrequency, MedicationTake } from '../../app/models/medication';
import { setMedicineStatus } from './utils/set-status-medicine';
import { TakesSQLService } from '../takes/takes.sqlservice';
import { from } from 'rxjs';
import { switchMap, take } from 'rxjs/operators';


import { Plugins } from '@capacitor/core';
const { MedNotifications } = Plugins;


@Injectable({
    providedIn: 'root'
})

export class MedicationRemindersService {

    public today;

    constructor(
        private localNotifications: LocalNotifications,
        private dbService: MedicationSQLService,
        private takesSQLService: TakesSQLService,
        private translate: TranslateService,
        private platform: Platform
    ) {
        if (this.platform.is( 'ios' ) ) {
            MedNotifications.initNotifications();
        }
     }

    async createReminders( medItem )  {
        console.log( medItem );
        this.today = moment();
        let startDate = moment(medItem.startDate, 'YYYY-MM-DD');
        const endDate = moment(medItem.endDate + ' 23:59', 'YYYY-MM-DD hh:mm');
        const startDiffToday = startDate.diff(this.today);
        startDate = startDiffToday > 0 ? startDate : this.today.clone();

        if (endDate.isValid()) {
            const diff = endDate.diff(startDate, 'days');
            medItem.durationInDays = diff;
            // * Identificar el caso según el valor del campo "frequency", donde 1 = todos los días; 2 = días alternos (ej.: lunes, miércoles, viernes) y 3 = intervalo de días (ej.: cada 15 días)
            switch (medItem.frequency) {
                // * Medicamento con frecuencia diaria, se crean tantas alertas diarias como tomas haya en el objeto "medItem.takes", para cada uno de los días
                case MedicationFrequency.EVERYDAY:
                this.iterateThroughDurationAndTakes(medItem, startDate, medItem.durationInDays);
                break;
                case MedicationFrequency.INTERVAL:
                let numVeces = medItem.frequencyInterval ? medItem.durationInDays / medItem.frequencyInterval : medItem.durationInDays;
                numVeces = Math.floor(numVeces);
                this.iterateThroughDurationAndTakes(medItem, startDate, numVeces);
                break;
                case MedicationFrequency.ALTERNATING:
                this.iterateThroughDurationAndTakes(medItem, startDate, medItem.durationInDays);
                break;
            }
            console.log('==> Fin de la función CON FECHA FIN <== ');
            // * Medicamento sin fecha de finalización
        } else {
            console.log('Medicamentos SIN fecha fin');
            switch (medItem.frequency) {
                case MedicationFrequency.EVERYDAY:
                this.iterateThroughDurationAndTakes(medItem, startDate);
                break;
                case MedicationFrequency.ALTERNATING:
                this.iterateThroughDurationAndTakes(medItem, startDate);
                break;
                case MedicationFrequency.INTERVAL:
                this.iterateThroughDurationAndTakes(medItem, startDate);
                break;
            }
        }
    }

    public refreshReminders(reminderId?: number) {
        console.log('refreshReminders()', reminderId);
        if (reminderId) {
            return;
        }
        from(this.deleteAllReminders()).pipe(
            take(1),
            switchMap(() => {
                console.error('fin del borrado de recordatorios viejos');
                return from(this.createNewReminders());
            }),
            ).subscribe();
    }

    // * Método para borrar uno o más recordatorios, se puede llamar dentro de un bucle para eliminar varios (idealmente, todos los de una medicación)
    async deleteReminderById( idReminder ): Promise<any> {
        console.log( 'DELETE REMINDER BY ID ', idReminder );
        return await this.cancelReminderById(idReminder).then(() => {
        });
    }

    public async cancelReminderById(id): Promise<any> {
        await this.consoleLogs( true, id );
        return this.platform.is( 'ios' )
            ? MedNotifications.removeNotification(
                { id }
            )
            : this.localNotifications.cancel(id).then(async resp => {
            console.log('ID ' + id, resp);
            await this.consoleLogs(false, id);
        });
    }

    public async consoleLogs(before, id) {
        const todos1 = await this.localNotifications.getAll().then(resp => {
            console.error('LISTADO TODOS ' + (before ? 'ANTES' : 'DESPUES'), resp);
        });
        return;
    }

    // * Método para borrar todos los recordatorios de golpe, no se usará de forma habitual o solo para propósitos de desarrollo
    deleteAllReminders(): Promise<any> {
        return this.platform.is( 'ios' )
            ? MedNotifications.removeAllPendingNotifications()
            : this.localNotifications.cancelAll().then(
                (res) => console.log('Eliminados todos los recordatorios: ', res),
                (err) => console.error('No se han podido eliminar los recordatorios, ', err),
            );
    }

    // * Método para consultar todos los recordatorios creados. Se usará principalmente para propósitos de desarrollo.
    checkAllCreatedReminders(): Promise<any> {
        return this.localNotifications.getAll();
    }

    /*
    FREQUENCY = 1 2 o 3 tipo de frecuencia Todos los días, alternative, interval
    FREQUENCY VALUES = true false true false....
    FREQUENCY INTERVAL = en case de ser frequency 3
    */
    public async iterateThroughDurationAndTakes(medItem, startDate, numVeces?) {
        let arrayOfLocalNotificationsIds = [];
        if (typeof numVeces === 'number') {
            for (let i = 0; i <= numVeces; i++) {
                arrayOfLocalNotificationsIds =
                    arrayOfLocalNotificationsIds
                        .concat(
                            this.loopTakesNotification(
                                medItem
                                , startDate
                                , null
                                , startDate.isoWeekday() - 1
                            )
                        );
                if (!medItem.frequencyInterval) {
                    startDate.add(1, 'days');
                } else {
                    startDate.add(medItem.frequencyInterval, 'days');
                }
            }
        } else {
            // notificaciones sin fecha fin
            // ALTERNANCIA
            if (medItem && medItem.frequencyValues) {
                medItem
                    .frequencyValues.toString().split( ',' )
                    .forEach( ( dayOfWeek, index ) => {
                        arrayOfLocalNotificationsIds =
                            arrayOfLocalNotificationsIds
                                .concat(
                                    this.loopTakesNotification(
                                        medItem
                                        , startDate
                                        , medItem.frequency
                                        , index
                                    )
                                );
                    });
            }
            // EVERY DAY , INTERVAL
            arrayOfLocalNotificationsIds =
                arrayOfLocalNotificationsIds
                    .concat(
                        this.loopTakesNotification(
                            medItem
                            , startDate
                            , medItem.frequency
                            , null
                        )
                    );
        }
        console.log(arrayOfLocalNotificationsIds);
        this.dbService.saveSQLiteTakesIdToMedicineField(medItem.medicationId, arrayOfLocalNotificationsIds.toString());
    }

    loopTakesNotification(medItem, startDate, frequency, position): any[] {
        const arrayOfLocalNotificationsIds = [];
        if (this.checkAlternating(medItem.frequencyValues , position)) {
            medItem.takes.forEach(take => {
                const takeDate = new Date(
                    startDate.year(),
                    startDate.month(),
                    startDate.date(),
                    Number(take.time.substring(0, 2)),
                    Number(take.time.substring(3, 5)),
                );
                if (this.today.diff(takeDate) > 0 && medItem.endDate) { // evitamos que se notifiquen tomas del día de hoy que hayan pasado
                    return;
                }

                if (frequency === MedicationFrequency.INTERVAL) {
                    const firstLocalNotificationId = new Date().valueOf() + Math.floor(Math.random() * 10000000000000001);
                    arrayOfLocalNotificationsIds.push(firstLocalNotificationId);
                    this.scheduleNotification(
                        take,
                        firstLocalNotificationId,
                        medItem.name,
                        { at: takeDate},
                        frequency ? null : moment(takeDate)
                    );
                }
                const LocalNotificationId = new Date().valueOf() + Math.floor(Math.random() * 10000000000000001);
                arrayOfLocalNotificationsIds.push(LocalNotificationId);
                this.scheduleNotification(
                    take,
                    LocalNotificationId,
                    medItem.name,
                    this.setTrigger(takeDate, frequency, position, medItem.frequencyInterval),
                    frequency ? null : moment(takeDate)
                );
            });
            return arrayOfLocalNotificationsIds;
        }
    }

    setTrigger(take: Date, frequency, index, frequencyInterval) {
        // los casos del switch son para las tomas SIN fecha fin, el default es para las tomas con fecha fin
        const year = new Date().getFullYear();
        const month = new Date().getMonth();
        const day = new Date().getDate();

        const time1 = new Date(year, month, day, 0, 0, 0, 0);
        if (index !== null) {
            index = index + 1 ;
            index = index > 6 ? 0 : index;
        }

        const hourMinute = { hour: take.getHours(), minute: take.getMinutes(), second: 0};
        // const hour = Number(take.time.substring(0, 2));
        // const minute = Number(take.time.substring(3, 5));
        switch (frequency) {
            case MedicationFrequency.EVERYDAY:
            return {
                every : { ...hourMinute }
            };
            case MedicationFrequency.ALTERNATING:
            return {
                every : {weekday: index, ...hourMinute }
            };
            case MedicationFrequency.INTERVAL:
            return {
                every: ( Number( frequencyInterval ) )
                , unit: 'day'
                , firstAt: take
            };
            default : return { at: take };
        }
    }

    checkAlternating(frequencyValues, position) {
        return frequencyValues ? frequencyValues.toString().split(',')[position] === 'true' : true;
    }

    public scheduleNotification(takeNotification: any, id: number, name: string, trigger, fechaToma) {
        // const param = this.buildParamToSchedule(takeNotification, id, name, trigger);
        const param = {
            id,
            title: `${this.translate.instant('MY-MEDICATION.PUSH_TITLE')} ${name}`,
            text: this.translate.instant('MY-MEDICATION.PUSH_TEXT'),
            trigger,
            data: {
                takeNotification
                , notificationId: id
                , title: `${this.translate.instant('MY-MEDICATION.PUSH_TITLE')} ${name}`
                , text: this.translate.instant('MY-MEDICATION.PUSH_TEXT')
                , triggerDate: fechaToma
            },
            foreground: true,
            icon: 'res://ic_launcher.png',
            smallIcon: 'res://ic_launcher.png',
            priority: 2,
        };
        // param = this.platform.is('android') ? param : {...param, };
        // this.platform.is( 'ios' )
        console.log( 'ScheduleNotification', param );
        this.platform.is('ios')
             ? MedNotifications.scheduleNotification(param)
             : this.localNotifications.schedule(param);
    }

    public createNewReminders() {
        return this.dbService.getSQLiteMedication().then((medicines) => {
            if (medicines && medicines.length > 0) {
                return medicines;
            }
        }).then(medicines => {
            if (medicines && medicines.length > 0) {
                medicines.map(med => {
                    setMedicineStatus(med);
                    this.takesSQLService.getSQLiteTakes(med.medicationId).then((takes: MedicationTake[]) => {
                        med.takes = takes;
                        if (med.textStatus === 'activo') {
                            this.createReminders(med);
                        }
                    });
                });
            }
        });
    }
}
