import { Injectable } from '@angular/core';
import { BaseHttpService } from '../basehttp.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { HttpClient } from '@angular/common/http';
import { throwError } from 'rxjs';
import { mapToMedicationModel } from 'src/services/medications/utils/medication-mapper-model';
import { MedicationSQLService } from './medication.sqlservice';
import { TranslateService } from '@ngx-translate/core';
import { AlertService } from '../alert.service';
import { MedicationRemindersService } from './medication.reminders.service';
import { SwitchState } from '../../app/models/medication';

@Injectable({
  providedIn: 'root'
})
export class MedicationService extends BaseHttpService {
  medicationUrl = 'bidafarma/fhir/rest/PrescripcionDTO?_format=json&_query=prescripciones&codPaciente=';

  // private medication$: BehaviorSubject<MedicationModel[]> = new BehaviorSubject([]);

  public TEST_DATA = 
  {
    "resourceType":"Bundle",
    "id":"19d3bd1e-097e-43df-abab-7525ad48dfc6",
    "meta":{
        "lastUpdated":"2021-01-26T17:26:57.701+01:00"
    },
    "type":"searchset",
    "total":1,
    "link":[
        {
            "relation":"self",
            "url":"https://172.30.240.139/bidafarma/fhir/rest/PrescripcionDTO?_format=json&_query=prescripciones&codPaciente=AC00000160"
        }
    ],
    "entry":[
        {
            "fullUrl":"https://172.30.240.139/bidafarma/fhir/rest/PrescripcionesDTO/FGV8p",
            "resource":{
                "resourceType":"PrescripcionesDTO",
                "id":"FGV8p",
                "meta":{
                    "lastUpdated":"2021-01-25T00:00:00.000+01:00",
                    "tag":[
                        {
                            "system":"http://hl7.org/fhir/tag/profile",
                            "code":"http://hn.indra.es/fhir/profiles/PrescripcionesDTO"
                        }
                    ]
                },
                "prescripciones":[
                    {
                        "resourceType":"PrescripcionDTO",
                        "id":"94",
                        "nombre":"Almax 50",
                        "codPaciente":"AC00000160",
                        "prescriptionReminders":{
                            "resourceType":"PrescriptionReminderDTO",
                            "id":"469",
                            "startDate":"18/01/2021 18:44",
                            "endDate":"24/01/2021 00:00",
                            "duration":7,
                            "posologyUnit":"01",
                            "posologyUnitDescription":"Comprimidos",
                            "posologyUnitDescriptionSingular":"Comprimido",
                            "frequency":"1",
                            "frequencyDescription":"Todos los días",
                            "numberDailyTake":"10",
                            "numberDailyTakeDescription":"3 veces al día",
                            "message":"Recordatorio desde el 18 ene 2021 hasta el 24 ene 2021, todos los días<br> Comprimidos, 3 veces al día: 10:00 (1) - 11:00 (1) - 12:00 (1).",
                            "notificationActive":true,
                            "active":true,
                            "takePills":[
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1580",
                                    "time":"10:00",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1581",
                                    "time":"11:00",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1582",
                                    "time":"12:00",
                                    "quantity":"1"
                                }
                            ]
                        }
                    },
                    {
                        "resourceType":"PrescripcionDTO",
                        "id":"202",
                        "nombre":"Ibuprofeno 400 mg",
                        "codPaciente":"AC00000160",
                        "prescriptionReminders":{
                            "resourceType":"PrescriptionReminderDTO",
                            "id":"476",
                            "startDate":"26/01/2021 13:31",
                            "endDate":"01/02/2021 00:00",
                            "duration":7,
                            "posologyUnit":"02",
                            "posologyUnitDescription":"Cápsulas",
                            "posologyUnitDescriptionSingular":"Cápsula",
                            "frequency":"1",
                            "frequencyDescription":"Todos los días",
                            "numberDailyTake":"17",
                            "numberDailyTakeDescription":"10 veces al día",
                            "message":"Recordatorio desde el 26 ene 2021 hasta el 1 feb 2021, todos los días<br> Cápsulas, 10 veces al día: 16:05 (1) - 16:10 (1) - 16:15 (1) - 16:20 (1) - 16:25 (1) - 16:30 (1) - 16:35 (1) - 16:40 (1) - 16:45 (1) - 16:50 (1).",
                            "notificationActive":true,
                            "active":true,
                            "takePills":[
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1607",
                                    "time":"16:05",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1608",
                                    "time":"16:10",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1609",
                                    "time":"16:15",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1610",
                                    "time":"16:20",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1611",
                                    "time":"16:25",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1612",
                                    "time":"16:30",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1613",
                                    "time":"16:35",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1614",
                                    "time":"16:40",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1615",
                                    "time":"16:45",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1616",
                                    "time":"16:50",
                                    "quantity":"1"
                                }
                            ]
                        }
                    },
                    {
                        "resourceType":"PrescripcionDTO",
                        "id":"203",
                        "nombre":"Omeprazol 20 mg",
                        "codPaciente":"AC00000160",
                        "prescriptionReminders":{
                            "resourceType":"PrescriptionReminderDTO",
                            "id":"474",
                            "startDate":"25/01/2021 12:32",
                            "endDate":"31/01/2021 00:00",
                            "duration":7,
                            "posologyUnit":"01",
                            "posologyUnitDescription":"Comprimidos",
                            "posologyUnitDescriptionSingular":"Comprimido",
                            "frequency":"1",
                            "frequencyDescription":"Todos los días",
                            "numberDailyTake":"09",
                            "numberDailyTakeDescription":"2 veces al día",
                            "message":"Recordatorio desde el 25 ene 2021 hasta el 31 ene 2021, todos los días<br> Comprimidos, 2 veces al día: 16:00 (1) - 17:00 (1).",
                            "notificationActive":true,
                            "active":true,
                            "takePills":[
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1595",
                                    "time":"16:00",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1596",
                                    "time":"17:00",
                                    "quantity":"1"
                                }
                            ]
                        }
                    },
                    {
                        "resourceType":"PrescripcionDTO",
                        "id":"74",
                        "nombre":"Paracetamol 1gr",
                        "codPaciente":"AC00000160",
                        "prescriptionReminders":{
                            "resourceType":"PrescriptionReminderDTO",
                            "id":"471",
                            "startDate":"21/01/2021 09:18",
                            "endDate":"23/01/2021 00:00",
                            "duration":3,
                            "posologyUnit":"01",
                            "posologyUnitDescription":"Comprimidos",
                            "posologyUnitDescriptionSingular":"Comprimido",
                            "frequency":"2",
                            "frequencyDescription":"Días específicos",
                            "frequencyValues":[
                                true,
                                true,
                                true,
                                true,
                                true,
                                true,
                                true
                            ],
                            "numberDailyTake":"10",
                            "numberDailyTakeDescription":"3 veces al día",
                            "message":"Recordatorio desde el 21 ene 2021 hasta el 23 ene 2021, lun - mar - mie - juv - vie - sab - dom<br> Comprimidos, 3 veces al día: 09:25 (1) - 09:30 (1) - 09:35 (1).",
                            "notificationActive":true,
                            "active":true,
                            "takePills":[
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1586",
                                    "time":"09:25",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1587",
                                    "time":"09:30",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1588",
                                    "time":"09:35",
                                    "quantity":"1"
                                }
                            ]
                        }
                    },
                    {
                        "resourceType":"PrescripcionDTO",
                        "id":"118",
                        "nombre":"Ramipril comprimidos",
                        "codPaciente":"AC00000160",
                        "prescriptionReminders":{
                            "resourceType":"PrescriptionReminderDTO",
                            "id":"470",
                            "startDate":"20/01/2021 09:44",
                            "endDate":"29/01/2021 00:00",
                            "duration":10,
                            "posologyUnit":"01",
                            "posologyUnitDescription":"Comprimidos",
                            "posologyUnitDescriptionSingular":"Comprimido",
                            "frequency":"1",
                            "frequencyDescription":"Todos los días",
                            "numberDailyTake":"10",
                            "numberDailyTakeDescription":"3 veces al día",
                            "message":"Recordatorio desde el 20 ene 2021 hasta el 29 ene 2021, todos los días<br> Comprimidos, 3 veces al día: 09:47 (1) - 09:49 (1) - 09:51 (1).",
                            "notificationActive":true,
                            "active":true,
                            "takePills":[
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1583",
                                    "time":"09:47",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1584",
                                    "time":"09:49",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1585",
                                    "time":"09:51",
                                    "quantity":"1"
                                }
                            ]
                        }
                    },
                    {
                        "resourceType":"PrescripcionDTO",
                        "id":"77",
                        "nombre":"Seguril",
                        "codPaciente":"AC00000160",
                        "prescriptionReminders":{
                            "resourceType":"PrescriptionReminderDTO",
                            "id":"472",
                            "startDate":"21/01/2021 09:19",
                            "endDate":"23/01/2021 00:00",
                            "duration":3,
                            "posologyUnit":"01",
                            "posologyUnitDescription":"Comprimidos",
                            "posologyUnitDescriptionSingular":"Comprimido",
                            "frequency":"3",
                            "frequencyDescription":"Intervalo",
                            "frequencyInterval":"2",
                            "numberDailyTake":"10",
                            "numberDailyTakeDescription":"3 veces al día",
                            "message":"Recordatorio desde el 21 ene 2021 hasta el 23 ene 2021, cada 2 días<br> Comprimidos, 3 veces al día: 09:00 (1) - 09:10 (1) - 09:20 (1).",
                            "notificationActive":true,
                            "active":true,
                            "takePills":[
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1589",
                                    "time":"09:00",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1590",
                                    "time":"09:10",
                                    "quantity":"1"
                                },
                                {
                                    "resourceType":"TakePillDTO",
                                    "id":"1591",
                                    "time":"09:20",
                                    "quantity":"1"
                                }
                            ]
                        }
                    }
                ]
            }
        }
    ]
}
  ;
  constructor(
    protected currentConfig : ConfigGlobalModel,
    protected httpClient    : HttpClient,
    private medicationSQLService: MedicationSQLService,
    private translate: TranslateService,
    private alertSrvc: AlertService,
    private reminderService: MedicationRemindersService
  ) {
    super(currentConfig, httpClient);
  }

  // public get medication() {
  //   return this.medication$.asObservable();
  // }

  public updateSQL(prescriptions) {
    return this.medicationSQLService.addSQLiteMedication(prescriptions);
  }

  public deleteLocalRecords(prescriptions) {
    // TODO: Hacer aquí lo de eliminar las tomas que no tengan hasreminder?
    if (prescriptions && prescriptions.length > 0) {
      const stringOfIds = prescriptions.map(prescription => prescription.medicationId).join(',');
      this.medicationSQLService.deleteSQLiteMedication(stringOfIds);
    }
  }

  public async getMedicines() {
    return this.medicationSQLService.getSQLiteMedication();
  }

    public fetchMedication()  {
      console.log('INSIDE FETCH MEDICATIONS');
      const url = `${this.currentConfig.config.baseUrl}${this.medicationUrl}${this.currentConfig.config.codPaciente}`;
      let prescriptions = [];
      let medicationToUpdate;
      let medicationToDelete;
      return new Promise(async (resolve, reject) => {
      const localMedications = await this.getMedicines();
      this.getRequest( url, null ).then( async res =>  {
          console.log('GET REQUEST', res);
          if (res && res.entry &&
              res.entry.length > 0 &&
              res.entry[0].resource &&
              res.entry[0].resource.prescripciones) {
      prescriptions = res.entry[0].resource.prescripciones;
      prescriptions = mapToMedicationModel( prescriptions, this.translate );
      console.log('MAPPED PRESCRIPTIONS', prescriptions);
      const toUpdateToDelete = await this.fetchPrescriptions(prescriptions, localMedications);
      medicationToDelete = toUpdateToDelete[0];
      medicationToUpdate = toUpdateToDelete[ 1 ];
      console.log('syncing data');
      await this.syncingData(medicationToUpdate, medicationToDelete, prescriptions, localMedications);
      resolve(this.getMedicines());
        } else {
          reject (this.getMedicines());
        }
        }).catch((err) => {
          console.log('ERROR DE PETICION');
          this.alertSrvc.presentToast(this.translate.instant('COMMON.SERVER-ERROR'));
          reject (throwError(err));
        });
    });
  }

    async syncingData( medicationToUpdate, medicationToDelete, prescriptions, localMedications )    {
      console.log('SYNC DATA', medicationToUpdate, medicationToDelete, prescriptions, localMedications );
      if (medicationToDelete && medicationToDelete.length > 0) {
      await this.deleteLocalRecords(medicationToDelete);
    }
      if (medicationToUpdate && medicationToUpdate.length > 0) {
      await this.updateSQL(medicationToUpdate).then(updated => {
        for (const medicineUpdated of medicationToUpdate) {
          const ToggleButtonState = localMedications.find(localMed =>
            (localMed.medicationId === medicineUpdated.medicationId)
            && localMed.toggleNotificationButtonState === SwitchState.ON
          );
          if ( ToggleButtonState ) {
              console.log('CREATE REMINDERS');
              this.reminderService.createReminders(medicineUpdated);
          }
        }
      });
    }
      if (localMedications && localMedications.length <= 0) {
      await this.updateSQL(prescriptions).then(async updated => {
        await this.reminderService.createNewReminders();
      });
    }
  }

    async fetchPrescriptions( prescriptions, localMedications ) {
      console.log('FETCH PRESCRIPTIONS');
      return new Promise(async (resolve, reject) => {
    const medicationToDelete = [];
    let medicationToUpdate = [];
    let newPrescriptions = [];
    for (const medication of localMedications) {
      const medicationMatch = prescriptions.find(prescription => medication.medicationId === prescription.medicationId);
      if (
            ( medicationMatch && !medicationMatch.hasReminders && medication.hasReminders === 'Yes' )
            || ( medicationMatch && medication.reminderId !== medicationMatch.reminderId )
        // TODO: analizar las condiciones para que pase en el caso en que se hayan añadido recordatorios, es decir, cuando medication.hasReminders sea 'No'
        ) {
            console.log('MEDICATION MATCH');
            const tomaEliminada = await this.medicationSQLService.deleteSQLiteMedicationTakeById(medication.medicationId);
            await this.deleteLocalNotificationByMedicine(medication);
            medicationToUpdate.push(medicationMatch);
            console.log('Medication deleted after back modification');
        } else if (
                  medicationMatch &&
                  medicationMatch.medicationId === medication.medicationId &&
                  medicationMatch.name !== medication.name ) {

            console.log('NO MATCH DELETELOCALNOTIFICATIONBYMEDICINE');
            await this.deleteLocalNotificationByMedicine(medication);
            medicationToUpdate.push(medicationMatch);
      }
      if ( !medicationMatch ) {
            console.log('NO MATCH AT ALL DELETELOCALNOTIFICATIONBYMEDICINE');

            await this.deleteLocalNotificationByMedicine(medication);
            medicationToDelete.push(medication);
      }
    }
    newPrescriptions = prescriptions.filter((prescription) => !localMedications.some((local) => local.medicationId === prescription.medicationId));

    if (newPrescriptions.length > 0) {
      medicationToUpdate = medicationToUpdate.concat(newPrescriptions);
    }
    resolve([medicationToDelete, medicationToUpdate]);
});
}

  async deleteLocalNotificationByMedicine(medicine) {
    if (medicine.takeIdsAsString) {
      const reminders = medicine.takeIdsAsString.split(',');
      for (const reminder of reminders) {
        await this.reminderService.deleteReminderById(reminder);
      }
    }
  }

}
