
import { Injectable } from '@angular/core';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { SQLiteService } from '../sqlite.service';
import { MedicationModel } from 'src/models/medication.model';
import { DatabaseService } from '../database.service';
import { buildMedicineObj } from './utils/build-medication-object';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class MedicationSQLService {
    constructor(
    protected currentConfig : ConfigGlobalModel,
    private sql: SQLiteService,
    private directSQL : DatabaseService,
  ) {
  }


 /*
 ************************************************
                SQLITE METHODS
 ************************************************
 */


async addSQLiteMedication(medicines: MedicationModel[]): Promise<any>  {
      for (const medicine of medicines) {

        const medicineData = buildMedicineObj(medicine, this.currentConfig.config.codPaciente);
        const sqlMedicineStatement = `INSERT or REPLACE into medicines (
          medicationId,
          codPaciente,
          name,
          hasReminders,
          reminderId,
          startDate,
          endDate,
          durationInDays,
          remoteNotificationActive,
          frequency,
          frequencyDescription,
          frequencyInterval,
          frequencyValues,
          toggleNotificationButtonState,
          frequencyString,
          numberDailyTakeDescription,
          takeIdsAsString
          ) VALUES (
            ${medicineData[0] ? medicineData[0] : null},
            ${medicineData[1] ? "'" + medicineData[1] + "'" : null},
            ${medicineData[2] ? "'" + medicineData[2] + "'" : null},
            ${medicineData[3] ? "'" + medicineData[3] + "'" : null},
            ${medicineData[4] ? "'" + medicineData[4] + "'" : null},
            ${medicineData[5] ? "'" + medicineData[5] + "'" : null},
            ${medicineData[6] ? `'${medicineData[6]}'` : null },
            ${medicineData[7] ? `${medicineData[7]}` : null },
            ${medicineData[8] ? "'" + medicineData[8] + "'" : null},
            ${medicineData[9] ? "'" + medicineData[9] + "'" : null},
            ${medicineData[10] ? "'" + medicineData[10] + "'" : null},
            ${medicineData[11] ? "'" + medicineData[11] + "'" : null},
            ${medicineData[12] ? "'" + medicineData[12] + "'" : null},
            (Select
            CASE WHEN count(*) = 0
                THEN 'on'
                ELSE toggleNotificationButtonState END toggleNotificationButtonState
            from  medicines  where codPaciente = '${medicineData[1]}'
            AND medicationId = '${medicineData[0]}'),
            ${medicineData[13] ? "'" + medicineData[13] + "'" : null},
            ${medicineData[14] ? "'" + medicineData[14] + "'" : null},
            ${medicineData[15] ? "'" + medicineData[15] + "'" : null}
          )`;
        this.directSQL.dbInstance.executeSql(sqlMedicineStatement).then(resp => {
          if (medicine && medicine.takes && medicine.takes.length > 0 ) {
            for (const take of medicine.takes) {
              const sqlTakeStatement = 'INSERT or REPLACE into takes(medicationId, codPaciente, id, quantity, time, posology) VALUES (?, ?, ?, ?, ?, ?)';
              const takeData = [medicine.medicationId, this.currentConfig.config.codPaciente, take.id, take.quantity, take.time, take.posology];
              this.directSQL.dbInstance.executeSql(sqlTakeStatement, takeData);
            }
          }
        }).catch(error => {
          if (medicine && medicine.takes && medicine.takes.length > 0 ) {
            for (const take of medicine.takes) {
              const sqlTakeStatement = 'INSERT or REPLACE into takes(medicationId, codPaciente, id, quantity, time, posology) VALUES (?, ?, ?, ?, ?, ?)';
              const takeData = [medicine.medicationId, this.currentConfig.config.codPaciente, take.id, take.quantity, take.time, take.posology];
              this.directSQL.dbInstance.executeSql(sqlTakeStatement, takeData);
            }
          }
        });
      }
}

getSQLiteMedication(startDate?, endOfWeek?, hasRedimender?) {
  const today = moment().format('YYYY-MM-DD');
  let statement = ` WHERE codPaciente = '${this.currentConfig.config.codPaciente}'`;
  if (startDate) {
    statement = statement + `
      AND (
        strftime(endDate) > strftime('${today}')
        OR endDate IS NULL
        OR (
          strftime(startDate) BETWEEN strftime('${startDate}') AND strftime('${endOfWeek}')
          OR strftime(endDate) BETWEEN strftime('${startDate}') AND strftime('${endOfWeek}')
        )
      )`;
  }
  if (hasRedimender === 'Yes') {
      statement = statement + ` AND hasReminders = 'Yes'`;
  }
  statement = statement + ` ORDER BY name ASC`;
  return this.sql.getTableData('medicines', statement);
}

getSQLiteMedicine(id: string) {
  return this.sql.getTableData('medicines', ` WHERE medicationId = ${id} AND codPaciente = '${this.currentConfig.config.codPaciente}'`);
}

updateSQLiteMedicine(medicineId, newButtonState) {
    const sqlStatement = `UPDATE medicines SET toggleNotificationButtonState = ? WHERE medicationId = ${medicineId} AND codPaciente = '${this.currentConfig.config.codPaciente}'`;
    return this.directSQL.dbInstance.executeSql(sqlStatement, [newButtonState]);
  }

saveSQLiteTakesIdToMedicineField(medicineId, takeIdsAsString) {
    const sqlStatement = `UPDATE medicines SET takeIdsAsString = ? WHERE medicationId = ${medicineId} AND codPaciente = '${this.currentConfig.config.codPaciente}'`;
    return this.directSQL.dbInstance.executeSql(sqlStatement, [takeIdsAsString])
    .then(() => {
      console.log('id de las tomas guardado en DB');
      this.getSQLiteMedicine(medicineId).then();
    })
    .catch(err => {
      console.log(err);
      console.log('error al guardar el id de las tomas en DB');
    });
  }

  async deleteSQLiteMedication(medications: string) {
    const deleteTakesStatement = ` where medicationId IN(${medications}) AND codPaciente = '${this.currentConfig.config.codPaciente}'`;
    this.sql.deleteTableData('takes', null, null, deleteTakesStatement).then(resp => {
      console.log(resp);
      const deleteMedicationStatement = ` where medicationId IN (${medications})
      AND codPaciente = '${this.currentConfig.config.codPaciente}'`;
      this.sql.deleteTableData('medicines', null, null, deleteMedicationStatement).then(deletedMedicines => {
        console.log(deletedMedicines);
      });
    }).catch(error => {
      console.error(error);
    });

  }

  async deleteSQLiteMedicationTakeById(medicationId: string) {
    const deleteTakesStatement = ` where medicationId = ${medicationId}
    AND codPaciente = '${this.currentConfig.config.codPaciente}'`;
    return await this.sql.deleteTableData('takes', null, null, deleteTakesStatement).then((resp) => resp);
  }

 /*
 ************************************************
                SQLITE METHODS
 ************************************************
 */

}
