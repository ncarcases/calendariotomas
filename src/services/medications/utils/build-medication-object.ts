export function buildMedicineObj(medicine, codPaciente) {
    return [
      medicine.medicationId,
      codPaciente,
      medicine.name,
      medicine.hasReminders ? 'Yes' : 'No',
      medicine.reminderId,
      medicine.startDate,
      medicine.endDate,
      medicine.durationInDays ? medicine.durationInDays : null,
      medicine.remoteNotificationActive,
      medicine.frequency,
      medicine.frequencyDescription,
      medicine.frequencyInterval ? medicine.frequencyInterval : null,
      medicine.frequencyValues ? medicine.frequencyValues.toString() : null,
      medicine.frequencyString ? medicine.frequencyString : null,
      medicine.numberDailyTake ? medicine.numberDailyTake : null ,
      ''
    ];
  }
