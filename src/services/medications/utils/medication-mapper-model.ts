import { MedicationModel, MedicationTakeModel } from 'src/models/medication.model';

export function mapToMedicationModel(data, translate): MedicationModel[] {
    const meds: MedicationModel[] = [];
    data.forEach(prescripcion => {

      const med = new MedicationModel();
      // * Información genérica: id, nombre del medicamento en MyMed y si tiene recordatorios o no, además del número de recordatorios interno de MyMed si lo tiene, y si no tiene fecha de final también para bifurcar más adelante
      med.medicationId = prescripcion.id;
      med.name = prescripcion.nombre;
      med.hasReminders = prescripcion.prescriptionReminders ? true : false;
      med.reminderId = prescripcion.prescriptionReminders ? prescripcion.prescriptionReminders.id : null;
      med.hasEndDate = prescripcion.prescriptionReminders && prescripcion.prescriptionReminders.endDate ? true : false;

      // * Información de la fecha de inicio y final
      med.startDate = med.hasReminders ? (prescripcion.prescriptionReminders.startDate.substring(0, 10)).split('/').reverse().join('-') : null;
      med.endDate = med.hasReminders && med.hasEndDate ? (prescripcion.prescriptionReminders.endDate.substring(0, 10)).split('/').reverse().join('-') : null;

      // * Días totales que la medicación será tomada
      med.durationInDays = med.hasReminders ? prescripcion.prescriptionReminders.duration : null;

      // * Se usa para colorear o no el botón y el icono de la campana, se setea a null en un principio porque depende de la interacción del usuario
      // med.toggleNotificationButtonState = null;

      // * Mira si el médico o farmacéutico activó las notificaciones, así el botón de los avisos locales estará activo o no
      med.remoteNotificationActive = med.hasReminders ? prescripcion.prescriptionReminders.notificationActive : false;

      // * Información sobre la frecuencia y la repetición de la medicación en intervalos
      med.frequency = med.hasReminders ? prescripcion.prescriptionReminders.frequency : null;

      med.frequencyDescription = med.hasReminders ? prescripcion.prescriptionReminders.frequencyDescription : null;

      med.frequencyInterval = med.hasReminders ? prescripcion.prescriptionReminders.frequencyInterval : null;

      med.frequencyValues = med.hasReminders ? prescripcion.prescriptionReminders.frequencyValues : null;

      if (med.hasReminders) {
        switch (prescripcion.prescriptionReminders.frequency) {
          case '1':
            // * Expected output: "Tomar todos los días"
            med.frequencyString = translate.instant('MY-MEDICATION.MODAL.TOMAR-TODOS-DIAS');
            break;
          case '2':
            // * 1) Compose an iterable to find what days the med has to be taken.
            const days = [
              { key: 'Lun', value: med.frequencyValues[0] },
              { key: 'Mar', value: med.frequencyValues[1] },
              { key: 'Mie', value: med.frequencyValues[2] },
              { key: 'Jue', value: med.frequencyValues[3] },
              { key: 'Vie', value: med.frequencyValues[4] },
              { key: 'Sab', value: med.frequencyValues[5] },
              { key: 'Dom', value: med.frequencyValues[6] },
            ];
            // * 2) Iterate over the iterable to compose the string
            let daysStr = '';
            days.forEach(day => {
              if (day.value) {
                if (daysStr.length === 0) {
                  daysStr = day.key;
                } else {
                  daysStr = daysStr + ' - ' + day.key;
                }
              }
            });
            // * Expected output: "Tomar Lun - Mie - Vie"
            med.frequencyString = translate.instant('MY-MEDICATION.MODAL.TOMAR-DIAS-ALTERNOS', { days: daysStr });
            break;
          case '3':
            // * Expected output: "Tomar cada 15 días"
            med.frequencyString = translate.instant('MY-MEDICATION.MODAL.TOMAR-CADA-X-DIAS', { numDays: med.frequencyInterval});
            break;
          default :
            med.frequencyString = null;
          }
        }

      // * Setea el estatus de la medicina, para luego usarlo en el filtro. Opciones: activo, finalizado, suspendido, sin-recordatorio
      // setMedicineStatus(med); // TODO: ver si hace falta ejecutar con los datos del servicio

      // * Información sobre las tomas
      med.numberDailyTakeDescription = med.hasReminders ? prescripcion.prescriptionReminders.numberDailyTakeDescription : null;

      // * Lógica para poblar el array de tomas
      if (med.hasReminders && prescripcion.prescriptionReminders.takePills) {
        const takes = [];
        prescripcion.prescriptionReminders.takePills.forEach(take => {
          // * Las tomas pueden ser de una o más unidades de medicación (sobres, píldoras, etc), así que hallar si la cantidad es 1 o es mayor para que sea en singular ("píldora") o en plural ("píldoras")
          let takePosology: any = null;
          if (Number(take.quantity) === 1) {
            takePosology = `${take.quantity} ${prescripcion.prescriptionReminders.posologyUnitDescriptionSingular}`;
          } else {
            takePosology = `${take.quantity} ${prescripcion.prescriptionReminders.posologyUnitDescription}`;
          }
          const singleTake = new MedicationTakeModel();
          singleTake.id = take.id;
          singleTake.time = take.time;
          singleTake.quantity = take.quantity;
          singleTake.posology = takePosology;
          takes.push(singleTake);
        });
        med.takes = takes;
      } else {
        med.takes = null;
      }

      meds.push(med);
    });
    return meds;
  }
