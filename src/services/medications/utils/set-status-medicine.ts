import * as moment from 'moment';
import {
  MedicationState,
  SwitchState,
  Medication,
  ReminderState,
} from '../../../app/models/medication';
import { MedicationModel } from 'src/models/medication.model';

export function setMedicineStatus(medicine: Medication): void {
  // si No tiene recordatorios
  if (!medicine.hasReminders || medicine.hasReminders === ReminderState.NO) {
    // * Case 1) The medicine in MyMed has no reminders
    medicine.textStatus = MedicationState.SIN_RECORDATORIO;
    return;
  } else { // si tiene recordatorios
    const today = moment();
    if (medicine.endDate) {
      // * Create a Moment object so we can find the diff() in between today and medicine.endDate
      const medicineEndDateAsMomentObject = moment(medicine.endDate, 'YYYY-MM-DD');
      const differenceInDaysBetweenTodayAndEndDate = medicineEndDateAsMomentObject.diff(today, 'days');
      // * Then, the difference in days is used to know what textStatus the medicine must have
      if (differenceInDaysBetweenTodayAndEndDate >= 0) {
        medicine.textStatus = MedicationState.ACTIVO;
      } else {
        medicine.textStatus = MedicationState.FINALIZADO;
      }
    } else if (!medicine.endDate && medicine.startDate) {
      medicine.textStatus = MedicationState.ACTIVO;
    }

    if (medicine.textStatus === MedicationState.ACTIVO && medicine.toggleNotificationButtonState === SwitchState.OFF) {
      // * Case 2) The medicine has its own button for reminders suspended
      // ! TODO Revisar porque esto no lo tengo muy claro
      medicine.textStatus = MedicationState.SUSPENDIDO;
    }
  }
}
