import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

// from https://stackblitz.com/edit/angular-6-communicating-between-components-1gunkw?file=app%2Fapp.component.ts
@Injectable({ providedIn: 'root' })
export class MessageService { // ExperimentalDecorators set to true.
    private subject = new Subject<any>();

    sendMessage(message: string) {
        this.subject.next({ text: message });
    }

    clearMessage() {
        this.subject.next();
    }

    getMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}
