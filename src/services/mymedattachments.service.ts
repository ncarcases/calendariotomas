import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { TranslateService } from '@ngx-translate/core';
import { BaseHttpService } from './basehttp.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { AttachmentModel } from 'src/models/attachment.model';
import { ToastService } from './toast.service';
import { AttachmentViewerModalComponent } from 'src/app/components/attachment-viewer-modal/attachment-viewer-modal.component';
import { ModalController } from '@ionic/angular';
import { File } from '@ionic-native/file/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';

@Injectable()
export class MyMedAttachmentsService extends BaseHttpService {
  // search a constant file and move there.
  static ATT_PDF_EXT = 'pdf';
  static CONTENTTYPE_PDF_EXT = 'application/pdf';
  static ATT_DOC_EXT = 'doc';
  static CONTENTTYPE_DOC_EXT = 'application/msword';
  static ATT_DOCX_EXT = 'docx';
  static CONTENTTYPE_DOCX_EXT = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
  static ATT_PPT_EXT = 'ppt';
  static CONTENTTYPE_PPT_EXT = 'application/vnd.ms-powerpoint';
  static ATT_PPTX_EXT = 'pptx';
  static CONTENTTYPE_PPTX_EXT = 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
  static ATT_XLS_EXT = 'xls';
  static CONTENTTYPE_XLS_EXT = 'application/vnd.ms-excel';
  static ATT_XLSX_EXT = 'xlsx';
  static CONTENTTYPE_XLSX_EXT = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
  static ATT_JPG_EXT = 'jpg';
  static ATT_JPEG_EXT = 'jpeg';
  static ATT_PNG_EXT = 'png';

  // relative path to web service.
  getImageUrl =  'bidafarma/file/obtenerImagenValoracion';
  imageToShow: any = null;
  urlDocPreview: SafeResourceUrl = null;

  constructor(
    protected httpClient: HttpClient,
    private domSanitizer: DomSanitizer,
    private translate: TranslateService,
    protected currentConfig: ConfigGlobalModel,
    private toast: ToastService,
    private modalCtlr: ModalController,
    private file: File,
    private fileOpener: FileOpener
  ) {
    super(currentConfig, httpClient);
    this.getImageUrl = this.currentConfig.config.baseUrl + this.getImageUrl;
  }

  /*
  ? connects to MyMed service and download file into system.
  ? @param inputFile
  Example: /obtenerImagenValoracion?studyUID=&serieUID=&objectUID=&extension=jpg&codPaciente=&thumbnail=false
  MIME-Types help: https://www.iana.org/assignments/media-types/media-types.xhtml
  */

  async download(inputFile: AttachmentModel): Promise<string> {
    // First, create a toast message
    this.toast.presentToast(this.translate.instant('COMMON.DOWNLOAD-START'), 2000);
    if (inputFile === null) { return; }
    try {
      const url = `${this.getImageUrl}?studyUID=${inputFile.studyUID}&serieUID=${inputFile.serieUID}&objectUID=${inputFile.objectUID}&extension=${inputFile.fileExtension}&codPaciente=${this.currentConfig.config.codPaciente}&thumbnail=false`;
      const filenameLowerCase = inputFile.fileExtension.toLocaleLowerCase();
      const responseType = 'blob';
      let response: any;

        // * Check if it's a image file (jpg, jpeg or png extension)
      if (filenameLowerCase === MyMedAttachmentsService.ATT_JPG_EXT
      || filenameLowerCase === MyMedAttachmentsService.ATT_JPEG_EXT
      || filenameLowerCase === MyMedAttachmentsService.ATT_PNG_EXT) {
        const headers = new HttpHeaders()
        .set('Authorization', `Bearer ${this.currentConfig.config.token}`)
        .set('Content-type', 'image/' + filenameLowerCase);
        response = await this.getRequest(url, {headers}, responseType);
        // Remove the toast once it's downloaded
        this.toast.dismissToast();
        this.createImageFromBlob(response, this.sanitizeFilename(inputFile.fileName));
      } else {
        // * It's not an image, so find out what kind of document is
        let auxContentType = '';
        switch (filenameLowerCase) {
          case MyMedAttachmentsService.ATT_PDF_EXT:
            auxContentType = MyMedAttachmentsService.CONTENTTYPE_PDF_EXT;
            break;
          case MyMedAttachmentsService.ATT_DOC_EXT:
            auxContentType = MyMedAttachmentsService.CONTENTTYPE_DOC_EXT;
            break;
          case MyMedAttachmentsService.ATT_DOCX_EXT:
            auxContentType = MyMedAttachmentsService.CONTENTTYPE_DOCX_EXT;
            break;
          case MyMedAttachmentsService.ATT_PPT_EXT:
            auxContentType = MyMedAttachmentsService.CONTENTTYPE_PPT_EXT;
            break;
          case MyMedAttachmentsService.ATT_PPTX_EXT:
            auxContentType = MyMedAttachmentsService.CONTENTTYPE_PPTX_EXT;
            break;
          case MyMedAttachmentsService.ATT_XLS_EXT:
            auxContentType = MyMedAttachmentsService.CONTENTTYPE_XLS_EXT;
            break;
          case MyMedAttachmentsService.ATT_XLSX_EXT:
            auxContentType = MyMedAttachmentsService.CONTENTTYPE_XLSX_EXT;
            break;
          default:
            break;
        }
        const headers = new HttpHeaders()
        .set('Authorization', `Bearer ${this.currentConfig.config.token}`)
        .set('Content-type', 'application/json');
        response = await this.getRequest(url, { headers }, responseType);
        // Remove the toast once it's downloaded
        this.toast.dismissToast();
        this.createDocumentFromBlob(response, auxContentType, this.sanitizeFilename(inputFile.fileName));
        }
      } catch (err) {
        this.toast.presentToast(this.translate.instant('COMMON.CANT-DOWNLOAD-DOCUMENT'));
        throw new Error(JSON.stringify(err));
      }
    }

  private sanitizeFilename(file: string) {
    /*
    ? Used to remove "tildes" and "ñ" in filename when language is Spanish
    */
    let sanitizedFilename = '';
    const chars = {
      á: 'a', é: 'e', í: 'i', ó: 'o', ú: 'u',
      à: 'a', è: 'e', ì: 'i', ò: 'o', ù: 'u', ñ: 'n',
      Á: 'A', É: 'E', Í: 'I', Ó: 'O', Ú: 'U',
      À: 'A', È: 'E', Ì: 'I', Ò: 'O', Ù: 'U', Ñ: 'N' };
    sanitizedFilename = file.replace(/\s/g, '-');
    sanitizedFilename = sanitizedFilename.replace(/[áàéèíìóòúùñ]/ig, (e) => chars[e]);
    return sanitizedFilename;
  }

  private async createImageFromBlob(image: Blob, name: string) {
    const unsafeImageUrl = await URL.createObjectURL(image);
    const imageUrl = this.domSanitizer.bypassSecurityTrustUrl(unsafeImageUrl);

    const attachmentModal = await this.modalCtlr.create({
      component: AttachmentViewerModalComponent,
      componentProps: {
        imageBlob: image,
        imageUrl
      },
      showBackdrop: true
    });
    attachmentModal.present();
  }

  public createDocumentFromBlob(response, contentType, filename) {
    if (this.file.dataDirectory) {
      this.file.writeFile(this.file.dataDirectory, filename, response, {replace: true})
      .then(fileWritten => {
        console.log(fileWritten);
        this.fileOpener.showOpenWithDialog(fileWritten.nativeURL, contentType)
        .then(() => {
          console.log('éxito al abrir');
        })
        .catch(err => {
          console.log(err);
          this.toast.presentToast('Error al descargar o abrir');
        });
      })
      .catch(err => {
        console.error(err);
        this.toast.presentToast('Error al descargar o abrir');
      });
    }
  }
}
