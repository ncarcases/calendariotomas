import { Injectable, NgZone } from '@angular/core';
import { ConfigGlobalModel } from '../models/config.globalmodel';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { BaseHttpService } from './basehttp.service';
import { from, Observable, of } from 'rxjs';
import { Router } from '@angular/router';

import {
    Plugins,
    PushNotification,
    PushNotificationToken,
    PushNotificationActionPerformed,
} from '@capacitor/core';

const { PushNotifications } = Plugins;

import { ModalController } from '@ionic/angular';
import { AlertService } from './alert.service';
import {  generateUUID,
} from './medications/utils/uuid';

@Injectable({
    providedIn: 'root'
})

export class PushService extends BaseHttpService {
    constructor(
        protected currentConfig: ConfigGlobalModel,
        protected http: HttpClient,
        private router: Router,
        private ngZone: NgZone,
        private alertService: AlertService,
        private modalController: ModalController,

        ) {
            super(currentConfig, http);
        }
        public firebaseToken: any;

        public initPushNotifications() {
            this.registerForRemoteNotifications();

            PushNotifications.addListener('registration',
                (token: PushNotificationToken) => {
                    this.onNotificationRegistration( token );
                }
            );

            PushNotifications.addListener('registrationError',
                (error: any) => {
                    this.onNotificationRegistrationError( error );
                }
            );

            PushNotifications.addListener('pushNotificationReceived',
                (notification: PushNotification) => {
                    this.onNotificationReceived( notification );
                }
            );

            PushNotifications.addListener('pushNotificationActionPerformed',
                (notification: PushNotificationActionPerformed) => {
                    this.onNotificationTapped( notification );
                }
            );
        }

    private onNotificationTapped( notification: PushNotificationActionPerformed )
    {
            console.log("notification action", notification)
            const response = notification.notification.data;
            this.redirectToOverView( response );
        }

        private onNotificationReceived( notification: PushNotification ) {
            console.log( 'Push received: ' + JSON.stringify( notification ) );
            this.updateMedicationInfo(notification);
        }

        private onNotificationRegistrationError( error: any ) {
            console.log( 'Error on registration: ' + JSON.stringify( error ) );
        }

        private onNotificationRegistration( token: PushNotificationToken ) {
            this.firebaseToken = token.value;
            console.log( 'push notification ok yayyyy', token );
            this.addUserFirebaseToken( token.value ).subscribe();
        }

        private registerForRemoteNotifications() {
            PushNotifications.requestPermission().then( result => {
                if (result.granted) {
                    PushNotifications.register();
                } else {
                    // Show some error
                }
            });
        }


        public addUserFirebaseToken(token): Observable<any> {
            if (token) {
                const { mail, baseUrl } = this.currentConfig.config;

                const url = `${baseUrl}backend-api/users/addToken`;
                const headers = new HttpHeaders()
                .set('X-Request-ID', generateUUID())
                .set('Content-Type', 'application/json');

                const body = { token, mail };

                return from(this.postRequest(url, body, {headers}));
            } else {
                return of();
            }
        }

        public redirectToOverView(response) {
            this.ngZone.run(() => {
                this.router.navigate(['/my-medication', 'overview'], {
                    queryParams: {
                        ...response,
                    }
                });
            });
        }

        public updateMedicationInfo(notification) {
        // TODO: actualizar listado de medicamentos en background
        // TODO: Poner un mensaje deferente en función del tipo de notificación:
        console.log(notification);
        this.alertService.presentToastWithOnDismiss(
        notification.title,
        6000,
        'med-toast-info',
        () => {
            this.alertService.dismissLoadingDialog();
            // TODO CERRAR TOAST
            this.unsetModals();
            this.router.navigate(['/my-medication', 'overview'], {
            queryParams: {
                ...notification.data,
            }
            });
        }
        );
    }

        public unsetModals() { // TODO: hacer un servicio de modales donde poner esta funcionalidad
            this.modalController.getTop().then((modal) => {
            if (modal) {
                modal.dismiss();
            }
            });
        }

        public removeToken(mail): Observable<any> {
            const { baseUrl } = this.currentConfig.config;

            const url = `${baseUrl}backend-api/users/removeToken`;
            const headers = new HttpHeaders()
            .set('Content-Type', 'application/json');

            const body = { token: this.firebaseToken, mail };

            return from(this.postRequest(url, body, {headers}));
        }
    }
