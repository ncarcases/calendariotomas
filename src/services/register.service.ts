import { Injectable } from '@angular/core';
import { ConfigGlobalModel } from '../models/config.globalmodel';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { FormGroup } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { LocalStorageKeys } from '../models/localstorage.keys.model';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  // no usar esta url, login y registro estan en sitios distintos
  // registerUrl = `${this.currentConfig.config.baseUrl}users/add`;

  registerUrl;
  constructor(
    private currentConfig: ConfigGlobalModel,
    private httpClient: HttpClient,
    public navCtlr: NavController,
    public storage: Storage,
  ) {
  }

  public async register(data: FormGroup) {
    await this.getUrl();
    // There's no need to check if properties have content because the form can't be sent if it's empty or if the checkbox isn't accepted
    if (this.currentConfig.config.baseUrl && this.currentConfig.config.baseUrl.length > 0) {
      try {
        const headers = new HttpHeaders()
        .set('content-type', 'application/json');
        const body = {
          mail: data.value.mail,
          password: data.value.password
        };
        const response: any = await this.httpClient.post(this.registerUrl, JSON.stringify(body), { headers })
        .toPromise();
        return response;
      } catch (err) {
        console.error(err);
      }
    }
  }

  async getUrl() {
    await this.storage.get(LocalStorageKeys.KEY_ENVIRONMENT).then(async selectedEnv => {
      if (selectedEnv === 'Development') {
       this.registerUrl = 'http://192.168.146.201:8080/users/add';
      } else {
        this.registerUrl = `${this.currentConfig.config.baseUrl}backend-api/users/add`;
      }
    });
  }
}

