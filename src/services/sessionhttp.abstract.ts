import { HttpHeaders } from '@angular/common/http';

export abstract class SessionHttp {
  // it requests a get HTTP request and injects the session token
  abstract getRequest(url: string, inputheader: { headers: HttpHeaders; }): Promise<any>;

  // it requests a post HTTP request and injects the session token
  abstract postRequest(url: string, postData: any, inputheader: { headers: HttpHeaders; }): Promise<any>;
}
