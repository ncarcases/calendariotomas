import { Injectable } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Injectable({
  providedIn: 'root'
})
export class ShareService {

  constructor(
    public actionSheet: ActionSheetController,
    public translate: TranslateService,
    private socialSharing: SocialSharing
  ) { }

  public share(data: string[]): boolean {
    let result = true;
    try {
      if (data) {
        let parsedMessage = '';
        data.forEach(element => {
          parsedMessage += element + '\r\n';
        });
        while (parsedMessage.search('undefined') > 0) {
          parsedMessage = parsedMessage.replace('undefined', this.translate.instant('COMMON.NO-ENTRY'));
        }
        const options: any = {
          message: parsedMessage,
          subject: this.translate.instant('COMMON.COMPANY'),
          files: [],
          url: '',
          chooserTitle: ''
        };
        this.socialSharing.shareWithOptions(options)
          .then(res => {
            result = true;
            // tslint:disable-next-line: no-console
            console.debug('Correctly sent.');
          })
          .catch(err => {
            result = false;
            // tslint:disable-next-line: no-console
            console.debug('Incorrectly sent.');
          });
      } else {
        console.log('No data to share.');
        result = false;
      }
    } catch (err) {
      console.error(JSON.stringify(err));
      result = false;
    }

    return result;
  }

  public shareWithoutParameters() {
    this.socialSharing.share();
  }

  public shareWithParameters(options) {
    this.socialSharing.shareWithOptions(options);
  }
}
