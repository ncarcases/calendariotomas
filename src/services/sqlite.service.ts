import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { DatabaseService } from './database.service';
import { createTablesQueries } from '../app/config/sql-queries';

declare var window: any;

@Injectable({
    providedIn: 'root',
})
export class SQLiteService {
    data = new BehaviorSubject([]);
    private isDbReady: BehaviorSubject<boolean> = new BehaviorSubject(false);
    private areTablesCreated: BehaviorSubject<boolean> = new BehaviorSubject(
        false
    );

    dbInstance: any;

    constructor(
        private platform: Platform,
        private httpClient: HttpClient,
        private sql: DatabaseService
    ) {}

    // create DB tables

    private createTables() {
        this.isDbReady.next(true);
        return this.createTablesByName('medicines')
            .then(() => this.createTablesByName('takes'))
            .then((result) => {
                this.areTablesCreated.next(true);
            })
            .catch((err) => {
                console.error(err);
                console.error('error creating or opening one of the databases');
            });
    }

    ////////////////////
    enableForeignKeys() {
        return this.sql.dbInstance.executeSql('pragma foreign_keys = on');
    }

    createTablesByName(tableName) {
        return this.sql.dbInstance.executeSql(
            createTablesQueries(tableName),
            []
        );
    }

    dbState() {
        return this.isDbReady.asObservable();
    }

    getTableData(tableName, condition?, selectModifier?) {
        const response = [];
        return this.createTables().then(() => {
            const sqlStatement = `SELECT ${selectModifier ? selectModifier : ' *'}
            from ${tableName} ${condition ? condition : undefined}`;
            return this.sql.dbInstance
                .executeSql(
                    sqlStatement,
                    []
                )
                .then((data) => {
                    for (let i = 0; i < data.rows.length; i++) {
                        response.push(data.rows.item(i));
                    }
                    return response;
                })
                .catch((err) => {
                    if (err) {
                        console.log('Err getting data?', err);
                    }
                });
        });
    }

    addTableData(tableName, data?, statement?): Promise<any> {
        const promise = new Promise((resolve, reject) => {
            this.createTables().then(() => {
                if (statement) {
                    const responseData = {
                        response: '',
                        data: statement
                    };
                    this.sql.dbInstance
                        .executeSql(statement, [])
                        .then(() => {
                            // console.log(`${statement} Data Written`);
                            responseData.response = 'SUCCESS';
                            responseData.data = statement;
                            resolve(responseData);
                        })
                        .catch((err) => {
                            console.log('error writing to db');
                            console.error(err);
                            responseData.response = 'ERROR';
                            responseData.data = statement;
                            reject(responseData);
                        });
                } else {
                if (data) {
                    let sqlStatement = '';
                    let testingData = [];
                    let isArray = false;
                    const responseData = {
                        response: '',
                        data: {},
                    };
                    if (Array.isArray(data) && data.length > 0) {

                        data.forEach((dt) => {
                            testingData = Object.values(dt);
                            let keys: any = Object.keys(dt);
                            testingData.forEach((td, i) => {
                                if (!td && typeof td !== 'number') {
                                    testingData.splice(i, 1);
                                    keys.splice(i, 1);
                                }
                            });
                            let questionMarks = '';
                            keys.forEach((k, i) => {
                                if (
                                    testingData[i] ||
                                    typeof testingData[i] === 'number'
                                ) {
                                    if (i < keys.length - 1) {
                                        questionMarks = questionMarks + '?, ';
                                    } else {
                                        questionMarks = questionMarks + '?';
                                    }
                                } else {
                                    keys.splice(i, 1);
                                    testingData.splice(i, 1);
                                    if (i === keys.length - 1) {
                                        questionMarks = questionMarks + '?';
                                    } else {
                                        questionMarks = questionMarks + '?, ';
                                    }
                                }
                            });
                            keys = '(' + keys.join(',').toString() + ')';
                            sqlStatement = `INSERT INTO ${tableName} ${keys} VALUES (${questionMarks})`;
                            this.sql.dbInstance
                                .executeSql(sqlStatement, testingData)
                                .then(() => {
                                    responseData.response = 'SUCCESS';
                                    responseData.data = data;
                                    resolve(responseData);
                                })
                                .catch((err) => {
                                    console.log('error writing to db');
                                    console.error(err);
                                    responseData.response = 'ERROR';
                                    responseData.data = data;
                                    reject(responseData);
                                });
                        });
                    } else {
                        isArray = false;
                        testingData = Object.values(data);
                        let keys: any = Object.keys(data);
                        testingData.forEach((td, i) => {
                            if (!td && typeof td !== 'number') {
                                testingData.splice(i, 1);
                                keys.splice(i, 1);
                            }
                        });
                        let questionMarks = '';
                        keys.forEach((k, i) => {
                            if (
                                testingData[i] ||
                                typeof testingData[i] === 'number'
                            ) {
                                if (i < keys.length - 1) {
                                    questionMarks = questionMarks + '?, ';
                                } else {
                                    questionMarks = questionMarks + '?';
                                }
                            } else {
                                keys.splice(i, 1);
                                testingData.splice(i, 1);
                                if (i === keys.length - 1) {
                                    questionMarks = questionMarks + '?';
                                } else {
                                    questionMarks = questionMarks + '?, ';
                                }
                            }
                        });
                        keys = '(' + keys.join(',').toString() + ')';
                        sqlStatement = `INSERT INTO ${tableName} ${keys} VALUES (${questionMarks})`;
                        this.sql.dbInstance
                            .executeSql(
                                sqlStatement,
                                !isArray ? testingData : []
                            )
                            .then(() => {
                                responseData.response = 'SUCCESS';
                                responseData.data = data;
                                resolve(responseData);
                            })
                            .catch((err) => {
                                console.log('error writing to db');
                                console.error(err);
                                responseData.response = 'ERROR';
                                responseData.data = data;
                                reject(responseData);
                            });
                    }
                }
            }
            });
        });

        return promise;
    }

    updateTableData(
        tableName,
        data?,
        idVal?,
        idField?,
        condition?
    ): Promise<any> {
        const promise = new Promise((resolve, reject) => {
            this.createTables().then(() => {
                let sqlStatement = `UPDATE ${tableName} SET`;
                if (data) {
                    const keysForBack = Object.keys(data);
                    keysForBack.forEach((kfb, i) => {
                        if (typeof data[kfb] === 'string') {
                            sqlStatement =
                                sqlStatement + ` ${kfb} = '${data[kfb]}',`;
                        }

                        if (i === keysForBack.length - 1) {
                            sqlStatement =
                                sqlStatement + ` ${kfb} = '${data[kfb]}'`;
                        }
                    });
                }
                if (!condition) {
                    sqlStatement =
                        sqlStatement + ` WHERE ${idField} = '${idVal}'`;
                } else {
                    sqlStatement = sqlStatement + `${condition}`;
                }

                // const foundTableId = data.find((dt) => dt );
                const responseData = {
                    response: '',
                    data: {},
                };
                this.sql.dbInstance
                    .executeSql(sqlStatement, [])
                    .then(() => {
                        responseData.response = 'SUCCESS';
                        responseData.data = data ? data : condition;
                        resolve(responseData);
                    })
                    .catch((err) => {
                        console.log('error updating to db');
                        console.error(err);
                        responseData.response = 'ERROR';
                        responseData.data = data ? data : condition;
                        reject(responseData);
                    });
            });
        });

        return promise;
    }

    deleteTableData(tableName, tableKey?, id?, condition?): Promise<any> {
        return this.createTables().then(() => {
                if (id) {
                    if (typeof id === 'string') {
                        id = '"' + id + '"';
                    }
                }
                let whereClause;
                whereClause = `WHERE ${tableKey} = ${id};`;
                const sqlStatement = `DELETE from ${tableName}
                        ${condition ? condition : whereClause}`;
                const responseData = {
                    response: '',
                    data: {},
                };
                return this.sql.dbInstance
                    .executeSql(sqlStatement, [])
                    .then(() => {
                        responseData.response = 'SUCCESS';
                        responseData.data = id ? id : condition;
                    })
                    .catch((err) => {
                        console.log('error deleting from db');
                        console.error(err);
                        responseData.response = 'ERROR';
                        responseData.data = id ? id : condition;
                    });
            });
    }
}
