
  import { Injectable } from '@angular/core';
  import { BaseHttpService } from '../basehttp.service';
  import { ConfigGlobalModel } from 'src/models/config.globalmodel';
  import { HttpClient } from '@angular/common/http';


  @Injectable({
    providedIn: 'root'
  })
  export class TakesService extends BaseHttpService {
    takesUrl = `${this.currentConfig.config.baseUrl}bidafarma/fhir/rest/TomaDTO`;
    constructor(
      protected currentConfig : ConfigGlobalModel,
      protected httpClient    : HttpClient,
    ) {
        super(currentConfig, httpClient);
    }

    public async getMedicationTakesLog(parameters): Promise<any> {
      let customUrl = `${this.takesUrl}` + '?';
      for (const parameter of Object.entries(parameters)) {
        customUrl += parameter[0] + '=' + parameter[1] + '&';
      }
      // remove last ampersant &
      customUrl = customUrl + '_sort:asc=fchToma';
      const response: any = await this.getRequest(customUrl, null);
      return response;
    }

    public async postMedicationTakeRegister(payload): Promise<any> {
        const response: any = await this.postRequest(this.takesUrl, payload);
        return response;
    }

    public async deleteMedicationTakeRegister(payload): Promise<any> {
      const response: any = await this.deleteRequest(`${this.takesUrl}/${payload}?_format=json`);
      return response;
    }


  }
