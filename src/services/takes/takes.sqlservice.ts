
  import { Injectable } from '@angular/core';
  import { ConfigGlobalModel } from 'src/models/config.globalmodel';
  import { SQLiteService } from '../sqlite.service';


  @Injectable({
    providedIn: 'root'
  })
  export class TakesSQLService  {
    constructor(
      protected currentConfig : ConfigGlobalModel,
      private sql: SQLiteService,
    ) {
    }


   /*
   ************************************************
                  SQLITE METHODS
   ************************************************
   */

  getSQLiteTakes(idMedication) {
    return this.sql.getTableData('takes', ` WHERE medicationId = ${idMedication} AND codPaciente = '${this.currentConfig.config.codPaciente}'`);
  }

   /*
   ************************************************
                  SQLITE METHODS
   ************************************************
   */

  }
