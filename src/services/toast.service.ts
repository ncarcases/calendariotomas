import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {
  toast: any = null;
  constructor(
    public toastCtlr: ToastController
  ) { }

  async presentToast(msg: string, time = 2000, cssClass?: string) {
    this.toast = await this.toastCtlr.create({
      message: msg,
      position: 'top',
      color: 'dark',
      duration: time,
      showCloseButton: true,
      closeButtonText: 'Cerrar',
      cssClass
    });
    this.toast.present();
  }
  async presentToastWithOnDismiss(msg: string, time = 2000, cssClass?: string, cb?: any) {
    this.toast = await this.toastCtlr.create({
      message: msg,
      position: 'top',
      color: 'dark',
      duration: time,
      cssClass,
      buttons: [
        {
          text: 'Ver',
          cssClass,
          handler: () => {
            cb();
            this.dismissToast();
          }
        }
      ]
    });
    this.toast.present();
  }

  async dismissToast() {
    if (this.toast) {
      this.toast.dismiss();
    }
  }
}






