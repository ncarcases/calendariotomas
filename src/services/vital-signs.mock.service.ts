import { Injectable } from '@angular/core';
import { VitalSignsModel, VitalSignsMeasures, VitalSignsMeasuresList } from 'src/models/vital-signs.model';

@Injectable()
export class VitalSignsMockService {
  constructor() {}

  getVitalSigns() {
    const result: VitalSignsModel = new VitalSignsModel();

    result.lastUpdated = new Date('2019/08/20');
    result.category = 'Constantes';
    result.cssClassIcon = 'icon-bida-constantes';
    result.cssBackgroundColor = 'rgba(171,72,229,0.1)';
    result.entries = [];

    const measure1: VitalSignsMeasures = new VitalSignsMeasures();
    measure1.lastUpdatedLocal = new Date();
    measure1.list = [{
      key: 'Peso (kg)',
      value: 96
    }, {
      key: 'Talla (cm)',
      value: 179
    }, {
      key: 'Glucosa (mg/dl)',
      value: 124
    }, {
      key: 'FC (lpm)',
      value: 82
    }, {
      key: 'TAS (mmHg)',
      value: 50
    }, {
      key: 'TAD (mmHg)',
      value: 130
    }];

    const measure2: VitalSignsMeasures = new VitalSignsMeasures();
    measure2.lastUpdatedLocal = new Date();
    measure2.list = [{
      key: 'Peso (kg)',
      value: 85
    }, {
      key: 'Talla (cm)',
      value: 181
    }, {
      key: 'Glucosa (mg/dl)',
      value: 90
    }, {
      key: 'FC (lpm)',
      value: 72
    }, {
      key: 'TAS (mmHg)',
      value: 60
    }, {
      key: 'TAD (mmHg)',
      value: 120
    }];

    const measure3: VitalSignsMeasures = new VitalSignsMeasures();
    measure3.lastUpdatedLocal = new Date();
    measure3.list = [{
      key: 'Peso (kg)',
      value: 78
    }, {
      key: 'Talla (cm)',
      value: 173
    }, {
      key: 'FC (lpm)',
      value: 88
    }];

    result.entries.push(measure1);
    result.entries.push(measure2);
    result.entries.push(measure3);

    return result;
  }
}
