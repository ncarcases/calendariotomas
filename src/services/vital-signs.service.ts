import { Injectable } from '@angular/core';
import { BaseHttpService } from './basehttp.service';
import { ConfigGlobalModel } from 'src/models/config.globalmodel';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class VitalSignsService extends BaseHttpService {

  vitalSignsUrl = 'bidafarma/fhir/rest/ConstanteDTO?_format=json&_query=constantes&codPaciente=';

  constructor(
    protected currentConfig: ConfigGlobalModel,
    protected httpClient: HttpClient
  ) {
    super(currentConfig, httpClient);
  }

  public async getVitalSigns(): Promise<any> {
    const url = `${this.currentConfig.config.baseUrl}${this.vitalSignsUrl}${this.currentConfig.config.codPaciente}`;

    const response: any = await this.getRequest(url, null);

    return response;
  }
}
