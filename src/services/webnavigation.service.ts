import { Injectable } from '@angular/core';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Injectable()
export class WebNavigationService {
    constructor(private iab: InAppBrowser) {}

    public navigateTo(url: string): boolean {
        const result = false;
        try {
            if (url) {
                this.iab.create(encodeURI(url), '_system');
            } else {
                console.error('No URL to navigate to.');
            }
        } catch (err) {
            console.error(JSON.stringify(err));
        }
        return result;
    }
}
